<?php

use Flood\Component\Storage\Storage;
use Hydro\Container;
use Flood\Captn;
use Doctrine\Common\Cache\FilesystemCache;

if(!defined('HYDRO_WD')) {
    define('HYDRO_WD', __DIR__ . '/');
}

function hydroFatalError($name, \Exception $e) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);

    $err_code = $e->getCode();

    if(\Hydro\Hydro::$env === 'development') {
        $dgtrace = nl2br(json_encode($e->getTrace(), JSON_PRETTY_PRINT));

        return <<<HTML
<div style="font-family: 'Consolas', 'Monaco', 'Andale Mono', monospace;">
    <h1 style="color: #ff726d; margin: 6px 0 18px 0;">{$name}</h1>
    <p>{$e->getMessage()}</p>
    <pre style="background: #272B36; color: #bfbfbf; padding: 12px; line-height: 9px; white-space: pre-wrap; word-break: break-all; margin: 6px 0;">Code: {$err_code}</pre>
    <pre style="background: #272B36; color: #bfbfbf; padding: 12px; line-height: 9px; white-space: pre-wrap; word-break: break-all; margin: 6px 0;">{$dgtrace}</pre>
</div>
HTML;
    }

    return <<<HTML
<div style="font-family: 'Consolas', 'Monaco', 'Andale Mono', monospace;">
    <h1 style="color: #ff726d; margin: 6px 0 18px 0;">System Error</h1>
    <p>Please try again later.</p>
    <pre style="background: #272B36; color: #bfbfbf; padding: 12px; line-height: 9px; white-space: pre-wrap; word-break: break-all; margin: 6px 0;">Code: {$err_code}</pre>
</div>
HTML;
}

if(function_exists('captnIsSteering') && captnIsSteering()) {

    Captn\Acknowledge::signal(
        'hydro.hydro',
        'root.hydro.hydro',
        Captn::TYPE__LIB,
        [
            'annotation' => [
                // which folders should be scanned
                'scan' => [
                    [
                        'ext' => 'php',
                        'include' => __DIR__ . '/src/',
                    ],
                    [
                        'ext' => 'php',
                        'include' => __DIR__ . '/../component-storage/src/',
                    ],
                ],
            ],
        ]
    );

    /*
     * Hydro Boot Events - Config
     */

    Captn\EventDispatcher::on(
        'hydro.boot',
        static function() {
            Container::i()->register('config', new \Hydro\Config());
            Container::_config()->readFromMeta('config/meta.json', HYDRO_WD);
        }
    );

    /*
     * Hydro Flow Init - Events
     */

    Captn\EventDispatcher::on(
        'hydro.init',
        static function() {
            $config = Container::_config()->getData(['sonar']);
            if(isset($config['cache']['path'])) {
                $config['cache']['path'] = HYDRO_WD . $config['cache']['path'];
            }
            Container::i()->register('sonar', new \Flood\Component\Sonar($config, new FilesystemCache($config['cache']['path'])));
            //Captn\EventDispatcher::trigger('flood.parseAnnotation', Container::_sonar());
        }
    );

    Captn\EventDispatcher::on(
        'hydro.init',
        static function() {
            //
            // autowiring just singleton service atm
            //
            Container::i()->register('hydro', static function() {
                return new \Hydro\Hydro();
            }, false);
            Container::i()->register('hook-storage', static function() {
                return new \Hydro\Hook\Storage();
            }, false);
            Container::i()->register('feature-access-manager', static function() {
                return new \HydroFeature\User\AccessManager();
            }, false);
            Container::i()->register('feature-user', static function() {
                return new \HydroFeature\User\User();
            }, false);
            Container::i()->register('feature-user-active', static function() {
                return new \HydroFeature\User\Active();
            }, false);
            Container::i()->register('feature-user-token', static function() {
                return new \HydroFeature\User\Token();
            }, false);
            Container::i()->register('feature-user-roles', static function() {
                return new \HydroFeature\User\Roles();
            }, false);
            Container::i()->register('feature-template', static function() {
                return new \HydroFeature\Template\Template();
            }, false);
            Container::i()->register('config-template', static function() {
                return new \HydroFeature\Template\Config();
            }, false);
            Container::i()->register('feature-file-manager', static function() {
                return new \HydroFeature\FileManager\FileManager();
            }, false);
            Container::i()->register('feature-content', static function() {
                return new \HydroFeature\Content\Content();
            }, false);
            Container::i()->register('feature-content-schema', static function() {
                return new \HydroFeature\Content\Schema();
            }, false);
            Container::i()->register('feature-content-page-type', static function() {
                return new \HydroFeature\Content\PageType();
            }, false);
            Container::i()->register('feature-content-block', static function() {
                return new \HydroFeature\Content\Block();
            }, false);
            Container::i()->register('feature-shop', static function() {
                return new \HydroFeature\Shop\Shop();
            }, false);
            Container::i()->register('feature-shop-product-type', static function() {
                return new \HydroFeature\Shop\ProductTypes();
            }, false);
            Container::i()->register('feature-shop-order-builder', static function() {
                return new \HydroFeature\Shop\OrderBuilder();
            });
            Container::i()->register('feature-inventory', static function() {
                return new \HydroFeature\Shop\Inventory();
            }, false);
            Container::i()->register('feature-mailboxes', static function() {
                return new \HydroFeature\Mailer\MailBoxManager();
            }, false);
            Container::i()->register('feature-mailings', static function() {
                return new \HydroFeature\Mailer\MailingStorage();
            }, false);
            Container::i()->register('feature-message', static function() {
                return new \HydroFeature\Message\MessageManager();
            }, false);
            Container::i()->register('feature-newsletter', static function() {
                return new \HydroFeature\Newsletter\Newsletter();
            });
            Container::i()->register('feature-newsletter-registration', static function() {
                return new \HydroFeature\Newsletter\NewsletterRegistrationHandler();
            });
            Container::i()->register('feature-workflow', static function() {
                return new \HydroFeature\Workflow\WorkflowManager();
            }, false);
            Container::i()->register('storage-data', static function() {
                Storage::$config = static function($selector) {
                    array_unshift($selector, 'db');
                    return Container::_config()->getData($selector);
                };
                return new Storage();
            }, false);
            Container::i()->register('cdn', static function() {
                return new \Flood\Component\Cdn\CDN();
            }, false);
            Container::i()->register('session', static function() {
                return new Hydro\Session\Session();
            }, false);
            Container::i()->register('config-session', static function() {
                return new Hydro\Session\Config();
            }, false);

            $container_service_map = Container::_sonar()::$storage->search('Flood\Component\Sonar\Annotation\Service');
            //var_dump($container_service_map);
            foreach($container_service_map as $target => $service) {
                /**
                 * @var \Flood\Component\Sonar\Annotation\Service $service
                 */
                Container::i()->register($service->id, static function() use ($target) {
                    return new $target();
                }, $service->executed);
            }
        }
    );

    /*
     * Hydro Flow Exec - Events
     */
    Captn\EventDispatcher::on(
        'hydro.exec',
        static function() {
            $hydro = Container::_hydro();
            $hydro->prepareComponent();
            $hydro->prepareHook();
            Captn\EventDispatcher::trigger('hydro.exec.bindComponent');
            $hydro->route();
            $hydro->display($hydro->mangle($hydro->dispatch()));
        }
    );

    /*
     * Hydro  - Events
     */
}