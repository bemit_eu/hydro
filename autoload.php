<?php

$vendor_autoload = dirname(__DIR__, 2) . '/autoload.php';

// composer install validation
if(file_exists($vendor_autoload)) {
    // pre-load only the classes needed for hydro auto register logic which are executed through composer file includes
    require dirname(__DIR__, 2) . '/psr/container/src/ContainerExceptionInterface.php';
    require dirname(__DIR__, 2) . '/psr/container/src/NotFoundExceptionInterface.php';
    require dirname(__DIR__, 2) . '/psr/container/src/ContainerInterface.php';

    require dirname(__DIR__, 2) . '/flood/captn/autoload.php';

    $composer_autoloader = require $vendor_autoload;
} else {
    throw new \Exception(
        'Please run `composer install` to setup dependencies.'
    );
}