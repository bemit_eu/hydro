<?php

namespace HydroFeature;

use Hydro\Container as HydroContainer;

/**
 * Flood\Component\Container
 *
 * @category
 * @package    HydroFeature
 * @author     Michael Becker - michael@bemit.codes
 * @copyright  2019 bemit UG (haftungsbeschraenkt)
 * @since      Version 0.0.1
 * @version    0.0.1
 */
class Container {

    /**
     * convenience method for `get('feature-access-manager')`
     *
     * @return \HydroFeature\User\AccessManager
     */
    public static function _accessManager() {
        return HydroContainer::i()->get('feature-access-manager');
    }

    /**
     * convenience method for `get('feature-user-active')`
     *
     * @return \HydroFeature\User\Active
     */
    public static function _userActive() {
        return HydroContainer::i()->get('feature-user-active');
    }

    /**
     * convenience method for `get('feature-user')`
     *
     * @return \HydroFeature\User\User
     */
    public static function _user() {
        return HydroContainer::i()->get('feature-user');
    }

    /**
     * convenience method for `get('feature-user-token')`
     *
     * @return \HydroFeature\User\Token
     */
    public static function _userToken() {
        return HydroContainer::i()->get('feature-user-token');
    }

    /**
     * convenience method for `get('feature-user-roles')`
     *
     * @return \HydroFeature\User\Roles
     */
    public static function _userRoles() {
        return HydroContainer::i()->get('feature-user-roles');
    }

    /**
     * convenience method for `get('feature-template')`
     *
     * @return \HydroFeature\Template\Template
     */
    public static function _template() {
        return HydroContainer::i()->get('feature-template');
    }

    /**
     * convenience method for `get('feature-file-manager')`
     *
     * @return \HydroFeature\FileManager\FileManager
     */
    public static function _fileManager() {
        return HydroContainer::i()->get('feature-file-manager');
    }

    /**
     * convenience method for `get('feature-content')`
     *
     * @return \HydroFeature\Content\Content
     */
    public static function _content() {
        return HydroContainer::i()->get('feature-content');
    }

    /**
     * convenience method for `get('feature-content-schema')`
     *
     * @return \HydroFeature\Content\Schema
     */
    public static function _contentSchema() {
        return HydroContainer::i()->get('feature-content-schema');
    }

    /**
     * convenience method for `get('feature-content-page-type')`
     *
     * @return \HydroFeature\Content\PageType
     */
    public static function _contentPageType() {
        return HydroContainer::i()->get('feature-content-page-type');
    }

    /**
     * convenience method for `get('feature-content-block')`
     *
     * @return \HydroFeature\Content\Block
     */
    public static function _contentBlock() {
        return HydroContainer::i()->get('feature-content-block');
    }

    /**
     * convenience method for `get('feature-shop')`
     *
     * @return \HydroFeature\Shop\Shop
     */
    public static function _shop() {
        return HydroContainer::i()->get('feature-shop');
    }

    /**
     * convenience method for `get('feature-shop-product-type')`
     *
     * @return \HydroFeature\Shop\ProductTypes
     */
    public static function _shopProductType() {
        return HydroContainer::i()->get('feature-shop-product-type');
    }

    /**
     * convenience method for `get('feature-shop-order-builder')`
     *
     * @return \HydroFeature\Shop\OrderBuilder
     */
    public static function _shopOrderBuilder() {
        return HydroContainer::i()->get('feature-shop-order-builder');
    }

    /**
     * convenience method for `get('feature-inventory')`
     *
     * @return \HydroFeature\Shop\Inventory
     */
    public static function _inventory() {
        return HydroContainer::i()->get('feature-inventory');
    }

    /**
     * convenience method for `get('feature-mailboxes')`
     *
     * @return \HydroFeature\Mailer\MailBoxManager
     */
    public static function _mailboxes() {
        return HydroContainer::i()->get('feature-mailboxes');
    }

    /**
     * convenience method for `get('feature-mailings')`
     *
     * @return \HydroFeature\Mailer\MailingStorage
     */
    public static function _mailings() {
        return HydroContainer::i()->get('feature-mailings');
    }

    /**
     * convenience method for `get('feature-message')`
     *
     * @return \HydroFeature\Message\MessageManager
     */
    public static function _message() {
        return HydroContainer::i()->get('feature-message');
    }

    /**
     * convenience method for `get('feature-newsletter')`
     *
     * @return \HydroFeature\Newsletter\Newsletter
     */
    public static function _newsletter() {
        return HydroContainer::i()->get('feature-newsletter');
    }

    /**
     * convenience method for `get('feature-newsletter-registration')`
     *
     * @return \HydroFeature\Newsletter\NewsletterRegistrationHandler
     */
    public static function _newsletterRegistrationHandler() {
        return HydroContainer::i()->get('feature-newsletter-registration');
    }


    /**
     * convenience method for `get('feature-workflow')`
     *
     * @return \HydroFeature\Workflow\WorkflowManager
     */
    public static function _workflow() {
        return HydroContainer::i()->get('feature-workflow');
    }
}