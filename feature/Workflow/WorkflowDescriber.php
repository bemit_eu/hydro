<?php

namespace HydroFeature\Workflow;


class WorkflowDescriber {
    /**
     * @var string reference to the step which is used as start
     */
    public $step_start;
    /**
     * @var callable[]|\HydroFeature\Workflow\WorkflowStep[]
     */
    public $steps;

    public function __construct() {
    }

    public function addStart($id, $step) {
        $this->step_start = $id;

        $this->addStep($id, $step);
        return $this;
    }

    public function addStep($id, $handler) {
        $this->steps[$id] = $handler;

        return $this;
    }

    /**
     * @param $id
     *
     * @throws
     *
     * @return \HydroFeature\Workflow\WorkflowStep|\HydroFeature\Workflow\WorkflowStep[]
     */
    public function inspect($id = false) {
        if(false === $id) {
            $step_ids = array_keys($this->steps);
            $steps = [];
            foreach($step_ids as $step_id) {
                $steps[$step_id] = $this->inspect($step_id);
            }
            return $steps;
        }
        if(!isset($this->steps[$id])) {
            throw new \Exception('NotFound step: ' . $id);
        }
        if(is_callable($this->steps[$id])) {
            $this->steps[$id] = call_user_func($this->steps[$id]);
        }
        return $this->steps[$id];
    }

    public function start($event) {
        return $this->next(false, $this->step_start, $event);
    }

    public function next($from_id, $to_id, $event) {
        if(!$from_id && $to_id !== $this->step_start) {
            throw new \Exception('NotFound: transition from nowhere exists only for start');
        }

        $from = $this->inspect($from_id);
        $from->getTransition($to_id);
        $to = $this->inspect($to_id);
        // todo: here atm workflow

        return [];
    }

    public function requestNext($from_id, $to_id) {

    }
}