<?php

namespace HydroFeature\Workflow;


class WorkflowTransition {
    public $id;
    public $schema;
    public $desc;
    public $validator;

    public function __construct($id, $schema = '', $desc = '', $validator = false) {
        $this->id = $id;
        $this->schema = $schema;
        $this->desc = $desc;
        $this->validator = $validator;
    }
}