<?php

namespace HydroFeature\Workflow;

class WorkflowRunner {
    /**
     * @var \HydroFeature\Workflow\WorkflowDescriber
     */
    protected $workflow;

    /**
     * WorkflowRunner constructor.
     *
     * @param \HydroFeature\Workflow\WorkflowDescriber $workflow
     */
    public function __construct($workflow) {
        $this->workflow = $workflow;
    }

    public function start($event) {
        return $this->workflow->start($event);
    }

    public function next($from, $to, $event) {
        return $this->workflow->next($from, $to, $event);
    }
}