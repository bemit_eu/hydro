<?php

namespace HydroFeature\Message\StorageItem;


use Flood\Component\Storage\StorageItem;
use HydroFeature\Mailer\Mailer;

class Message extends StorageItem {

    public $id;

    /**
     * @var \HydroFeature\Message\StorageItem\MessageUser
     */
    public $recipient;
    public $recipient_id;
    /**
     * @var \HydroFeature\Message\StorageItem\MessageUser
     */
    public $sender;
    public $sender_id;

    public $time;

    public $subject;
    public $text;

    public $type;
    public $hook;

    protected $storage = null;

    public function __construct($data = []) {
        $this->parse(
            $data,
            ['sender', 'recipient'],
            ['id',]
        );

        $this->storage = new \HydroFeature\Message\StorageHandler\Message();
    }

    public function setTime() {
        return $this->time = date('Y-m-d H:i:s');
    }

    public function create() {
        $created = $this->storage->create($this);
        if($created) {
            $this->id = $created;
        }
        return $created;
    }

    /**
     * @param \HydroFeature\Mailer\MailBoxItem $mailbox
     *
     * @return
     */
    public function send($mailbox) {
        $mailer = new Mailer($mailbox);
        return $mailer->message($this->subject);
    }
}