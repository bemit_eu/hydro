<?php

namespace HydroFeature\Message\StorageItem;


class MessageUser {
    /**
     * @var bool|string|array either a simple string as name or an array ['Firstname', 'Middlename', 'Lastname']
     */
    public $name = false;
    /**
     * @var bool|string
     */
    public $email = false;
    /**
     * @var bool|string when matched to an registered user, the ID of him will be here
     */
    public $user_id = false;
}