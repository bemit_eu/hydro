<?php

namespace HydroFeature\Workflow;


class WorkflowStep {
    public $transitions;

    public function __construct() {
    }

    public function setHandler($handler) {
    }

    public function setNext($transition) {
        $this->transitions = $transition;
    }

    public function setValidator($validator) {
    }

    public function setDescription($description) {
    }

    public function getTransition($transition_id) {
    }
}