<?php

namespace HydroFeature\Workflow;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * @Service(id="feature-workflow", executed=false)
 * @package HydroFeature\Workflow
 */
class WorkflowManager {
    /**
     * @var WorkflowDescriber[][]
     */
    protected $workflows;

    /**
     * @param string $id name of the workflow in general, could be existing multiple times
     * @param string $hook name of the hook this workflow applies to, when executing in a hook scope (e.g. shop order) it tries to find a specialized one or gets the default
     *
     * @return \HydroFeature\Workflow\WorkflowDescriber
     */
    public function describe($id, $hook = 'default') {
        if(!isset($this->workflows[$id])) {
            $this->workflows[$id] = [];
        }
        $this->workflows[$id][$hook] = new WorkflowDescriber();
        return $this->workflows[$id][$hook];
    }

    /**
     * @param string $id
     * @param string $hook
     *
     * @throws \Exception
     * @return \HydroFeature\Workflow\WorkflowRunner
     */
    public function trigger($id, $hook = 'default') {
        if(isset($this->workflows[$id][$hook])) {
            return new WorkflowRunner($this->workflows[$id][$hook]);
        }

        if('default' !== $hook) {
            return $this->trigger($id);
        }

        throw new \Exception('WorkflowNotFound: ' . $id . ' for ' . $hook);
    }

    /**
     * @param $id
     * @param string $hook
     *
     * @throws \Exception
     * @return \HydroFeature\Workflow\WorkflowDescriber
     */
    public function inspect($id, $hook = 'default') {
        if(isset($this->workflows[$id][$hook])) {
            return $this->workflows[$id][$hook];
        }

        if('default' !== $hook) {
            return $this->inspect($id);
        }

        throw new \Exception('WorkflowNotFound: ' . $id . ' for ' . $hook);
    }
}