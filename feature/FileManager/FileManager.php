<?php

namespace HydroFeature\FileManager;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 *
 * @Service(id="feature-file-manager", executed=false)
 * @package HydroFeature\Template
 */
class FileManager {
    /**
     * @var string do not use `__DIR__.'/../folder'` but `dirname(__DIR__).'/folder'`
     */
    static public $upload_root = '';

    protected function createAbsPath($filenamedir) {
        $path = [];
        foreach(explode('/', $filenamedir) as $part) {
            // ignore parts that have no value
            if(empty($part) || $part === '.') {
                continue;
            }

            if($part !== '..') {
                array_push($path, $part);
            } else if(count($path) > 0) {
                array_pop($path);
            } else {
                return false;
            }
        }

        return DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, $path);
    }

    protected function checkScope($dir) {
        $realpath = realpath($dir);

        // security check: only allow if wanted dir is child of upload root and not above it
        return (0 === strpos($realpath . DIRECTORY_SEPARATOR, static::$upload_root));
    }

    protected function checkNonExistingScope($dir) {
        $realpath = $this->createAbsPath($dir);

        // security check: only allow if wanted dir is child of upload root and not above it
        return (0 === strpos($realpath . DIRECTORY_SEPARATOR, static::$upload_root));
    }

    public function handleFileUpload($file, $dir) {
        $target = static::$upload_root;
        if($dir) {
            if(!$this->checkScope($target . $dir)) {
                throw new \Exception('dir-not-in-root');
            }
            $target .= $dir;
        }

        $target .= '/' . $file['name'];

        if(is_file($target)) {
            return -1;
        }

        if(!is_dir(dirname($target))) {
            mkdir(dirname($target));
        }
        if(move_uploaded_file($file['tmp_name'], $target)) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteFile($file) {
        $target = static::$upload_root;

        if($file) {
            if(!$this->checkScope($target . $file)) {
                throw new \Exception('file-not-in-root');
            }
            $target .= $file;
        }

        if(is_dir($target)) {
            return false;
        }

        if(is_file($target)) {
            unlink($target);
        }

        if(!is_file($target)) {
            return true;
        }

        return false;
    }

    public function renameFile($file, $to) {
        $source = static::$upload_root;
        $target = static::$upload_root;

        if($file && $to) {
            if(!$this->checkScope($source . $file)) {
                throw new \Exception('file-not-in-root');
            }
            $source .= $file;

            if(!$this->checkNonExistingScope($target . $to)) {
                throw new \Exception('file-not-in-root');
            }
            $target .= $to;
        } else {
            return false;
        }

        if(
            is_dir($source) || is_dir($target) ||
            is_file($target)
        ) {
            return false;
        }

        if(is_file($source)) {
            rename($source, $target);
        }

        if(!is_file($source) && is_file($target)) {
            return true;
        }

        return false;
    }

    /**
     * @param $dir
     *
     * @return array
     * @throws \Exception
     */
    public function list($dir) {
        $target = static::$upload_root;
        $dirs = [];
        $files = [];

        if($dir) {
            if(!$this->checkScope($target . $dir)) {
                throw new \Exception('dir-not-in-root');
            }
            $target .= $dir;
        }

        $info = new \DirectoryIterator($target);
        foreach($info as $fileinfo) {
            if($fileinfo->isDir() && !$fileinfo->isDot()) {
                $dirs[] = [
                    'name' => $fileinfo->getFilename(),
                ];
            } else if(!$fileinfo->isDot()) {
                $files[] = [
                    'name' => $fileinfo->getFilename(),
                    'mime' => mime_content_type($fileinfo->getRealPath()),
                ];
            }
        }

        return [
            'dir' => $dirs,
            'file' => $files,
        ];
    }

    public function createDir($dir) {
        $target = static::$upload_root;

        if($dir) {
            if(!$this->checkNonExistingScope($target . $dir)) {
                throw new \Exception('dir-not-in-root');
            }
            $target .= $dir;
        } else {
            return false;
        }

        if(is_file($target) || is_dir($target)) {
            return true;
        }

        mkdir($target);

        if(is_file($target) || is_dir($target)) {
            return true;
        }

        return false;
    }

    public function deleteDir($dir) {
        $target = static::$upload_root;

        if($dir) {
            if(!$this->checkScope($target . $dir)) {
                throw new \Exception('dir-not-in-root');
            }
            $target .= $dir;
        } else {
            return false;
        }

        if(is_file($target)) {
            return false;
        }

        if(is_dir($target)) {
            if(!empty(array_diff(scandir($target), ['.', '..']))) {
                throw new \Exception('dir-not-empty');
            }

            rmdir($target);
        }

        if(!is_dir($target)) {
            return true;
        }

        return false;
    }

    public function renameDir($dir, $to) {
        $source = static::$upload_root;
        $target = static::$upload_root;

        if($dir && $to) {
            if(!$this->checkScope($source . $dir)) {
                throw new \Exception('dir-not-in-root');
            }
            $source .= $dir;

            if(!$this->checkNonExistingScope($target . $to)) {
                throw new \Exception('dir-not-in-root');
            }
            $target .= $to;
        } else {
            return false;
        }

        if(
            is_file($source) || is_file($target) ||
            is_dir($target)
        ) {
            return false;
        }

        if(is_dir($source)) {
            rename($source, $target);
        }

        if(!is_dir($source) && is_dir($target)) {
            return true;
        }

        return false;
    }
}