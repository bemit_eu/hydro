<?php

namespace HydroFeature\Template;

class MarkDown {

    /**
     * @var \Parsedown
     */
    public $parsedown;

    /**
     * Markdown constructor.
     */
    public function __construct() {
        $this->parsedown = new \Parsedown();
    }

    /**
     * @param $path
     *
     * @return bool|string
     */
    public function textFile($path) {
        if(false !== ($file_content = file_get_contents($path))) {
            return $this->text($file_content);
        } else {
            return false;
        }
    }

    /**
     * @param $text
     *
     * @return string
     */
    public function text($text) {
        return $this->parsedown->text($text);
    }
}