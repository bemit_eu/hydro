<?php

namespace HydroFeature\Template;

trait TemplateData {
    private $template_data = [];

    /**
     * @param $key
     * @param $val
     */
    public function assignTemplateData($key, $val) {
        $this->template_data[$key] = $val;
    }

    /**
     * @param $key
     * @param $val
     */
    public function assignTemplateDataByRef($key, &$val) {
        $this->template_data[$key] = &$val;
    }

    /**
     * Resets the assigned template data
     */
    public function cleanTemplateData() {
        $this->template_data = [];
    }
}