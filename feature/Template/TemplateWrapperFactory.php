<?php

namespace HydroFeature\Template;

/**
 * Interface TemplateWrapperFactory for wrapping the `load` of a template file or `createTemplate` for a string template and passing it to the actual render process
 */
interface TemplateWrapperFactory {
    /**
     * @param \Twig_Environment $twig_env
     * @param string            $template
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     *
     * @return \Twig_TemplateWrapper|\Twig_Template $template_wrapper
     */
    public function __invoke($twig_env, $template);
}