<?php

namespace HydroFeature\Template;

use Hydro\Container;
use Flood\Component\Sonar\Annotation\Service as Service;

/**
 *
 * @Service(id="feature-template", executed=false)
 * @package HydroFeature\Template
 */
class Template {
    /**
     * Adding Data to the container instance means they are used on every `render()`
     */
    use TemplateData;

    /**
     * @var \Twig_Loader_Filesystem
     */
    public $twig_loader = null;
    /**
     * @var \Twig_Environment
     */
    public $twig_env = null;

    /**
     * @var MarkDown
     */
    protected $md_parser;

    public function __construct() {
        try {
            $twig_config = Container::_config()->template()->envConfig();

            if(!is_array($twig_config)) {
                throw new \Exception('Template: env-config not valid.');
            }

            if(isset($twig_config['cache'])) {
                $twig_config['cache'] = Container::_config()->serverPath(true) . $twig_config['cache'];
            }

            $this->twig_loader = new \Twig\Loader\FilesystemLoader();

            $this->twig_env = new \Twig\Environment($this->twig_loader, $twig_config);

            if(Container::_config()->template()->debug()) {
                // make dump() and more available
                $this->twig_env->addExtension(new \Twig\Extension\DebugExtension());
                $this->twig_env->enableDebug();
            }

            $this->md_parser = new MarkDown();
        } catch(\Exception $e) {
            $msg = 'Hydro.Template: init ' . $e->getMessage();
            if(\Hydro\Hydro::$debug) {
                error_log($msg);
            }
            exit(1);
        }
    }

    /**
     * @param        $path
     * @param string $namespace
     */
    public function addPath($path, $namespace = \Twig\Loader\FilesystemLoader::MAIN_NAMESPACE) {
        try {
            $this->twig_loader->addPath(Container::_config()->serverPath(true) . $path, $namespace);
        } catch(\Twig\Error\LoaderError $e) {
            if(\Hydro\Hydro::$debug) {
                echo $e->getMessage();
            } else {
                error_log($e->getMessage());
            }
        }
    }

    /**
     * @param array $data
     */
    public function addPathArray($data) {
        foreach($data as $namespace => $paths) {
            foreach($paths as $path) {
                if(is_string($namespace) && '' !== trim($namespace) && !is_numeric($namespace)) {
                    $this->addPath($path, $namespace);
                } else {
                    $this->addPath($path);
                }
            }
        }
    }

    public function md() {
        return $this->md_parser;
    }

    /**
     * Extend twig with custom functions
     *
     * @see https://twig.symfony.com/doc/2.x/advanced.html#functions
     *
     * @param $name
     * @param $callable
     */
    public function addFunction($name, $callable) {
        $this->twig_env->addFunction(new \Twig\TwigFunction($name, $callable));
    }

    /**
     * @param string $template relative path to template file
     * @param array $data
     *
     * @return string
     */
    public function render($template = '', $data = []) {
        return $this->renderFile($template, $data);
    }

    /**
     * @param string $template relative path to template file
     * @param array $data
     *
     * @return string
     */
    public function renderFile($template = '', $data = []) {
        return $this->renderInternal(
            new class() implements TemplateWrapperFactory {
                public function __invoke($twig_env, $template) {
                    return $twig_env->load($template);
                }
            },
            $template,
            $data
        );
    }

    /**
     * @param string $template relative path to template file
     * @param array $data
     *
     * @return string
     */
    public function renderString($template = '', $data = []) {
        return $this->renderInternal(
            new class() implements TemplateWrapperFactory {
                public function __invoke($twig_env, $template) {
                    return $twig_env->createTemplate($template);
                }
            },
            $template,
            $data
        );
    }

    /**
     * @param TemplateWrapperFactory $wrapper_factory
     * @param string $template
     * @param array $data
     *
     * @return string
     */
    protected function renderInternal($wrapper_factory, $template, $data) {
        try {
            $template_wrapper = $wrapper_factory($this->twig_env, $template);

            return $template_wrapper->render(array_merge($this->template_data, $data));
        } catch(\Twig\Error\LoaderError $e) {
            throw new \Exception('Twig_Error_Loader: ' . $e->getMessage());
        } catch(\Twig\Error\RuntimeError $e) {
            throw new \Exception('Twig_Error_Runtime: ' . $e->getMessage());
        } catch(\Twig\Error\SyntaxError $e) {
            throw new \Exception('Twig_Error_Syntax: ' . $e->getMessage());
        } catch(\Throwable $e) {
            throw new \Exception('Twig_Env AnyError: ' . $e->getMessage());
        } catch(\Exception $e) {
            $msg = 'Hydro.Template.render: ' . $e->getMessage();
            if(\Hydro\Hydro::$debug) {
                error_log($msg);
            }

            return '';
        }
    }
}
