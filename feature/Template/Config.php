<?php

namespace HydroFeature\Template;

use Hydro\Container;
use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * Short description for class
 *
 * Long description for class
 *
 * @Service(id="config-template", executed=false)
 * @category
 * @package
 * @author     Original Author mb.project@bemit.eu
 * @link
 * @copyright  2017
 * @since      Version
 * @version    Release: @package_version@
 */
class Config extends \Flood\Component\Config\Config {
    /**
     * @return bool|null
     */
    public function envConfig() {
        return $this->getData(['template', 'env-config']);
    }

    /**
     * @return bool|null
     */
    public function debug() {
        return $this->getData(['template', 'debug']);
    }
}