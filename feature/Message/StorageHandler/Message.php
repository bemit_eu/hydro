<?php

namespace HydroFeature\Message\StorageHandler;

use Hydro\Container;

class Message {
    protected static $table = 'message';
    protected static $column = ['ID', 'time', 'recipient_id', 'recipient', 'sender_id', 'sender', 'subject', 'text', 'type', 'hook'];

    public function __construct() {
    }

    /**
     * @param \HydroFeature\Message\StorageItem\Message $message
     * @param bool                                      $tried
     *
     * @return bool|string
     */
    public function create($message, $tried = false) {
        $id = Container::_storageData()->randomId();

        $inserted = Container::_storageData()->insert(
            static::$table,
            [
                'ID' => $id,
                'recipient_id' => $message->recipient_id,
                'recipient' => json_encode($message->recipient, true),
                'sender_id' => $message->sender_id,
                'sender' => json_encode($message->sender, true),
                'subject' => $message->subject,
                'text' => $message->text,
                'type' => $message->type,
                'hook' => $message->hook,
            ]
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->create($message, $tried);
    }

    public function getById($id) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['ID' => $id]);
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(\Exception $e) {
            error_log('Message Storage getById error: ' . $e->getMessage());

            return false;
        }
    }

    public function getByRecipient($recipient_id) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['recipient_id' => $recipient_id]);
        try {
            $stmt->execute();

            $res = $stmt->fetchAll();
            $messages = [];
            foreach($res as $message) {
                $messages[] = new \HydroFeature\Message\StorageItem\Message($message);
            }

            return $messages;
        } catch(\Exception $e) {
            error_log('Message Storage getByRecipient error: ' . $e->getMessage());

            return false;
        }
    }

    public function getBySender($sender_id) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['sender_id' => $sender_id]);
        try {
            $stmt->execute();

            $res = $stmt->fetchAll();
            $messages = [];
            foreach($res as $message) {
                $messages[] = new \HydroFeature\Message\StorageItem\Message($message);
            }

            return $messages;
        } catch(\Exception $e) {
            error_log('Message Storage getBySender error: ' . $e->getMessage());

            return false;
        }
    }

    public function getAll($condition = []) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, $condition);
        try {
            $stmt->execute();

            $res = $stmt->fetchAll();
            $messages = [];
            foreach($res as $message) {
                $messages[] = new \HydroFeature\Message\StorageItem\Message($message);
            }

            return $messages;
        } catch(\Exception $e) {
            error_log('Message Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }
}