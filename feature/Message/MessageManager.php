<?php

namespace HydroFeature\Message;

use Flood\Component\Sonar\Annotation\Service as Service;
use HydroFeature\Message\StorageItem\Message;
use HydroFeature\Message\StorageItem\MessageUser;

/**
 * @Service(id="feature-message", executed=false)
 * @package HydroFeature\Message
 */
class MessageManager {
    protected $storage;

    public function __construct() {
        $this->storage = new StorageHandler\Message();
    }

    /**
     * @return \HydroFeature\Message\StorageItem\Message
     */
    public function create() {
        return new Message();
    }

    /**
     * @return \HydroFeature\Message\StorageItem\MessageUser
     */
    public function user() {
        return new MessageUser();
    }

    public function getMessages($hook = false) {
        if($hook) {
            $messages = $this->storage->getAll(['hook' => $hook]);
        } else {
            $messages = $this->storage->getAll();
        }
        if($messages) {

            return $messages;
        }

        return [];
    }

    public function getById($id) {
        $messages = $this->storage->getById($id);
        if($messages) {
            try {
                return new StorageItem\Message($messages);
            } catch(\Exception $e) {
                error_log('HydroFeature\Message: data is invalid for id `' . $id . '`');

                return false;
            }
        }

        error_log('HydroFeature\Message: could not get data for id `' . $id . '`');
        return [];
    }
}