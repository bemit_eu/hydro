<?php

namespace HydroFeature\Mailer;

class Mailer {
    /**
     * @var \Swift_SmtpTransport
     */
    protected $transport;
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;
    /**
     * @var \Swift_Plugins_Loggers_ArrayLogger
     */
    protected $logger;

    /**
     * @param \HydroFeature\Mailer\MailBoxItem $mailbox
     */
    public function __construct($mailbox) {
        try {
            // string null to null conversion because of env settings
            $this->transport = new \Swift_SmtpTransport($mailbox->host, $mailbox->port, 'null' === $mailbox->security ? null : $mailbox->security);
            $this->transport->setUsername($mailbox->user);
            $this->transport->setPassword($mailbox->pass);
            $this->mailer = new \Swift_Mailer($this->transport);

        } catch(\Swift_TransportException $e) {
            error_log('Mailer error on build transport for mailbox ' . $mailbox->id);
        }
    }

    public function message($subject) {
        $this->logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $this->mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($this->logger));

        return new \Swift_Message($subject);
    }

    /**
     * @param \Swift_Message $message
     *
     * @return int|bool
     */
    public function send(\Swift_Message $message) {
        try {
            return $this->mailer->send($message);
        } catch(\Swift_TransportException $e) {

            error_log('Mailer send error: ' . $e->getMessage());

            return false;
        }
    }
}