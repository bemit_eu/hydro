<?php

namespace HydroFeature\Mailer;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * @Service(id="feature-mailboxes", executed=false)
 * @package HydroFeature\Shop
 */
class MailBoxItem {
    /**
     * @var string with what this mailbox item has been registered to MailBoxManager
     */
    public $id;
    public $host;
    public $port;
    public $security;
    public $user;
    public $pass;
    /**
     * @var array|string when string it is only one email allowed, on array used index `name` for the ONE name and an string|array `addresses` for all email-addresses
     */
    public $from;

    /*
     * Info From `class Swift_Mime_SimpleMessage`:
     * You may pass an array of addresses if this message is from multiple people.
     *
     * If $name is passed and the first parameter is a string, this name will be
     * associated with the address.
     *
     * @param string|array $addresses
     * @param string       $name      optional
     */

    public function __construct($raw, $id = false) {
        $env_id = 'MAILBOX__' . strtoupper($id) . '__';

        if($id) {
            $this->id = $id;
        }

        if($id && getenv($env_id . 'HOST')) {
            $this->host = getenv($env_id . 'HOST');
        } else if(isset($raw['host'])) {
            $this->host = $raw['host'];
        }

        if($id && getenv($env_id . 'PORT')) {
            $this->port = getenv($env_id . 'PORT');
        } else if(isset($raw['port'])) {
            $this->port = $raw['port'];
        }

        if($id && getenv($env_id . 'SECURITY')) {
            $this->security = getenv($env_id . 'SECURITY');
        } else if(isset($raw['security'])) {
            $this->security = $raw['security'];
        }

        if($id && getenv($env_id . 'USER')) {
            $this->user = getenv($env_id . 'USER');
        } else if(isset($raw['user'])) {
            $this->user = $raw['user'];
        }

        if($id && getenv($env_id . 'PASS')) {
            $this->pass = getenv($env_id . 'PASS');
        } else if(isset($raw['pass'])) {
            $this->pass = $raw['pass'];
        }

        if($id && getenv($env_id . 'FROM')) {
            $this->from = getenv($env_id . 'FROM');
        } else if(isset($raw['from'])) {
            $this->from = $raw['from'];
        }
    }

    public function getFromEmail() {
        if(is_string($this->from)) {
            return $this->from;
        }

        if(isset($this->from['addresses'])) {
            return $this->from['addresses'];
        }

        return false;
    }

    public function getFromName() {
        if(is_string($this->from)) {
            return null;
        }

        if(isset($this->from['name'])) {
            return $this->from['name'];
        }

        return null;
    }
}