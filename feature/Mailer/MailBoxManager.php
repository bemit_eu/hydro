<?php

namespace HydroFeature\Mailer;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * @Service(id="feature-mailboxes", executed=false)
 * @package HydroFeature\Shop
 */
class MailBoxManager {
    static $store = [];

    public static function register($id, $path) {
        static::$store[$id] = [
            'loaded' => false,
            'path'   => $path,
        ];
    }

    /**
     * @param $id
     *
     * @return bool|\HydroFeature\Mailer\MailBoxItem
     */
    public static function get($id) {
        if(!isset(static::$store[$id])) {
            return false;
        }
        if(static::$store[$id]['loaded']) {
            return static::$store[$id]['value'];
        }

        $raw_mailbox = \Flood\Component\Json\Json::decodeFile(static::$store[$id]['path'], true);
        if($raw_mailbox) {
            $mailbox = new MailBoxItem($raw_mailbox,$id);
            static::$store[$id]['loaded'] = true;
            static::$store[$id]['value'] = $mailbox;

            return $mailbox;
        }

        return false;
    }

    public static function getList() {
        return array_keys(static::$store);
    }
}