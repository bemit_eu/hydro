<?php

namespace HydroFeature\Newsletter\StorageHandler;

use Hydro\Container;
use function PHPSTORM_META\type;

class NewsletterUser {
    protected static $table = 'marketing_newsletter';
    protected static $column = ['ID', 'email', 'state', 'token', 'subscribing_time', 'subscribing_ip', 'unsubscribe_time', 'optin_ip', 'optin_time', 'state_log', 'hook', 'source', 'optin_reference'];
    protected static $column_reduced = ['ID', 'email', 'state', 'subscribing_time', 'unsubscribe_time', 'optin_time'];

    public function __construct() {
    }

    /**
     * @param \HydroFeature\Newsletter\StorageItem\NewsletterUser $user
     * @param bool                                                $tried
     *
     * @return bool
     */
    public function create($user, $tried = false) {
        $data = (array)$user;
        unset($data['id']);

        $id = Container::_storageData()->randomId();
        $data['ID'] = $id;
        $inserted = Container::_storageData()->insert(
            static::$table,
            $data
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->create($user, $tried);
    }

    /**
     * @param $email
     *
     * @return bool|\HydroFeature\Newsletter\StorageItem\NewsletterUser
     */
    public function getByEmail($email) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['email' => $email]);
        try {
            $stmt->execute();

            $res = $stmt->fetch();
            if($res) {
                return new \HydroFeature\Newsletter\StorageItem\NewsletterUser($res);
            }
        } catch(\Exception $e) {
            error_log('Order Storage getById error: ' . $e->getMessage());
        }

        return false;
    }

    public function updateByEmail($email, $data) {
        try {
            if(isset($data['state_log'])) {
                $data['state_log'] = json_encode($data['state_log']);
            }

            return Container::_storageData()->update(static::$table,
                $data,
                ['email' => $email]);
        } catch(\Exception $e) {
            error_log('Order Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    /**
     * @param array $condition
     *
     * @return \HydroFeature\Newsletter\StorageItem\NewsletterUser[]
     */
    public function getAll($condition = []) {
        if(!is_array($condition)) {
            $condition = [];
        }
        $stmt = Container::_storageData()->select(static::$column, static::$table, $condition);
        try {
            $stmt->execute();

            $res = $stmt->fetchAll();
            $users = [];
            foreach($res as $user) {
                $users[] = new \HydroFeature\Newsletter\StorageItem\NewsletterUser($user);
            }

            return $users;
        } catch(\Exception $e) {
            error_log('Order Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }

    /**
     *
     * @param array $condition
     *
     * @return array|bool
     * @todo implement reduce/condition/order correctly and not only order by optin_time
     */
    public function getAllReduced($condition = []) {
        if(!is_array($condition)) {
            $condition = [];
        }
        $condition = ['ID' => '~%', 'ORDER' => ['optin_time' => 'DESC']];
        $stmt = Container::_storageData()->select(static::$column_reduced, static::$table, $condition);
        try {
            $stmt->execute();

            $res = $stmt->fetchAll();
            $users = [];
            foreach($res as $user) {
                $users[] = new \HydroFeature\Newsletter\StorageItem\NewsletterUser($user);
            }

            return $users;
        } catch(\Exception $e) {
            error_log('Order Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }
}
