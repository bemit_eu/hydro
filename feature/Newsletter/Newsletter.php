<?php

namespace HydroFeature\Newsletter;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 *
 * @Service(id="feature-newsletter", executed=true)
 * @package HydroFeature\Newsletter
 */
class Newsletter {
    protected $storage;

    public function __construct() {
        $this->storage = new StorageHandler\NewsletterUser();
    }

    public function getUsers($condition = []) {
        $groups = $this->storage->getAllReduced($condition);

        if(is_array($groups)) {
            return $groups;
        }

        return [];
    }
}