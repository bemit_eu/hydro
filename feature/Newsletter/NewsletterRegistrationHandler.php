<?php

namespace HydroFeature\Newsletter;

use Flood\Component\Sonar\Annotation\Service as Service;
use Hydro\Container;
use HydroFeature\Container as FeatureContainer;
use HydroFeature\Mailer\MailBoxItem;
use HydroFeature\Mailer\Mailer;
use HydroFeature\Newsletter\StorageItem\NewsletterUser;

/**
 *
 * @Service(id="feature-newsletter-registration", executed=true)
 * @package HydroFeature\Newsletter
 */
class NewsletterRegistrationHandler {
    const SUBSCRIBING = 'subscribing';
    const SUBSCRIBED = 'subscribed';
    const UNSUBSCRIBED = 'unsubscribed';
    public $locale;
    public $email;
    public $token;
    protected $mailbox;
    protected $storage;

    public function __construct() {
        $this->storage = new StorageHandler\NewsletterUser();
    }

    /**
     * @param callable    $doi_email receives the NewsletterUser as param, must return html for double optin email
     * @param MailBoxItem $mailbox
     *
     * @return bool
     */
    public function subscribe($doi_email, $mailbox) {
        $user = $this->storage->getByEmail($this->email);
        $new = false;
        if($user) {
            // user already exists
            if(static::SUBSCRIBED === $user->state) {
                // already subscribed
                return true;
            }

            if(static::SUBSCRIBING === $user->state) {
                // already in procedure, send DOI again, no changes to token or anything
                return $this->sendDoiMail($doi_email, $mailbox, $user);
            }

            if(static::UNSUBSCRIBED === $user->state) {
                // unsubscribed sometime, start new subscribing
                $user = $this->stateLogUpdate($user, static::SUBSCRIBING);
            }
        } else {
            // new user
            $user = new NewsletterUser();
            $user->email = $this->email;
            $new = true;
        }

        $user->state = static::SUBSCRIBING;
        $user->subscribing_ip = $this->fetchIp();
        $user->subscribing_time = date('Y-m-d H:i:s');
        $user->token = Container::_storageData()->randomId();

        $success = false;
        if($new) {
            $inserted = $this->storage->create($user);
            if($inserted) {
                $success = true;
            }
        } else {
            $updated = $this->storage->updateByEmail($user->email, (array)$user);
            if(is_int($updated)) {
                $success = true;
            }
        }

        if(!$success) {
            return false;
        }

        return $this->sendDoiMail($doi_email, $mailbox, $user);
    }

    public function confirm($token) {
        $payload = $this->confirmTokenDestruct($token);
        if(!isset($payload['email'])) {
            error_log('Payload E-Mail is not valid');

            return false;
        }
        if(!isset($payload['token'])) {
            error_log('Payload Token is not valid');

            return false;
        }

        $user = $this->storage->getByEmail($payload['email']);
        if($user) {
            /*if($user->token !== $payload['token']) {
                error_log('Payload Token is not valid with users saved token.');

                return false;
            }*/
            if(static::SUBSCRIBED === $user->state) {
                // already subscribed
                return true;
            }

            if(static::UNSUBSCRIBED === $user->state) {
                // unsubscribed sometime, must start again with subscribing not with confirm
                return false;
            }

            $user = $this->stateLogUpdate($user, static::SUBSCRIBED);
            $user->state = static::SUBSCRIBED;
            $user->optin_ip = $this->fetchIp();
            $user->optin_time = date('Y-m-d H:i:s');

            $res = $this->storage->updateByEmail($user->email, (array)$user);

            return is_int($res);
        }

        return false;
    }

    public function unsubscribe($token) {
        $payload = $this->confirmTokenDestruct($token);
        if(!isset($payload['email'])) {
            error_log('Payload E-Mail is not valid');

            return false;
        }
        if(!isset($payload['token'])) {
            error_log('Payload Token is not valid');

            return false;
        }
        $user = $this->storage->getByEmail($payload['email']);
        /*if($user->token !== $payload['token']) {
            error_log('Payload Token is not valid with users saved token.');

            return false;
        }*/

        $user = $this->stateLogUpdate($user, static::UNSUBSCRIBED);
        $user->state = static::UNSUBSCRIBED;
        $user->unsubscribe_time = date('Y-m-d H:i:s');

        $res = $this->storage->updateByEmail($user->email, (array)$user);

        return is_int($res);
    }

    public function confirmTokenBuild($token) {
        return urlencode(base64_encode(json_encode([
            'email' => $this->email,
            'token' => $token,
        ])));
    }

    /**
     * @param $token
     *
     * @return array with indices `email` and `token`
     */
    public function confirmTokenDestruct($token) {
        return json_decode(base64_decode(urldecode($token)), true);
    }

    /**
     * @param                $doi_email
     * @param MailBoxItem    $mailbox
     * @param NewsletterUser $user
     *
     * @return bool|int
     */
    public function sendDoiMail($doi_email, $mailbox, $user) {
        $mailer = new Mailer($mailbox);
        $message = $mailer->message('Gerharz Newsletter Anmeldung');
        $message->setFrom($mailbox->getFromEmail(), $mailbox->getFromName())
            ->setTo($user->email)
            ->setBody($doi_email($user, $this->confirmTokenBuild($user->token)), 'text/html');

        return $mailer->send($message);
    }

    /**
     * @return string|null
     */
    public function fetchIp() {
        if(filter_has_var(INPUT_SERVER, 'REMOTE_ADDR')) {
            return filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        }
        if(filter_has_var(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR')) {
            return array_pop(explode(',', filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR', FILTER_REQUIRE_ARRAY)));
        }

        return null;
    }

    public function setLocale($locale) {
        $this->locale = $locale;
    }

    /**
     * @param NewsletterUser $user
     * @param                $to_state
     *
     * @return NewsletterUser
     */
    protected function stateLogUpdate($user, $to_state) {
        if(!is_array($user->state_log)) {
            $user->state_log = [];
        }

        $user->state_log[] = [
            'time'     => time(),
            'snapshot' => (array)$user,
            'from'     => $user->state,
            'to'       => $to_state,
        ];

        return $user;
    }
}
