<?php

namespace HydroFeature\Newsletter\StorageItem;

class NewsletterUser {
    const SOURCE_MANUAL = 'manual';

    public $id;
    public $email;
    public $state;
    public $token;
    public $subscribing_time;
    public $subscribing_ip;
    public $unsubscribe_time;
    public $optin_ip;
    public $optin_time;
    public $state_log;
    public $hook;
    public $source;
    public $optin_reference;

    public function __construct($raw = []) {
        if(isset($raw['ID'])) {
            $this->id = $raw['ID'];
        }
        if(isset($raw['email'])) {
            $this->email = $raw['email'];
        }
        if(isset($raw['state'])) {
            $this->state = $raw['state'];
        }
        if(isset($raw['token'])) {
            $this->token = $raw['token'];
        }
        if(isset($raw['subscribing_time'])) {
            $this->subscribing_time = $raw['subscribing_time'];
        }
        if(isset($raw['subscribing_ip'])) {
            $this->subscribing_ip = $raw['subscribing_ip'];
        }
        if(isset($raw['unsubscribe_time'])) {
            $this->unsubscribe_time = $raw['unsubscribe_time'];
        }
        if(isset($raw['optin_ip'])) {
            $this->optin_ip = $raw['optin_ip'];
        }
        if(isset($raw['optin_time'])) {
            $this->optin_time = $raw['optin_time'];
        }
        if(isset($raw['state_log'])) {
            if(is_string($raw['state_log'])) {
                $this->state_log = json_decode($raw['state_log'], true);
            } else {
                $this->state_log = $raw['state_log'];
            }
        }
        if(isset($raw['hook'])) {
            $this->hook = $raw['hook'];
        }
        if(isset($raw['source'])) {
            $this->source = $raw['source'];
        }
        if(isset($raw['optin_reference'])) {
            $this->optin_reference = $raw['optin_reference'];
        }
    }
}