<?php

namespace HydroFeature\Shop\Order;

class ExceptionOutOfStock extends \Exception {
    public $products = [];
}