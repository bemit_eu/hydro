<?php

namespace HydroFeature\Shop\Order;

class ExceptionNotBuyAble extends \Exception {
    public $products = [];
}