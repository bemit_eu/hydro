<?php

namespace HydroFeature\Shop\Order;

class ProductListItem {
    /**
     * @var \HydroFeature\Shop\StorageItem\ProductData
     */
    public $product;
    public $qty;
    public $custom;
}
