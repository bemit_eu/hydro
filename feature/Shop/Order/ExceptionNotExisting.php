<?php

namespace HydroFeature\Shop\Order;

class ExceptionNotExisting extends \Exception {
    public $products = [];
}