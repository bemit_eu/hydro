<?php

namespace HydroFeature\Shop\StorageAbstraction;

use HydroFeature\Container as FeatureContainer;

class Product {
    protected static $product_data_cache = [];

    protected $cache;

    public function __construct($cache = true) {
        $this->cache = $cache;
    }

    /**
     * @param $data_id
     * @param bool $stock
     *
     * @return bool|\HydroFeature\Shop\StorageItem\ProductData
     */
    public function getProductData($data_id, $stock = false) {
        $product = FeatureContainer::_shop()->getProductData($data_id, $stock);
        // todo: add cache like for `loadProductsByNos`
        $this->handleLoadedProduct($product, $stock);

        return $product;
    }

    /**
     * @param $hook
     * @param $locale
     * @param bool $hidden
     * @param bool $stock
     * @param array $condition
     *
     * @return \HydroFeature\Shop\StorageItem\ProductData[]
     */
    public function getProductsForLocale($hook, $locale, $stock = false, $hidden = false, $condition = []) {
        $products = FeatureContainer::_shop()->getProductsForLocale($hook, $locale, $stock, $hidden, $condition);
        // todo: add cache like for `loadProductsByNos`
        if(is_array($products)) {
            foreach($products as &$product) {
                $this->handleLoadedProduct($product, $stock);
            }

            return $products;
        }

        return [];
    }

    /**
     * @param string|array $product_no
     * @param $active_locale
     * @param $stock
     *
     * @return \HydroFeature\Shop\StorageItem\ProductData[]|bool
     */
    public function loadProductsByNos($product_no, $active_locale, $stock = false) {
        if(!$product_no) {
            error_log('Found empty product_no for loadFullProductByProductNo');
            return false;
        }
        if(is_string($product_no)) {
            $product_no = [$product_no];
        }

        /**
         * @var array $to_find
         */
        $to_find = $product_no;
        if($this->cache) {
            $to_find = [];
            foreach($product_no as $product_n) {
                if(!array_key_exists($product_n, static::$product_data_cache)) {
                    $to_find[] = $product_n;
                }
            }
        }

        $ret = [];
        if(count($to_find) > 0) {
            $products = FeatureContainer::_shop()->getProductByProductNo($active_locale, $to_find, $stock);
            if($products) {
                foreach($products as $product) {
                    $this->handleLoadedProduct($product, $stock);

                    if($this->cache) {
                        static::$product_data_cache[$product->getProductNo()] = $product;
                    } else {
                        $ret[$product->getProductNo()] = $product;
                    }
                }
            }
        }

        if($this->cache) {
            $ret = [];
            foreach($product_no as $product_n) {
                if(isset(static::$product_data_cache[$product_n])) {
                    $ret[$product_n] = static::$product_data_cache[$product_n];
                }
            }
        }

        return $ret;
    }

    /**
     * @param \HydroFeature\Shop\StorageItem\ProductData $product
     * @param bool $stock
     */
    protected function handleLoadedProduct(&$product, $stock = false) {
        if($this->isBundle($product)) {
            $data = $product->getData();
            // if bundle, get bundled products and add to data
            $bundled_products = $this->parseProductNoList($data['products'], $product->getLocale(), $stock);
            $data['bundle']['totalPrice'] = 0;
            $data['bundle']['totalSize'] = 0;
            $data['bundle']['totalQty'] = 0;
            $data['bundle']['nonBottle'] = 0;
            $data['bundle']['available'] = [
                'total' => false,
                'products' => [],
            ];
            $data['bundled'] = $bundled_products;

            foreach($data['products'] as $bundled_product) {
                if(isset($bundled_products[$bundled_product['product_no']])) {
                    /**
                     * @var \HydroFeature\Shop\StorageItem\ProductData $p
                     */
                    $p = $bundled_products[$bundled_product['product_no']];
                    if($p->getPrice() * 1) {
                        $data['bundle']['totalPrice'] += ($p->getPrice() * 1) * $bundled_product['qty'];
                    }
                    if(isset($p->getData()['size']['value']) && $p->getData()['size']['value'] * 1) {
                        $data['bundle']['totalSize'] += $p->getData()['size']['value'] * $bundled_product['qty'];
                    }
                    if(isset($p->getData()['no_bottle']) && $p->getData()['no_bottle']) {
                        $data['bundle']['nonBottle']++;
                    }
                    $data['bundle']['totalQty'] += $bundled_product['qty'] * 1;

                    if(isset($p->amount_available)) {
                        if(false === $data['bundle']['available']['total']) {
                            $data['bundle']['available']['total'] = $p->amount_available / $bundled_product['qty'];
                        } else if($data['bundle']['available']['total'] > $p->amount_available / $bundled_product['qty']) {
                            $data['bundle']['available']['total'] = $p->amount_available / $bundled_product['qty'];
                        }
                        $data['bundle']['available']['products'][$p->getProductNo()] = $p->amount_available / $bundled_product['qty'];
                    }
                }
            }
            if($data['bundle']['available']['total']) {
                $data['bundle']['available']['total'] = floor($data['bundle']['available']['total']);
            }
            /*if(isset($data['info'])) {
                unset($data['info']);
            }*/
            /*if(isset($data['main_categories'])) {
                unset($data['main_categories']);
            }*/
            /*if(isset($data['categories'])) {
                unset($data['categories']);
            }
            if(isset($data['seo'])) {
                unset($data['seo']);
            }
            /*if(isset($data['bundled'])) {
                unset($data['bundled']);
            }*/
            $product->setData($data);
        }
    }

    /**
     * @param $products_list
     * @param string $active_locale
     * @param bool $stock
     * @param string $field name of the field where the `product_no` can be found
     *
     * @return \HydroFeature\Shop\StorageItem\ProductData[]
     */
    public function parseProductNoList($products_list, $active_locale, $stock = false, $field = 'product_no') {
        if(is_array($products_list)) {
            $product_numbers = [];
            foreach($products_list as $product_list_row) {
                if(isset($product_list_row[$field])) {
                    $product_numbers[] = $product_list_row[$field];
                }
            }

            return $this->loadProductsByNos($product_numbers, $active_locale, $stock);
        }

        return [];
    }

    /**
     * @param \HydroFeature\Shop\StorageItem\ProductData $product
     *
     * @return bool
     */
    public function isBundle($product) {
        return $product && isset($product->getData()['products']);
    }
}
