<?php

namespace HydroFeature\Shop\StorageItem;

class Order {
    public $id;
    public $date;
    public $locale;
    /**
     * @var \HydroFeature\Shop\StorageItem\OrderCustomer|null
     */
    public $customer;
    /**
     * @var \HydroFeature\Shop\StorageItem\OrderCustomer|null
     */
    public $customer_ship;
    public $products;
    public $shipping;
    public $state;
    public $state_log;
    public $hook;
    public $order_no;
    public $user_id;

    public function __construct($raw = []) {
        if(isset($raw['ID'])) {
            $this->id = $raw['ID'];
        }
        if(isset($raw['date'])) {
            $this->date = $raw['date'];
        }
        if(isset($raw['locale'])) {
            $this->locale = $raw['locale'];
        }
        if(isset($raw['customer'])) {
            if(is_string($raw['customer'])) {
                $this->customer = new OrderCustomer(json_decode($raw['customer'], true));
            } else {
                $this->customer = new OrderCustomer($raw['customer']);
            }
        }
        if(isset($raw['customer_ship'])) {
            if(is_string($raw['customer_ship'])) {
                $this->customer_ship = new OrderCustomer(json_decode($raw['customer_ship'], true));
            } else {
                $this->customer_ship = new OrderCustomer($raw['customer_ship']);
            }
        }
        if(isset($raw['products'])) {
            if(is_string($raw['products'])) {
                $this->products = json_decode($raw['products'], true);
            } else {
                $this->products = $raw['products'];
            }
        }
        if(isset($raw['shipping'])) {
            if(is_string($raw['shipping'])) {
                $this->shipping = json_decode($raw['shipping'], true);
            } else {
                $this->shipping = $raw['shipping'];
            }
        }
        if(isset($raw['state'])) {
            $this->state = $raw['state'];
        }
        if(isset($raw['state_log'])) {
            if(is_string($raw['state_log'])) {
                $this->state_log = json_decode($raw['state_log'], true);
            } else {
                $this->state_log = $raw['state_log'];
            }
        }
        if(isset($raw['hook'])) {
            $this->hook = $raw['hook'];
        }
        if(isset($raw['order_no'])) {
            $this->order_no = $raw['order_no'];
        }
        if(isset($raw['user_id'])) {
            $this->user_id = $raw['user_id'];
        }
    }
}
