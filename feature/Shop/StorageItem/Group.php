<?php

namespace HydroFeature\Shop\StorageItem;

use \HydroFeature\Shop\StorageHandler;

class Group {
    public $id;
    public $name;
    public $hook;

    protected $products = false;

    protected $storage;
    protected $storageProduct;

    /**
     * User constructor.
     *
     * @param $data
     *
     * @throws \Exception
     */
    public function __construct($data) {
        if(!isset($data['ID'])) {
            throw new \Exception('Tried to create shop_group without `ID`');
        }

        if(!isset($data['name'])) {
            throw new \Exception('Tried to create shop_group without `name`');
        }

        if(!isset($data['hook'])) {
            throw new \Exception('Tried to create shop_group without `hook`');
        }

        $this->id = $data['ID'];
        $this->name = $data['name'];
        $this->hook = $data['hook'];

        $this->storage = new StorageHandler\Group();
        $this->storageProduct = new StorageHandler\Product();
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getHook() {
        return $this->hook;
    }

    public function getProducts() {
        if($this->products) {
            return $this->products;
        }

        $products = $this->storageProduct->getByGroup($this->id);

        if(is_array($products)) {
            foreach($products as &$product) {
                $id = $product['ID'];
                unset ($product['ID']);
                $product['id'] = $id;
            }

            $this->products = $products;
            return $products;
        }

        return false;
    }

    public function save() {
        $updated = $this->storage->update($this->getId(), [
            'name' => $this->getName(),
        ]);

        if(is_int($updated)) {

            return true;
        } else {
            return false;
        }
    }

    public function delete() {
        $updated = $this->storage->delete($this->getId());

        if(is_int($updated)) {

            return true;
        } else {
            return false;
        }
    }
}