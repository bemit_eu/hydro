<?php

namespace HydroFeature\Shop\StorageItem;

class OrderCustomer {
    /**
     * @var bool|string false when guest order, string with customer id when registered order
     */
    public $account = false;
    public $name;
    public $email;
    public $street;
    public $street_no;
    public $post_code;
    public $city;
    public $country;
    public $data;

    public function __construct($raw = []) {
        if(isset($raw['account'])) {
            $this->account = $raw['account'];
        }
        if(isset($raw['name'])) {
            $this->name = $raw['name'];
        }
        if(isset($raw['email'])) {
            $this->email = $raw['email'];
        }
        if(isset($raw['street'])) {
            $this->street = $raw['street'];
        }
        if(isset($raw['street_no'])) {
            $this->street_no = $raw['street_no'];
        }
        if(isset($raw['post_code'])) {
            $this->post_code = $raw['post_code'];
        }
        if(isset($raw['city'])) {
            $this->city = $raw['city'];
        }
        if(isset($raw['country'])) {
            $this->country = $raw['country'];
        }
        if(isset($raw['data'])) {
            $this->data = $raw['data'];
        }
    }
}