<?php

namespace HydroFeature\Shop\StorageItem;

class ProductData {
    public $id;
    public $product;
    public $product_name;
    public $hook;
    public $locale;
    public $date_create;
    public $date_update;
    public $product_no;
    public $active;
    public $hidden;
    public $buy_able;
    public $tax_group;
    public $price;
    public $data;
    public $bundle;
    public $stock_item;
    public $amount_reserved;
    public $amount_available;

    protected $storage;

    /**
     * User constructor.
     *
     * @param $data
     *
     * @throws \Exception
     */
    public function __construct($data) {
        if(!isset($data['ID'])) {
            throw new \Exception('Tried to create shop_product_data without `ID`');
        }

        if(!isset($data['product'])) {
            throw new \Exception('Tried to create shop_product_data without `product`');
        }

        if(!isset($data['locale'])) {
            throw new \Exception('Tried to create shop_product_data without `locale`');
        }

        if(!isset($data['date_create'])) {
            throw new \Exception('Tried to create shop_product_data without `date_create`');
        }

        if(!isset($data['date_update'])) {
            throw new \Exception('Tried to create shop_product_data without `date_update`');
        }

        $this->id = $data['ID'];
        $this->product = $data['product'];
        $this->locale = $data['locale'];
        $this->date_create = $data['date_create'];
        $this->date_update = $data['date_update'];

        if(isset($data['name'])) {
            $this->product_name = $data['name'];
        }
        if(isset($data['hook'])) {
            $this->hook = $data['hook'];
        }

        if(isset($data['product_no'])) {
            $this->product_no = $data['product_no'];
        }
        if(isset($data['active'])) {
            $this->active = $data['active'];
        }
        if(isset($data['hidden'])) {
            $this->hidden = $data['hidden'];
        }
        if(isset($data['buy_able'])) {
            $this->buy_able = $data['buy_able'];
        }
        if(isset($data['tax_group'])) {
            $this->tax_group = $data['tax_group'];
        }
        if(isset($data['price'])) {
            $this->price = $data['price'];
        }
        if(isset($data['bundle'])) {
            $this->bundle = $data['bundle'];
        }
        if(isset($data['amount_available'])) {
            $this->amount_available = $data['amount_available'];
        }
        if(isset($data['amount_reserved'])) {
            $this->amount_reserved = $data['amount_reserved'];
        }
        if(isset($data['stock_item'])) {
            $this->stock_item = $data['stock_item'];
        }
        if(isset($data['data'])) {
            $dat = json_decode($data['data'], true);

            // todo: if isset `$data['data']['products']`, change this index to instance of `use HydroFeature\Shop\Order\ProductListItem;`, mv the class to a better namespace

            if(is_array($dat)) {
                if(
                    isset($dat['discount']['valid_till']) && $dat['discount']['valid_till'] &&
                    !getenv('FLAG_PROD_QUICK_DISCOUNT_FORCE__' . $this->id)
                ) {
                    try {
                        $valid_till = new \DateTime($dat['discount']['valid_till'], new \DateTimeZone('UTC'));
                        if($valid_till < new \DateTime('now', new \DateTimeZone('UTC'))) {
                            unset($dat['discount']);
                        }
                    } catch(\Exception $e) {

                    }
                }
                $this->data = $dat;
            } else {
                $this->data = null;
            }
        }

        $this->storage = new \HydroFeature\Shop\StorageHandler\Product();
    }

    /**
     * For API
     *
     * @return array
     * @deprecated
     * @todo refactor to somewhere else
     *
     */
    public function exportData() {
        $ret = [
            'id' => $this->getId(),
            'product' => $this->getProduct(),
            'locale' => $this->getLocale(),
            'date_create' => $this->getDateCreate(),
            'date_update' => $this->getDateUpdate(),
            'product_no' => $this->getProductNo(),
            'active' => $this->getActive(),
            'hidden' => $this->getHidden(),
            'buy_able' => $this->getBuyAble(),
            'tax_group' => $this->getTaxGroup(),
            'price' => $this->getPrice(),
            'bundle' => $this->getBundle(),
            'data' => $this->getData(),
        ];
        if($this->product_name) {
            $ret['product_name'] = $this->product_name;
        }
        if(isset($this->amount_available)) {
            $ret['amount_available'] = $this->amount_available;
        }
        if(isset($this->amount_reserved)) {
            $ret['amount_reserved'] = $this->amount_reserved;
        }
        if(isset($this->stock_item)) {
            $ret['stock_item'] = $this->stock_item;
        }

        return $ret;
    }

    /**
     * @return null|string id for this data object
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return null|string id of the product
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * @return null|string id of the locale
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * @return null|string timestamp
     */
    public function getDateCreate() {
        return $this->date_create;
    }

    /**
     * @return null|string timestamp
     */
    public function getDateUpdate() {
        return $this->date_update;
    }

    public function getProductNo() {
        return $this->product_no;
    }

    public function setProductNo($product_no) {
        $this->product_no = $product_no;
    }

    public function getActive() {
        return $this->active;
    }

    public function setActive($active) {
        return $this->active = $active;
    }

    public function getHidden() {
        return $this->hidden;
    }

    public function setHidden($hidden) {
        $this->hidden = $hidden;
    }

    public function getBuyAble() {
        return $this->buy_able;
    }

    public function setBuyAble($buy_able) {
        $this->buy_able = $buy_able;
    }

    public function getTaxGroup() {
        return $this->tax_group;
    }

    public function setTaxGroup($tax_group) {
        $this->tax_group = $tax_group;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getData() {
        return $this->data;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function getBundle() {
        return $this->bundle;
    }

    public function setBundle($bundle) {
        $this->bundle = $bundle;
    }

    public function save() {
        $updated = $this->storage->updateData($this->getId(), [
            'product' => $this->getProduct(),
            'locale' => $this->getLocale(),
            'date_create' => $this->getDateCreate(),
            'date_update' => $this->getDateUpdate(),
            'product_no' => $this->getProductNo(),
            'active' => $this->getActive() ? 1 : 0,
            'hidden' => $this->getHidden() ? 1 : 0,
            'buy_able' => $this->getBuyAble() ? 1 : 0,
            'tax_group' => $this->getTaxGroup(),
            'price' => $this->getPrice(),
            'bundle' => $this->getBundle(),
            'data' => json_encode($this->getData()),
        ]);

        if(is_int($updated)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete() {
        $deleted = $this->storage->deleteData($this->getId());

        if(is_int($deleted)) {
            return true;
        } else {
            return false;
        }
    }
}
