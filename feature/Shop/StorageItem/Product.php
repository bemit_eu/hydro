<?php

namespace HydroFeature\Shop\StorageItem;

use Hydro\Container;
use HydroApi\HookHydroApi;
use HydroFeature\Container as FeatureContainer;

class Product {
    protected $id;
    protected $group;
    protected $name;
    protected $hook;
    protected $product_type;

    /**
     * @var bool|array
     */
    protected $locales = false;
    /**
     * @var bool|ProductData[]
     */
    protected $data = false;

    protected $storage;

    /**
     * User constructor.
     *
     * @param $data
     *
     * @throws \Exception
     */
    public function __construct($data) {
        if(!isset($data['ID'])) {
            throw new \Exception('Tried to create shop_product without `ID`');
        }

        if(!isset($data['group'])) {
            throw new \Exception('Tried to create shop_product without `group`');
        }

        if(!isset($data['name'])) {
            throw new \Exception('Tried to create shop_product without `name`');
        }

        if(!isset($data['hook'])) {
            throw new \Exception('Tried to create shop_product without `hook`');
        }

        $this->id = $data['ID'];
        $this->group = $data['group'];
        $this->name = $data['name'];
        $this->hook = $data['hook'];

        if(isset($data['product_type'])) {
            $this->product_type = $data['product_type'];
        }

        $this->storage = new \HydroFeature\Shop\StorageHandler\Product();
    }

    /**
     * For API
     *
     * @return array
     * @deprecated
     * @todo refactor to somewhere else
     *
     */
    public function exportData() {
        $hook = Container::_hookStorage()->get($this->getHook());

        $value = [
            'id' => $this->getId(),
            'group' => $this->getGroup(),
            'name' => $this->getName(),
            'product_type' => $this->getType(),
            'locales' => $this->getLocales(),
            // todo: dynamic default locale
            'localeDefault' => 'en-US',
            'localesPossible' => $hook->getRouteLocaleList(),
            'productTypesPossible' => FeatureContainer::_shopProductType()::getListMerged($this->getHook()),
            'hook' => $this->hook,
            'data' => [],
        ];

        if($this->getType()) {
            $product_type = FeatureContainer::_shopProductType()::get($this->getType(), $this->getHook());
            if(!$product_type) {
                $product_type = FeatureContainer::_shopProductType()::get($this->getType());
            }
            if($product_type) {
                $value['product_type_data'] = $product_type;
            }
        }

        foreach($this->getLocales() as $locale) {
            $data = $this->getData($locale);
            if($data) {
                $value['data'][$locale] = $data->exportData();
            }
        }

        return $value;
    }

    public function getId() {
        return $this->id;
    }

    public function getGroup() {
        return $this->group;
    }

    /**
     * @param $group
     *
     * @throws \Exception
     */
    public function setGroup($group) {
        if('' === trim($group)) {
            throw new \Exception('StorageItem\Product: setGroup received empty group');
        } else {
            $this->group = trim($group);
        }
    }

    public function getType() {
        return $this->product_type;
    }

    public function setType($type) {
        if('' === trim($type) || !is_string($type)) {
            $this->product_type = null;
        } else {
            $this->product_type = trim($type);
        }
    }

    public function getName() {
        return $this->name;
    }

    public function getHook() {
        return $this->hook;
    }

    /**
     * @param $name
     *
     * @throws \Exception
     */
    public function setName($name) {
        if('' === trim($name)) {
            throw new \Exception('StorageItem\Product: setName received empty name');
        } else {
            $this->name = trim($name);
        }
    }

    public function getLocales() {
        if($this->locales) {
            return $this->locales;
        }

        $this->initData();

        return $this->locales;
    }

    /**
     * @param string $locale
     *
     * @return bool|\HydroFeature\Shop\StorageItem\ProductData
     */
    public function getData($locale) {
        if(!$this->data) {
            $this->initData();
        }

        if($locale && isset($this->data[$locale])) {
            return $this->data[$locale];
        }

        return false;
    }

    protected function initData() {
        $data = $this->storage->getDataByProduct($this->id);

        if(is_array($data)) {
            $this->locales = [];
            $this->data = [];
            foreach($data as $d) {
                try {
                    $data_product = new ProductData($d);
                    $this->locales[] = $data_product->getLocale();
                    $this->data[$data_product->getLocale()] = $data_product;
                } catch(\Exception $e) {
                    error_log('Product Data not valid for product `' . $this->id . '` ' . $e->getMessage());
                }
            }
        }
    }

    public function save() {
        $updated = $this->storage->update($this->getId(), [
            'group' => $this->getGroup(),
            'name' => $this->getName(),
            'product_type' => $this->getType(),
        ]);

        if(is_int($updated)) {

            return true;
        } else {
            return false;
        }
    }

    public function delete() {
        $updated = $this->storage->delete($this->getId());

        if(is_int($updated)) {

            return true;
        } else {
            return false;
        }
    }
}