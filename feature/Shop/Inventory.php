<?php

namespace HydroFeature\Shop;

use Flood\Component\Sonar\Annotation\Service as Service;
use HydroFeature\Container;

/**
 * @Service(id="feature-inventory", executed=false)
 * @package HydroFeature\Shop
 */
class Inventory {

    protected $storageWarehouse;
    protected $storageStock;
    protected $storageStockItem;

    public function __construct() {
        $this->storageWarehouse = new StorageHandler\Warehouse();
        $this->storageStock = new StorageHandler\Stock();
        $this->storageStockItem = new StorageHandler\StockItem();
    }

    public function createStockItem($stock, $name, $sku, $amount_available) {
        return $this->storageStockItem->create($stock, $name, $sku, $amount_available);
    }

    public function stockItemAvailableAdd($id, $value) {
        return $this->storageStockItem->availableAdd($id, $value);
    }

    public function stockItemAvailableRemove($id, $value) {
        return $this->storageStockItem->availableAdd($id, $value * -1);
    }

    public function stockItemReserve($id, $value, $order = null) {
        return $this->storageStockItem->reserve($id, $value, $order);
    }

    public function stockItemUnreserve($id, $value, $order = null) {
        return $this->storageStockItem->reserve($id, $value * -1, $order);
    }

    public function stockItemReservedRemove($id, $value) {
        return $this->storageStockItem->reservedRemove($id, $value);
    }

    public function stockItemLog($stock_item_id, $amount_available = 0, $amount_reserved = 0, $order = null) {
        return $this->storageStockItem->log($stock_item_id, $amount_available, $amount_reserved, $order);
    }

    public function bindProducts2StockItem($products, $stock_item_id) {
        return $this->storageStockItem->createBinding($products, $stock_item_id);
    }

    public function getStockItem($id, $amount_only = false) {
        return $this->storageStockItem->getById($id, $amount_only);
    }

    public function getStockItemForProduct($product) {
        return $this->storageStockItem->getByProduct($product);
    }

    public function getWarehouses() {
        $warehouses = $this->storageWarehouse->getAll();

        if($warehouses) {
            foreach($warehouses as &$warehouse) {
                $id = $warehouse['ID'];
                unset ($warehouse['ID']);
                $warehouse['id'] = $id;
            }
            unset($warehouse);

            return $warehouses;
        }

        return [];
    }

    public function getStocks() {
        $stocks = $this->storageStock->getAll();

        if($stocks) {
            foreach($stocks as &$stock) {
                $id = $stock['ID'];
                unset ($stock['ID']);
                $stock['id'] = $id;
            }
            unset($stock);

            return $stocks;
        }

        return [];
    }

    /**
     * @param bool|string $stock
     *
     * @return array|bool|mixed[]
     */
    public function getStockItems($stock = false) {
        $condition = [];
        if($stock) {
            $condition['stock'] = $stock;
        }

        $stockitems = $this->storageStockItem->getAll($condition);

        if($stockitems) {
            foreach($stockitems as &$item) {
                $id = $item['ID'];
                unset ($item['ID']);
                $item['id'] = $id;
            }
            unset($item);

            return $stockitems;
        }

        return [];
    }

    /**
     * @param bool|string $stock
     *
     * @return array|bool|mixed[]
     */
    public function getStockItemsDetailed($stock = false) {
        $condition = [];
        if($stock) {
            $condition['stock'] = $stock;
        }

        $stockitems = $this->storageStockItem->getAllDetailed($condition);

        if($stockitems) {
            foreach($stockitems as &$item) {
                $id = $item['ID'];
                unset ($item['ID']);
                $item['id'] = $id;
            }
            unset($item);

            return $stockitems;
        }

        return [];
    }
}