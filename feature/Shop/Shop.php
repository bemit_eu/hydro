<?php

namespace HydroFeature\Shop;

use Flood\Component\Sonar\Annotation\Service as Service;
use HydroFeature\Container;

/**
 * @Service(id="feature-shop", executed=false)
 * @package HydroFeature\Shop
 */
class Shop {

    protected $storageGroup;
    protected $storageProduct;
    protected $storageOrder;

    public function __construct() {
        $this->storageGroup = new StorageHandler\Group();
        $this->storageProduct = new StorageHandler\Product();
        $this->storageOrder = new StorageHandler\Order();
    }

    public function createGroup($name, $hook) {
        return $this->storageGroup->create($name, $hook);
    }

    public function createProduct($group, $name, $product_type, $hook) {
        return $this->storageProduct->create($group, $name, $product_type, $hook);
    }

    public function createProductData($product, $locale) {
        return $this->storageProduct->createData($product, $locale);
    }

    public function getGroups($hook = false) {
        if($hook) {
            $groups = $this->storageGroup->getByHook($hook);
        } else {
            $groups = $this->storageGroup->getAll();
        }

        if($groups) {
            foreach($groups as &$group) {
                $id = $group['ID'];
                unset ($group['ID']);
                $group['id'] = $id;
            }

            return $groups;
        }

        return [];
    }

    /**
     * @param $id
     *
     * @return bool|\HydroFeature\Shop\StorageItem\Group
     */
    public function getGroup($id) {
        $group = $this->storageGroup->getById($id);
        if($group) {
            try {
                return new StorageItem\Group($group);
            } catch(\Exception $e) {
                error_log('HydroFeature\Shop: data is invalid for group  id `' . $id . '`');

                return false;
            }
        }

        return false;
    }

    public function getProducts($hook = false) {
        if($hook) {
            $products = $this->storageProduct->getByHook($hook);
        } else {
            $products = $this->storageProduct->getAll();
        }

        if(is_array($products)) {
            foreach($products as &$product) {
                $id = $product['ID'];
                unset ($product['ID']);
                $product['id'] = $id;
            }

            return $products;
        }

        return [];
    }

    /**
     * @param $hook
     * @param $locale
     * @param bool $hidden
     * @param bool $stock
     * @param array $condition
     *
     * @return StorageItem\ProductData[]
     */
    public function getProductsForLocale($hook, $locale, $stock = false, $hidden = false, $condition = []) {
        $products = $this->storageProduct->getDataActiveForLocale($hook, $locale, $stock, $hidden, $condition);
        if(is_array($products)) {
            foreach($products as &$product) {
                try {
                    $product = new StorageItem\ProductData($product);
                } catch(\Exception $e) {
                    error_log('HydroFeature\Shop: one product in list is not valid, product with id `' . $product['ID'] . '`');
                    unset($product);
                }
            }

            return $products;
        }

        return [];
    }

    /**
     * @param $id
     *
     * @return bool|\HydroFeature\Shop\StorageItem\Product
     */
    public function getProduct($id) {
        $product = $this->storageProduct->getById($id);
        if($product) {
            try {
                return new StorageItem\Product($product);
            } catch(\Exception $e) {
                error_log('HydroFeature\Shop: can not get product with id `' . $id . '`');

                return false;
            }
        }

        return false;
    }

    /**
     * @param string $locale
     * @param string|array $product_no
     * @param bool $stock
     *
     * @return bool|\HydroFeature\Shop\StorageItem\ProductData[]
     */
    public function getProductByProductNo($locale, $product_no, $stock = false) {
        $product_data = $this->storageProduct->getByProductNo($locale, $product_no, $stock);
        if($product_data) {
            try {
                $products = [];
                foreach($product_data as $product_dat) {
                    $products[] = new StorageItem\ProductData($product_dat);
                }
                return $products;
            } catch(\Exception $e) {
                error_log('HydroFeature\Shop: can not get product with product_no `' . json_encode($product_no) . '`');

                return false;
            }
        }

        return false;
    }

    /**
     * @param $data_id
     * @param $stock
     *
     * @return bool|\HydroFeature\Shop\StorageItem\ProductData
     */
    public function getProductData($data_id, $stock = false) {
        $data = $this->storageProduct->getDataById($data_id, $stock);
        if($data) {
            try {
                return new StorageItem\ProductData($data);
            } catch(\Exception $e) {
                error_log('HydroFeature\Shop: data is invalid for product id `' . $data_id . '`');

                return false;
            }
        }

        return false;
    }

    public function getOrder($order_id) {
        $order = $this->storageOrder->getById($order_id);

        if($order) {
            return $order;
        }

        return false;
    }

    /**
     * @param array $condition
     *
     * @return \HydroFeature\Shop\StorageItem\Order[]|bool
     */
    public function getOrders($condition = []) {
        $groups = $this->storageOrder->getAll($condition);

        if(is_array($groups)) {
            return $groups;
        }

        return [];
    }
}
