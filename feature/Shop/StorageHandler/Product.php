<?php

namespace HydroFeature\Shop\StorageHandler;

use Hydro\Container;
use Exception;

class Product {
    public static $table = 'shop_product';
    public static $column = ['shop_product.ID', 'group', 'name', 'product_type', 'hook'];

    public static $table_data = 'shop_product_data';
    public static $column_data = ['shop_product_data.ID', 'product', 'locale', 'date_create', 'date_update', 'product_no', 'active', 'hidden', 'buy_able', 'tax_group', 'price', 'data', 'variation_of', 'bundle'];

    public function __construct() {
    }

    protected function addJoinStock(&$columns, &$condition) {
        $condition['JOIN'] = [
            '<inventory_stock_item2shop_product' => ['ID' => 'inventory_stock_item2shop_product.shop_product'],
            '<inventory_stock_item' => ['inventory_stock_item.ID' => 'inventory_stock_item2shop_product.stock_item'],
        ];
        $columns[] = 'inventory_stock_item`.`ID` as `stock_item';
        $columns[] = 'amount_reserved';
        $columns[] = 'amount_available';
    }


    /**
     * @param $group
     * @param $name
     * @param $product_type
     * @param $hook
     * @param bool|int $tried
     *
     * @return bool|string
     */
    public function create($group, $name, $product_type, $hook, $tried = false) {
        $id = Container::_storageData()->randomId();
        $data = [
            'ID' => $id,
            'name' => $name,
            'group' => $group,
            'hook' => $hook,
        ];

        if('' === trim($product_type)) {
            $data['product_type'] = null;
        } else {
            $data['product_type'] = $product_type;
        }

        $inserted = Container::_storageData()->insert(
            static::$table,
            $data
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->create($group, $name, $product_type, $hook, $tried);
    }

    public function createData($product, $locale, $optional = [], $tried = false) {
        $id = Container::_storageData()->randomId();
        $data = [
            'ID' => $id,
            'product' => $product,
            'locale' => $locale,
        ];
        /*if(isset($optional['meta'])) {
            if(is_string($optional['meta'])) {
                $data['meta'] = $optional['meta'];
            } else if(is_array($optional['meta'])) {
                $data['meta'] = json_encode($optional['meta']);
            }
        }
        if(isset($optional['main_text'])) {
            $data['main_text'] = $optional['main_text'];
        }*/

        $inserted = Container::_storageData()->insert(
            static::$table_data,
            $data
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->createData($product, $locale, $optional, $tried);
    }

    public function getById($id) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['ID' => $id]);
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(Exception $e) {
            error_log('Product Storage getById error: ' . $e->getMessage());

            return false;
        }
    }

    /**
     * @param string $locale
     * @param string|array $product_no
     * @param bool $stock
     *
     * @return bool|mixed[]
     */
    public function getByProductNo($locale, $product_no, $stock = false) {
        if(!is_array($product_no)) {
            $product_no = [$product_no];
        }

        $condition = [
            'active' => true,
            'locale' => $locale,
            'OR' => [],
            'AND' => [
                'shop_product.ID' => '=shop_product_data.product',
            ],
        ];

        foreach($product_no as $k => $product_n) {
            $condition['OR']['product_no#' . $k] = $product_n;
        }

        $columns = static::$column_data;
        $columns[] = 'shop_product.name';
        $columns[] = 'shop_product.hook';

        if($stock) {
            $this->addJoinStock($columns, $condition);
        }

        $stmt = Container::_storageData()->select($columns, [static::$table, static::$table_data], $condition);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(Exception $e) {
            error_log('Product Storage getByProductNo error: ' . $e->getMessage());

            return false;
        }
    }

    public function getByGroup($sid) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['`group`' => $sid]);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(Exception $e) {
            error_log('Product Storage getByGroup error: ' . $e->getMessage());

            return false;
        }
    }

    public function getByHook($hook) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['hook' => $hook]);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(Exception $e) {
            error_log('Product Storage getByHook error: ' . $e->getMessage());

            return false;
        }
    }

    public function getAll() {
        $stmt = Container::_storageData()->select(static::$column, static::$table);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(Exception $e) {
            error_log('Product Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }

    public function getDataActiveForLocale($hook, $locale, $stock = false, $hidden = false, $condition = []) {
        $condition['active'] = true;
        $condition['shop_product`.`ID'] = '=product';
        $condition['shop_product`.`hook'] = $hook;
        $condition['locale'] = $locale;

        if($hidden === true) {
            $condition['OR#rjksdnkj'] = [
                'hidden' => true,
                'hidden#2' => false,
            ];
        } else {
            $condition['hidden'] = false;
        }

        $columns = static::$column_data;
        $tables = [static::$table, static::$table_data];

        if($stock) {
            $this->addJoinStock($columns, $condition);
        }
        $stmt = Container::_storageData()->select($columns, $tables, $condition);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(Exception $e) {
            error_log('Product Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }

    public function getDataById($data_id, $stock = false) {
        $columns = static::$column_data;
        $tables = [static::$table_data];

        $condition = [static::$table_data . '`.`ID' => $data_id];

        if($stock) {
            $this->addJoinStock($columns, $condition);
        }
        $stmt = Container::_storageData()->select($columns, $tables, $condition);
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(Exception $e) {
            error_log('Product Storage getDataById error: ' . $e->getMessage());

            return false;
        }
    }

    public function getDataByproduct($product_id) {
        $stmt = Container::_storageData()->select(static::$column_data, static::$table_data, ['product' => $product_id]);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(Exception $e) {
            error_log('Product Storage getDataByproduct error: ' . $e->getMessage());

            return false;
        }
    }

    public function update($product_id, $data) {
        try {
            return Container::_storageData()->update(static::$table,
                $data,
                ['ID' => $product_id]);
        } catch(Exception $e) {
            error_log('Product Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function updateData($data_id, $data) {
        try {
            return Container::_storageData()->update(static::$table_data,
                $data,
                ['ID' => $data_id]);
        } catch(Exception $e) {
            error_log('Product Storage updateData error: ' . $e->getMessage());

            return false;
        }
    }

    public function deleteData($data_id) {
        try {
            return Container::_storageData()->delete(static::$table_data, ['ID' => $data_id]);
        } catch(Exception $e) {
            error_log('Product Storage deleteData error: ' . $e->getMessage());

            return false;
        }
    }

    public function deleteDataFromProduct($product_id) {
        try {
            return Container::_storageData()->delete(static::$table_data, ['product' => $product_id]);
        } catch(Exception $e) {
            error_log('Product Storage deleteDataFromProduct error: ' . $e->getMessage());

            return false;
        }
    }

    public function delete($product_id) {
        try {
            $deleted = $this->deleteDataFromProduct($product_id);
            if(is_int($deleted)) {
                return Container::_storageData()->delete(static::$table, ['ID' => $product_id]);
            }
            throw new Exception('Deletion of data associated with product failed ' . $product_id);
        } catch(Exception $e) {
            error_log('Product Storage delete error: ' . $e->getMessage());

            return false;
        }
    }
}