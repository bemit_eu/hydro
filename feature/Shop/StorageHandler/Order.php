<?php

namespace HydroFeature\Shop\StorageHandler;

use Hydro\Container;

class Order {
    protected static $table = 'shop_order';
    protected static $column = ['ID', 'date', 'locale', 'customer', 'customer_ship', 'products', 'shipping', 'state', 'state_log', 'hook', 'order_no', 'user_id',];

    public function __construct() {
    }

    /**
     * @param \HydroFeature\Shop\OrderBuilder $order
     * @param bool $tried
     *
     * @return bool|\HydroFeature\Shop\StorageItem\Order
     */
    public function create($order, $tried = false) {
        $data = [];
        if($order->customer) {
            $data['customer'] = json_encode($order->customer);
        }
        if($order->customer_ship) {
            $data['customer_ship'] = json_encode($order->customer_ship);
        }
        if($order->hook) {
            $data['hook'] = $order->hook;
        }
        if($order->locale) {
            $data['locale'] = $order->locale;
        }
        if($order->products) {
            $data['products'] = [];
            foreach($order->products as $product) {
                $data['products'][] = [
                    'qty' => $product->qty,
                    'custom' => $product->custom,
                    'product' => $product->product->exportData(),
                ];
            }
            if(empty($data['products'])) {
                unset($data['products']);
            } else {
                $data['products'] = json_encode($data['products']);
            }
        }
        if($order->shipping) {
            $data['shipping'] = json_encode($order->shipping);
        }
        if($order->order_no_schema) {
            $data['order_no'] = $this->parseOrderNoSchema($order->order_no_schema);
        }
        if($order->customer->account) {
            $data['user_id'] = $order->customer->account;
        }

        $id = Container::_storageData()->randomId();
        $data['ID'] = $id;

        if(isset($data['order_no'])) {
            $inserted = $this->execInsert(
                $data
            );
        } else {
            $inserted = Container::_storageData()->insert(
                static::$table,
                $data
            );
        }

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->create($order, $tried);
    }

    protected function parseOrderNoSchema($order_no_schema) {
        // todo: more generic parsing
        // todo: add locale placeholder

        //$date = new \DateTime(null, new \DateTimeZone('Europe/Berlin'));
        $date = new \DateTime();
        if(false !== strpos($order_no_schema, '{Y}')) {
            $order_no_schema = str_replace('{Y}', $date->format('Y'), $order_no_schema);
        }
        if(false !== strpos($order_no_schema, '{m}')) {
            $order_no_schema = str_replace('{m}', $date->format('m'), $order_no_schema);
        }

        $len = 0;
        if(false !== strpos($order_no_schema, '{AI')) {
            $begin_ai = strpos($order_no_schema, '{AI') + 1;
            $raw_ai = substr($order_no_schema, $begin_ai, strpos($order_no_schema, '}', $begin_ai) - $begin_ai);
            if(false !== strpos($raw_ai, ':')) {
                $tmp_len = explode(':', $raw_ai);
                $len = $tmp_len[1];
            }

            $order_no_schema = substr($order_no_schema, 0, $begin_ai - 1);
        }

        return [
            // prefix for order_no, the last part will be incremented
            'prefix' => $order_no_schema,
            // len
            'len' => $len,
        ];
    }

    protected function execInsert(array $data) {
        if(empty($data)) {
            return Container::_storageData()->getDatabase()->executeUpdate('INSERT INTO ' . static::$table . ' () VALUES ()');
        }

        // update: performance ideas
        // subselect the last made order by date isn't good when orders in the same second are done
        // add an numeric invoice numbering column also that contains the simple numeric value

        $table = static::$table;

        $customer = 'NULL';
        if(isset($data['customer'])) {
            $customer = Container::_storageData()->getDatabase()->quote($data['customer']);
        }
        $customer_ship = 'NULL';
        if(isset($data['customer_ship'])) {
            $customer_ship = Container::_storageData()->getDatabase()->quote($data['customer_ship']);
        }
        $hook = 'NULL';
        if(isset($data['hook'])) {
            $hook = Container::_storageData()->getDatabase()->quote($data['hook']);
        }
        $locale = 'NULL';
        if(isset($data['locale'])) {
            $locale = Container::_storageData()->getDatabase()->quote($data['locale']);
        }
        $products = 'NULL';
        if(isset($data['products'])) {
            $products = Container::_storageData()->getDatabase()->quote($data['products']);
        }
        $shipping = 'NULL';
        if(isset($data['shipping'])) {
            $shipping = Container::_storageData()->getDatabase()->quote($data['shipping']);
        }
        $user_id = 'NULL';
        if(isset($data['user_id'])) {
            $user_id = Container::_storageData()->getDatabase()->quote($data['user_id']);
        }

        return Container::_storageData()->getDatabase()->executeUpdate(<<<SQL
INSERT INTO `{$table}` (`ID`, `customer`, `customer_ship`, `hook`, `locale`, `products`, `shipping`, `user_id`, `order_no`)
SELECT "{$data['ID']}", 
       {$customer}, {$customer_ship},  {$hook},  {$locale},  {$products}, {$shipping}, {$user_id},
       CONCAT("{$data['order_no']['prefix']}", LPAD((IFNULL(MAX(REPLACE(`order_no`, "{$data['order_no']['prefix']}", "")), "0") + 1), {$data['order_no']['len']}, "0"))
FROM `shop_order`
WHERE `order_no` LIKE "{$data['order_no']['prefix']}%"
SQL
        );
    }

    /**
     * @param $id
     *
     * @return bool|\HydroFeature\Shop\StorageItem\Order
     */
    public function getById($id) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['ID' => $id]);
        try {
            $stmt->execute();

            return new \HydroFeature\Shop\StorageItem\Order($stmt->fetch());
        } catch(\Exception $e) {
            error_log('Order Storage getById error: ' . $e->getMessage());

            return false;
        }
    }

    public function update($order_id, $data) {
        try {
            return Container::_storageData()->update(static::$table,
                $data,
                ['ID' => $order_id]);
        } catch(\Exception $e) {
            error_log('Order Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function getAll($condition = []) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, $condition);
        try {
            $stmt->execute();

            $res = $stmt->fetchAll();
            $orders = [];
            foreach($res as $order) {
                $orders[] = new \HydroFeature\Shop\StorageItem\Order($order);
            }

            return $orders;
        } catch(\Exception $e) {
            error_log('Order Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }
}
