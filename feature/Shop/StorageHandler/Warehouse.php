<?php

namespace HydroFeature\Shop\StorageHandler;

use Hydro\Container;

class Warehouse {
    public static $table = 'inventory_warehouse';
    public static $column = ['ID', 'name', 'sku_prefix', 'desc', 'location'];

    public function __construct() {
    }

    public function create($name, $tried = false) {
        // todo implement really
        $id = Container::_storageData()->randomId();
        $inserted = Container::_storageData()->insert(
            static::$table,
            [
                'ID' => $id,
                'name' => $name,
            ]
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->create($name, $tried);
    }

    public function getById($id) {
        // todo implement really
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['ID' => $id]);
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(\Exception $e) {
            error_log('Warehouse Storage getById error: ' . $e->getMessage());

            return false;
        }
    }

    public function update($warehouse_id, $data) {
        // todo implement really
        try {
            return Container::_storageData()->update(static::$table,
                $data,
                ['ID' => $warehouse_id]);
        } catch(\Exception $e) {
            error_log('Warehouse Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function delete($warehouse_id) {
        // todo implement really
        try {
            if($this->deleteStocks($warehouse_id)) {
                return Container::_storageData()->delete(static::$table, ['ID' => $warehouse_id]);
            }
            throw  new \Exception('Deletion of products within group failed `' . $warehouse_id . '`');
        } catch(\Exception $e) {
            error_log('Warehouse Storage delete error: ' . $e->getMessage());

            return false;
        }
    }

    protected function deleteStocks($warehouse_id) {
        // todo implement really
        $storageStocks = new Product();
        $stocks = $storageStocks->getByGroup($warehouse_id);

        if(is_array($stocks)) {
            /**
             * @var bool $no_error when no deletion failed, everything is correct
             */
            $no_error = true;
            foreach($stocks as $stock) {
                if(!$storageStocks->delete($stock['ID'])) {
                    $no_error = false;
                    error_log('Deleting stocks for warehouse `' . $warehouse_id . '` failed for stock `' . $stock['ID'] . '`');
                }
            }

            return $no_error;
        }

        return false;
    }

    public function getAll() {
        $stmt = Container::_storageData()->select(static::$column, static::$table);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('Warehouse Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }
}