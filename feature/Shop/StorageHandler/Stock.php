<?php

namespace HydroFeature\Shop\StorageHandler;

use Hydro\Container;

class Stock {
    public static $table = 'inventory_stock';
    public static $column = ['ID', 'warehouse', 'name', 'sku_prefix', 'desc', 'location'];

    public function __construct() {
    }

    public function create($name, $tried = false) {
        // todo implement really
        $id = Container::_storageData()->randomId();
        $inserted = Container::_storageData()->insert(
            static::$table,
            [
                'ID' => $id,
                'name' => $name,
            ]
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->create($name, $tried);
    }

    public function getById($id) {
        // todo implement really
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['ID' => $id]);
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(\Exception $e) {
            error_log('Stock Storage getById error: ' . $e->getMessage());

            return false;
        }
    }

    public function update($stock_id, $data) {
        // todo implement really
        try {
            return Container::_storageData()->update(static::$table,
                $data,
                ['ID' => $stock_id]);
        } catch(\Exception $e) {
            error_log('Stock Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function delete($stock_id) {
        // todo implement really
        try {
            if($this->deleteStocks($stock_id)) {
                return Container::_storageData()->delete(static::$table, ['ID' => $stock_id]);
            }
            throw  new \Exception('Deletion of products within group failed `' . $stock_id . '`');
        } catch(\Exception $e) {
            error_log('Stock Storage delete error: ' . $e->getMessage());

            return false;
        }
    }

    protected function deleteStocks($stock_id) {
        // todo implement really
        $storageStocks = new Product();
        $stocks = $storageStocks->getByGroup($stock_id);

        if(is_array($stocks)) {
            /**
             * @var bool $no_error when no deletion failed, everything is correct
             */
            $no_error = true;
            foreach($stocks as $stock) {
                if(!$storageStocks->delete($stock['ID'])) {
                    $no_error = false;
                    error_log('Deleting stocks for stock `' . $stock_id . '` failed for stock `' . $stock['ID'] . '`');
                }
            }

            return $no_error;
        }

        return false;
    }

    public function getAll() {
        $stmt = Container::_storageData()->select(static::$column, static::$table);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('Stock Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }
}