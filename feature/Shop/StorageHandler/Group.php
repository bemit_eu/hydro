<?php

namespace HydroFeature\Shop\StorageHandler;

use Hydro\Container;

class Group {
    protected static $table = 'shop_group';
    protected static $column = ['ID', 'name', 'hook'];

    public function __construct() {
    }

    public function create($name, $hook, $tried = false) {
        $id = Container::_storageData()->randomId();
        $inserted = Container::_storageData()->insert(
            static::$table,
            [
                'ID' => $id,
                'name' => $name,
                'hook' => $hook,
            ]
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->create($name, $hook, $tried);
    }

    public function getById($id) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['ID' => $id]);
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(\Exception $e) {
            error_log('Group Storage getById error: ' . $e->getMessage());

            return false;
        }
    }

    public function getByName($name) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['name' => $name]);
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(\Exception $e) {
            error_log('Group Storage getByName error: ' . $e->getMessage());

            return false;
        }
    }

    public function getByHook($hook) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['hook' => $hook]);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('Group Storage getByHook error: ' . $e->getMessage());

            return false;
        }
    }

    public function update($group_id, $data) {
        try {
            return Container::_storageData()->update(static::$table,
                $data,
                ['ID' => $group_id]);
        } catch(\Exception $e) {
            error_log('Group Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function delete($group_id) {
        try {
            if($this->deleteProducts($group_id)) {
                return Container::_storageData()->delete(static::$table, ['ID' => $group_id]);
            }
            throw  new \Exception('Deletion of products within group failed `' . $group_id . '`');
        } catch(\Exception $e) {
            error_log('Group Storage delete error: ' . $e->getMessage());

            return false;
        }
    }

    protected function deleteProducts($group_id) {
        $storageProduct = new Product();
        $products = $storageProduct->getByGroup($group_id);

        if(is_array($products)) {
            /**
             * @var bool $no_error when no deletion failed, everything is correct
             */
            $no_error = true;
            foreach($products as $product) {
                if(!$storageProduct->delete($product['ID'])) {
                    $no_error = false;
                    error_log('Deleting products for group `' . $group_id . '` failed for product `' . $product['ID'] . '`');
                }
            }

            return $no_error;
        }

        return false;
    }

    public function getAll() {
        $stmt = Container::_storageData()->select(static::$column, static::$table);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('Group Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }
}