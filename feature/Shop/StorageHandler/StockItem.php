<?php

namespace HydroFeature\Shop\StorageHandler;

use HydroFeature\Container as FeatureContainer;
use Hydro\Container;

class StockItem {
    public static $table = 'inventory_stock_item';
    public static $column = ['inventory_stock_item`.`ID', 'stock', 'sku', 'inventory_stock_item`.`name', 'inventory_stock_item`.`location', 'amount_reserved', 'amount_available'];

    public static $table_2_product = 'inventory_stock_item2shop_product';
    public static $column_2_product = ['stock_item', 'shop_product'];

    public static $table_log = 'inventory_stock_item_log';

    public function __construct() {
    }

    public function create($stock, $name = '', $sku = null, $amount_available = null, $tried = false) {
        $id = Container::_storageData()->randomId();
        $inserted = Container::_storageData()->insert(
            static::$table,
            [
                'ID' => $id,
                'stock' => $stock,
                'name' => $name,
                'amount_available' => $amount_available,
                'sku' => $sku,
            ]
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->create($stock, $name, $sku, $amount_available, $tried);
    }

    public function createBinding($products, $stock_item) {
        foreach($products as $product) {
            $this->deleteBindingByProduct($product);
            Container::_storageData()->insert(
                static::$table_2_product,
                [
                    'stock_item' => $stock_item,
                    'shop_product' => $product,
                ]
            );
        }
        return true;
    }

    public function getById($id, $amount_only = false) {
        $stmt = Container::_storageData()->select(
            $amount_only ?
                ['amount_reserved', 'amount_available']
                : static::$column,
            static::$table,
            ['ID' => $id]
        );
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(\Exception $e) {
            error_log('StockItem Storage getById error: ' . $e->getMessage());

            return false;
        }
    }

    public function getByProduct($product) {
        $stmt = Container::_storageData()->select(
            array_merge([
                Warehouse::$table . '`.`name` as `warehouse_name',
                Stock::$table . '`.`name` as `stock_name',
            ], static::$column),
            [
                Warehouse::$table,
                Stock::$table,
                static::$table,
                static::$table_2_product,
            ], [
            Warehouse::$table . '`.`ID' => '=`' . Stock::$table . '`.`warehouse',
            static::$table . '`.`ID' => '=stock_item',
            'shop_product' => $product,
        ]);

        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(\Exception $e) {
            error_log('StockItem Storage getByProduct error: ' . $e->getMessage());

            return false;
        }
    }

    public function update($stock_item_id, $data) {
        // todo implement really
        try {
            return Container::_storageData()->update(static::$table,
                $data,
                ['ID' => $stock_item_id]);
        } catch(\Exception $e) {
            error_log('StockItem Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function availableAdd($stock_item_id, $value) {
        try {
            $added = Container::_storageData()->getDatabase()
                              ->executeUpdate(
                                  'UPDATE ' . static::$table . ' SET `amount_available`=`amount_available`+ ? WHERE ID=?',
                                  [$value, $stock_item_id]
                              );
            if($added) {
                FeatureContainer::_inventory()->stockItemLog($stock_item_id, $value, 0);
                return true;
            }
            return false;
        } catch(\Exception $e) {
            error_log('StockItem Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function reserve($stock_item_id, $value, $order = null) {
        try {
            $reserved = Container::_storageData()->getDatabase()
                                 ->executeUpdate(
                                     'UPDATE ' . static::$table .
                                     ' SET   `amount_available`=`amount_available`- ?,' .
                                     '       `amount_reserved`=`amount_reserved`+ ?' .
                                     ' WHERE ID=?',
                                     [$value, $value, $stock_item_id]
                                 );
            if($reserved) {
                FeatureContainer::_inventory()->stockItemLog($stock_item_id, 0, $value, $order);
                return true;
            }
            return false;
        } catch(\Exception $e) {
            error_log('StockItem Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function reservedRemove($stock_item_id, $value) {
        try {
            $removed = Container::_storageData()->getDatabase()
                                ->executeUpdate(
                                    'UPDATE ' . static::$table . ' SET `amount_reserved`=`amount_reserved`- ? WHERE ID=?',
                                    [$value, $stock_item_id]
                                );
            if($removed) {
                FeatureContainer::_inventory()->stockItemLog($stock_item_id, 0, -1 * $value, null);
                return true;
            }
            return false;
        } catch(\Exception $e) {
            error_log('StockItem Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function log($stock_item_id, $amount_available, $amount_reserved, $order = null) {
        try {
            $inserted = Container::_storageData()->insert(
                static::$table_log,
                [
                    'stock_item' => $stock_item_id,
                    'amount_available' => $amount_available,
                    'amount_reserved' => $amount_reserved,
                    'order' => $order,
                ]
            );

            if(!$inserted) {
                throw new \Exception('stock item `' . $stock_item_id . '` was not updated');
            }
            return true;
        } catch(\Exception $e) {
            error_log('StockItem Storage update error: ' . $e->getMessage());

        }
        return false;
    }

    public function delete($stock_item_id) {
        try {
            return Container::_storageData()->delete(static::$table, ['ID' => $stock_item_id]);
        } catch(\Exception $e) {
            error_log('StockItem Storage delete error: ' . $e->getMessage());

            return false;
        }
    }

    public function deleteBindingByProduct($product) {
        try {
            return Container::_storageData()->delete(static::$table_2_product, ['shop_product' => $product]);
        } catch(\Exception $e) {
            error_log('StockItem Storage deleteBindingByProduct error: ' . $e->getMessage());

            return false;
        }
    }

    public function getAll($condition) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, $condition);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('StockItem Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }

    public function getAllDetailed($condition = []) {
        $stmt = Container::_storageData()->select(
            array_merge([
                Warehouse::$table . '`.`name` as `warehouse_name',
                Stock::$table . '`.`name` as `stock_name',
                Product::$table . '`.`name` as `product_name',
                'count(shop_product) as shop_products',
            ], static::$column),
            [
                Warehouse::$table,
                Stock::$table,
                static::$table,
            ],
            array_merge([
                Stock::$table . '`.`ID' => '=`' . static::$table . '`.`stock',
                Warehouse::$table . '`.`ID' => '=`' . Stock::$table . '`.`warehouse',
                'JOIN' => [
                    '<' . static::$table_2_product => ['ID' => 'inventory_stock_item2shop_product.stock_item'],
                    '<' . Product::$table_data => [Product::$table_data . '.ID' => 'inventory_stock_item2shop_product.shop_product'],
                    '<' . Product::$table => [Product::$table . '.ID' => Product::$table_data . '.product'],
                ],
                'GROUP' => [
                    static::$table . '`.`ID',
                ],
            ], $condition)
        );

        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('StockItem Storage getAllDetailed error: ' . $e->getMessage());

            return false;
        }
    }
}