<?php

namespace HydroFeature\Shop;

use Flood\Component\Sonar\Annotation\Service as Service;
use HydroFeature\Container as FeatureContainer;
use HydroFeature\Mailer\Mailer;
use HydroFeature\Shop\Order\ExceptionNotBuyAble;
use HydroFeature\Shop\Order\ExceptionNotExisting;
use HydroFeature\Shop\Order\ExceptionOutOfStock;
use HydroFeature\Shop\Order\ProductListItem;
use HydroFeature\Shop\StorageHandler\Order as StorageOrder;
use HydroFeature\Shop\StorageItem\OrderCustomer;
use HydroFeature\Shop\StorageItem\ProductData;

/**
 *
 * @Service(id="feature-shop-order-builder", executed=true)
 * @package HydroFeature\Shop
 */
class OrderBuilder {
    /**
     * @var bool|string id of order, only available after saving
     */
    public $order_id = false;
    public $locale;
    public $hook;
    /**
     * @var ProductListItem[]
     */
    public $products;
    /**
     * @var \HydroFeature\Shop\StorageItem\OrderCustomer
     */
    public $customer;
    /**
     * @var \HydroFeature\Shop\StorageItem\OrderCustomer
     */
    public $customer_ship;
    public $shipping;
    public $order_no_schema = '';

    public $total_price = 0;
    public $total_discount = 0;
    public $total_coupon_discount = 0;

    public function __construct() {
        $this->customer = new OrderCustomer();
    }

    /**
     * @param $product_data_id
     * @param $qty
     * @param $coupon_value
     * @param null|array $labels
     *
     * @return \HydroFeature\Shop\StorageItem\ProductData
     * @throws \HydroFeature\Shop\Order\ExceptionNotExisting
     *
     * @throws \HydroFeature\Shop\Order\ExceptionNotBuyAble
     */
    public function addProduct($product_data_id, $qty, $coupon_value, $labels = null) {
        $product_storage = new StorageAbstraction\Product(false);
        $product = $product_storage->getProductData($product_data_id, true);
        if(!$product) {
            $exception = new ExceptionNotExisting('Order Builder: tried to add non existing product_data: ' . $product_data_id);
            $exception->products = [$product->id];
            throw $exception;
        }

        return $this->addProductRaw($product, $qty, $coupon_value, $labels);
    }

    /**
     * @param \HydroFeature\Shop\StorageItem\ProductData $product
     * @param $qty
     * @param $coupon_value
     * @param null|array $labels
     *
     * @return \HydroFeature\Shop\StorageItem\ProductData
     * @throws \HydroFeature\Shop\Order\ExceptionNotExisting
     *
     * @throws \HydroFeature\Shop\Order\ExceptionNotBuyAble
     */
    public function addProductRaw(ProductData $product, $qty, $coupon_value, $labels = null) {
        if(!$product->getBuyAble()) {
            $exception = new ExceptionNotBuyAble('Order Builder: tried to add not-buy-able product_data: ' . $product->id);
            $exception->products = [$product->id];
            throw $exception;
        }

        $total = static::getProductPrice($product, $qty, $coupon_value);
        $this->total_price += $total['total'];
        if(isset($product->data['discount']['percentage']) && $product->data['discount']['percentage']) {
            $this->total_discount += $total['discount'];
        } else if(isset($product->data['discount']['slashed_price']) && $product->data['discount']['slashed_price']) {
            $this->total_discount += $total['discount'];
        } else {
            $this->total_coupon_discount += $total['discount'];
        }
        $product_list_item = new ProductListItem();
        $product_list_item->product = $product;
        $product_list_item->qty = $qty;
        if($labels) {
            $product_list_item->custom = $labels;
        }

        $this->products[] = $product_list_item;

        return $product;
    }

    public static function getProductPrice(ProductData $product, $qty = 1, $coupon_value = 0) {
        $discount = isset($product->data['discount']['percentage']) && $product->data['discount']['percentage'] ? $product->data['discount']['percentage'] : $coupon_value;
        $slashed_price = isset($product->data['discount']['slashed_price']) && $product->data['discount']['slashed_price'] ? $product->data['discount']['slashed_price'] : null;
        $total_price = 0;
        if(isset($product->data['bundle'])) {
            if(isset($product->price) && is_numeric($product->price)) {
                $total_price = $product->price;
            } else {
                $total_price = $product->data['bundle']['totalPrice'];
            }
        } else {
            $total_price = $product->price;
        }

        $total_price *= $qty;

        $total_discount = $slashed_price ?
            ($slashed_price * $qty) - $total_price : (
            $discount ? round($total_price / 100 * $discount) : 0
            );

        return [
            'total' => $slashed_price ? $total_price : $total_price - $total_discount,
            'discount' => $total_discount,
        ];
    }

    /*public static function getProductPrice(ProductData $product, $qty = 1, $coupon_value = 0) {
        $discount = isset($product->data['discount']['percentage']) && $product->data['discount']['percentage'] ? $product->data['discount']['percentage'] : $coupon_value;
        $slashed_price = isset($product->data['discount']['slashed_price']) && $product->data['discount']['slashed_price'] ? $product->data['discount']['slashed_price'] : null;
        $total_price = 0;
        if(isset($product->data['bundle'])) {
            if(isset($product->price) && is_numeric($product->price)) {
                return [
                    'total' => $slashed_price || !$discount ?
                        $product->price * $qty :
                        ($product->price - ($product->price * ($discount / 100))) * $qty,
                    'discount' => $slashed_price ?
                        ($slashed_price - $product->price) * $qty :
                        ($discount ? ($product->price * ($discount / 100)) * $qty : 0),
                ];
            }

            return [
                'total' => $slashed_price || !$discount ?
                    $product->data['bundle']['totalPrice'] * $qty :
                    ($product->data['bundle']['totalPrice'] - ($product->data['bundle']['totalPrice'] * ($discount / 100))) * $qty,
                'discount' => $slashed_price ?
                    ($slashed_price - $product->data['bundle']['totalPrice']) * $qty :
                    ($discount ? ($product->data['bundle']['totalPrice'] * ($discount / 100)) * $qty : 0),
            ];
        }
        return [
            'total' => $slashed_price || !$discount ?
                $product->price * $qty :
                ($product->price - ($product->price * ($discount / 100))) * $qty,
            'discount' => $slashed_price ?
                ($slashed_price - $product->price) * $qty :
                ($discount ? ($product->price * ($discount / 100)) * $qty : 0),
        ];
    }*/

    /**
     * @return bool
     * @throws \HydroFeature\Shop\Order\ExceptionOutOfStock
     */
    public function checkProducts() {
        $tmp_stocks = [];

        foreach($this->products as $product) {
            if($product->product->amount_available !== null) {
                if(!isset($tmp_stocks[$product->product->stock_item])) {
                    $tmp_stocks[$product->product->stock_item] = [
                        'available' => $product->product->amount_available,
                        'products' => [],
                        'qty' => 0,
                    ];
                }
                $tmp_stocks[$product->product->stock_item]['qty'] += $product->qty;
                $tmp_stocks[$product->product->stock_item]['products'][$product->product->id] = true;
            } else if($product->product->bundle) {
                foreach($product->product->getData()['products'] as $bun_p) {
                    /**
                     * @var \HydroFeature\Shop\StorageItem\ProductData $bundled_prod
                     */
                    $bundled_prod = $product->product->getData()['bundled'][$bun_p['product_no']];
                    if($bundled_prod->amount_available !== null) {
                        if(!isset($tmp_stocks[$bundled_prod->stock_item])) {
                            $tmp_stocks[$bundled_prod->stock_item] = [
                                'available' => $bundled_prod->amount_available,
                                'qty' => 0,
                            ];
                        }
                        $tmp_stocks[$bundled_prod->stock_item]['qty'] += $product->qty * $bun_p['qty'];
                        $tmp_stocks[$bundled_prod->stock_item]['products'][$product->product->id] = true;
                    }
                }
            }
        }

        $out_of_stock = [];
        foreach($tmp_stocks as $stock_item => $t_stock) {
            if(($t_stock['available'] - $t_stock['qty']) < 0) {
                $out_of_stock[] = $t_stock['products'];
            }
        }

        if(count($out_of_stock)) {
            $exception = new ExceptionOutOfStock();
            $exception->products = $out_of_stock;
            throw $exception;
        }

        foreach($tmp_stocks as $stock_item => $t_stock) {
            FeatureContainer::_inventory()->stockItemReserve($stock_item, $t_stock['qty'], null);
        }

        return true;
    }

    public function setHook($hook) {
        $this->hook = $hook;
    }

    public function setLocale($locale) {
        $this->locale = $locale;
    }

    /**
     * @return bool|\HydroFeature\Shop\StorageItem\Order
     */
    public function save() {
        $storage = new StorageOrder();

        $this->order_no_schema;

        $order_id = $storage->create($this);
        if(!$order_id) {
            error_log('OrderBuilder: order could not be saved');

            return false;
        }
        $order = $storage->getById($order_id);
        if($order) {
            $this->order_id = $order->id;

            return $order;
        }

        return false;
    }

    /**
     * @param \HydroFeature\Mailer\MailBoxItem $mailbox
     * @param                                  $title
     * @param                                  $msg
     * @param                                  $content_type
     *
     * @return bool
     */
    public function mail($mailbox, $title, $msg, $content_type = 'text/html') {
        if($this->customer->email) {
            $mailer = new Mailer($mailbox);
            $message = $mailer->message($title);
            $message->setFrom($mailbox->getFromEmail(), $mailbox->getFromName())
                ->setTo($this->customer->email, $this->customer->name)
                ->setBody($msg, $content_type);

            return $mailer->send($message);
        }

        return false;
    }

    /**
     * @param                                  $to
     * @param \HydroFeature\Mailer\MailBoxItem $mailbox
     * @param                                  $title
     * @param                                  $msg
     * @param                                  $content_type
     *
     * @return bool
     */
    public function mailTo($to, $mailbox, $title, $msg, $content_type = 'text/html') {
        if($this->customer->email) {
            $mailer = new Mailer($mailbox);
            $message = $mailer->message($title);
            $message->setFrom($mailbox->getFromEmail(), $mailbox->getFromName())
                ->setTo($to)
                ->setBody($msg, $content_type);

            return $mailer->send($message);
        }

        return false;
    }
}
