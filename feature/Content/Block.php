<?php

namespace HydroFeature\Content;

use Flood\Component\Sonar\Annotation\Service as Service;
use Flood\Component\Json\Json;

/**
 * @Service(id="feature-content-block", executed=false)
 * @package HydroFeature\Content
 */
class Block {
    static $store = [];
    static $store_hook = [];

    public static function register($id, $path, $hook = false) {
        if($hook) {
            if(!isset(static::$store_hook[$hook]) || !is_array(static::$store_hook[$hook])) {
                static::$store_hook[$hook] = [];
            }
            static::$store_hook[$hook][$id] = [
                'loaded' => false,
                'path' => $path,
            ];
        } else {
            static::$store[$id] = [
                'loaded' => false,
                'path' => $path,
            ];
        }
    }

    /**
     * @param $id
     * @param $hook
     *
     * @return bool|\HydroFeature\Content\StorageItem\Block
     */
    public static function get($id, $hook = false) {
        if($hook) {
            return static::getInHook($id, $hook);
        } else {
            return static::getGlobal($id);
        }
    }

    protected static function getGlobal($id) {
        if(!isset(static::$store[$id])) {
            return false;
        }
        if(static::$store[$id]['loaded']) {
            return static::$store[$id]['value'];
        }

        $block_raw = Json::decodeFile(static::$store[$id]['path'], true);

        $block = new StorageItem\Block(dirname(static::$store[$id]['path']), $block_raw);

        if($block->schema) {
            // todo: make schema loading optional/load-on-demand
            $block->schema = Json::decodeFile($block->path . DIRECTORY_SEPARATOR . $block->schema, true);
            if(!$block->schema) {
                // not valid schema
                error_log('Content\Block: schema for block `' . $id . '` is invalid');
                $block->schema = false;
            }
        }
        if($block) {
            static::$store[$id]['loaded'] = true;
            static::$store[$id]['value'] = $block;

            return $block;
        }

        return false;
    }

    protected static function getInHook($id, $hook) {
        if(!isset(static::$store_hook[$hook][$id])) {
            return false;
        }
        if(static::$store_hook[$hook][$id]['loaded']) {
            return static::$store_hook[$hook][$id]['value'];
        }

        $block_raw = Json::decodeFile(static::$store_hook[$hook][$id]['path'], true);

        $block = new StorageItem\Block(dirname(static::$store_hook[$hook][$id]['path']), $block_raw);
        $block->hook = $hook;

        if($block->schema) {
            // todo: make schema loading optional/load-on-demand
            $block->schema = Json::decodeFile($block->path . DIRECTORY_SEPARATOR . $block->schema, true);
            if(!$block->schema) {
                // not valid schema
                error_log('Content\Block: schema for block `' . $id . '` is invalid');
                $block->schema = false;
            }
        }
        if($block) {
            static::$store_hook[$hook][$id]['loaded'] = true;
            static::$store_hook[$hook][$id]['value'] = $block;

            return $block;
        }

        return false;
    }

    public static function getList($hook = false) {
        $store = static::$store;
        if($hook) {
            if(isset(static::$store_hook[$hook])) {
                $store = static::$store_hook[$hook];
            } else {
                return [];
            }
        }

        $list = [];
        foreach($store as $id => $block_raw) {
            if(static::get($id, $hook)) {
                $block = static::get($id, $hook);
                $list[$id] = [
                    'id' => $id,
                    'type' => $block->type,
                    'data' => [],
                ];
            }
        }

        return $list;
    }

    public static function getListMerged($hook) {
        $list_hook = static::getList($hook);
        $list = static::getList();

        if($list_hook || $list) {
            $combined = array_merge($list_hook, $list);
            ksort($combined, SORT_NATURAL);
            return array_values($combined);
        }
        return [];
    }

}