<?php

namespace HydroFeature\Content\DocTree;

class BlockRenderer {
    /**
     * @var BlockHandler[]
     */
    static $block_handler = [
        'content'  => BlockHandler\ContentBlock::class,
        'mainText' => BlockHandler\MainText::class,
    ];

    /**
     * @var bool|BlockHandler
     */
    protected $handler = false;

    /**
     * BlockRenderer constructor.
     *
     * @param \HydroFeature\Content\DocTree\DocTreeBlockItem  $block
     * @param \HydroFeature\Content\ContentModel\ContentModel $content_model
     */
    public function __construct($block, $content_model) {
        if(isset(static::$block_handler[$block->type])) {
            /**
             * @var BlockHandler $classname
             */
            $classname = static::$block_handler[$block->type];
            $this->handler = new $classname($block, $content_model);
        } else {
            // todo: fail silent or throw?
            error_log('Handler not registered for block-type: ' . $block->type);
        }
    }

    /**
     * @return string
     */
    public function render() {
        if($this->handler) {
            return $this->handler->render();
        }

        return '';
    }
}