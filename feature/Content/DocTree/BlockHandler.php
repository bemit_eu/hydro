<?php

namespace HydroFeature\Content\DocTree;

use HydroFeature\Content\ContentModel\ContentModel;

interface BlockHandler {
    function __construct(DocTreeBlockItem $block, ContentModel $content_model);

    function render(): string;
}