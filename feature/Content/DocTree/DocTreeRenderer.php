<?php

namespace HydroFeature\Content\DocTree;

class DocTreeRenderer {
    protected $block;
    protected $content_model;

    /**
     * DocTreeRenderer constructor.
     *
     * @param DocTreeBlockItem                                $block
     * @param \HydroFeature\Content\ContentModel\ContentModel $content_model
     */
    public function __construct($block, $content_model) {
        $this->block = $block;
        $this->content_model = $content_model;
    }

    public function render() {
        if($this->block->type) {
            try {
                return $this->renderBlock($this->block);
            } catch(\Exception $e) {
                error_log($e->getMessage());
            }
        } else if($this->block->children) {
            // for root_block, these only have children and are the most generic block type; in future also: grid-block
            // grid-block: grid nested blocks which will be returned as numeric values to build from view with loops
            // where the `col` is set for "desktop"/"render target specific" and smaller devices are calculated through css automatically
            // e.g. {% foreach grid_block as grid_block_item %}<div class="col-md-{{ grid_block_item.col }}{% endfor %}
            return $this->renderChildren($this->block->children);
        }

        // empty doc_tree block
        return '';
    }

    /**
     * @param \HydroFeature\Content\DocTree\DocTreeBlockItem $block
     *
     * @return mixed
     * @throws \Exception
     */
    public function renderBlock($block) {
        if($block->type) {
            $block_renderer = new BlockRenderer($block, $this->content_model);

            return $block_renderer->render();
        }
        throw new \Exception('DocTreeRenderer: renderBlock without a type found, ' . ($block->id ? 'id is: ' . $block->id : 'has no id.'));
    }

    public function renderChildren($children) {
        $rendered_children = [];
        if($children) {
            foreach($children as $child_raw) {
                $child_doc_tree = new DocTreeRenderer(new DocTreeBlockItem($child_raw), $this->content_model);
                $rendered_children[] = $child_doc_tree->render();
            }
        }

        return $rendered_children;
    }
}