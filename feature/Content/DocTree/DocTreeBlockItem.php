<?php

namespace HydroFeature\Content\DocTree;

/**
 * unified class for one root_block or an block, as a root_block is just a block with children and nothing else
 *
 * @package HydroFeature\Content\DocTree
 */
class DocTreeBlockItem {
    public $name = false;
    public $id = false;
    public $type = false;
    public $data = false;
    public $children = false;

    public function __construct($raw, $name = false) {
        if($name) {
            // children doctrees don't have a name as they are existing in an numeric array
            $this->name = $name;
        }

        if(isset($raw['id'])) {
            $this->id = $raw['id'];
        }

        if(isset($raw['type'])) {
            $this->type = $raw['type'];
        }

        if(isset($raw['data'])) {
            $this->data = $raw['data'];
        }

        if(isset($raw['children'])) {
            $this->children = $raw['children'];
        }
    }
}