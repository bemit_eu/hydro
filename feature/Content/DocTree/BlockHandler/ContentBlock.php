<?php

namespace HydroFeature\Content\DocTree\BlockHandler;

use Hydro\Container;
use HydroFeature\Container as FeatureContainer;
use HydroFeature\Content\DocTree\BlockHandler;
use HydroFeature\Content\DocTree\DocTreeBlockItem;
use HydroFeature\Content\DocTree\DocTreeRenderer;
use HydroFeature\Template\TemplateData;

class ContentBlock implements BlockHandler {
    use TemplateData;

    /**
     * @var bool|\HydroFeature\Content\DocTree\DocTreeBlockItem data for the DocTree Block Item
     */
    protected $block_doctree = false;
    /**
     * @var bool|\HydroFeature\Content\StorageItem\Block data retrieved for block by the id of `$block_doctree`
     */
    protected $block_store = false;

    /**
     * ContentBlock constructor.
     *
     * @param \HydroFeature\Content\DocTree\DocTreeBlockItem  $block
     * @param \HydroFeature\Content\ContentModel\ContentModel $content_model
     *
     * @throws \Exception
     */
    public function __construct(DocTreeBlockItem $block, $content_model) {
        if(!$block->id) {
            throw new \Exception('BlockHandler\ContentBlock created without id');
        }

        $block_storage = FeatureContainer::_contentBlock()::get($block->id, $content_model->article->getHook());
        if(!$block_storage) {
            // fallback to global block if not existing especially for hook
            $block_storage = FeatureContainer::_contentBlock()::get($block->id);
        }
        if(!$block_storage) {
            throw new \Exception('ContentBlock: requested ContentBlock `' . $block->id . '` is in DocTree but is not a defined block from any hook.');
        }

        $this->block_doctree = $block;
        $this->assignTemplateData('block_data', $this->block_doctree->data);

        $this->block_store = $block_storage;

        if($block->children) {
            $children_doctree = new DocTreeRenderer(
                new DocTreeBlockItem([
                    'children' => $block->children,
                ]),
                $content_model
            );
            // $children_doctree->render();
            $this->assignTemplateData('children', $children_doctree);
        }
    }

    public function render(): string {
        if($this->block_store->tpl) {
            // render/combining data from DocTree with template from stored block
            $template = FeatureContainer::_template()->render($this->block_store->tpl, $this->template_data);

            if($template) {
                return $template;
            }
        }

        return '';
    }
}