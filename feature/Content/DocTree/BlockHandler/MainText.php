<?php

namespace HydroFeature\Content\DocTree\BlockHandler;

use HydroFeature\Container as FeatureContainer;
use HydroFeature\Content\ContentModel\ContentModel;
use HydroFeature\Content\DocTree\BlockHandler;
use HydroFeature\Content\DocTree\DocTreeBlockItem;

class MainText implements BlockHandler {
    protected $content_model;
    protected $block;

    /**
     * MainText constructor.
     *
     * @param \HydroFeature\Content\DocTree\DocTreeBlockItem  $block
     * @param \HydroFeature\Content\ContentModel\ContentModel $content_model
     *
     * @throws \Exception
     */
    public function __construct(DocTreeBlockItem $block, ContentModel $content_model) {
        $this->content_model = $content_model;

        $block_storage = FeatureContainer::_contentBlock()::get($block->id, $content_model->article->getHook());
        if(!$block_storage) {
            // fallback to global block if not existing especially for hook
            $block_storage = FeatureContainer::_contentBlock()::get($block->id);
        }
        if(!$block_storage) {
            throw new \Exception('ContentBlock: requested Block `' . $block->id . '` is in DocTree but is not a defined block from any hook.');
        }

        $this->block = $block_storage;

    }

    public function render(): string {
        $main_text = FeatureContainer::_template()->renderString(FeatureContainer::_template()->md()->text($this->content_model->article_data->getMainText()));
        if($this->block->tpl) {
            // render the block template with pushed in main_text
            $template = FeatureContainer::_template()->render($this->block->tpl, ['main_text' => $main_text]);

            if($template) {
                return $template;
            }
        }

        return $main_text;
    }
}