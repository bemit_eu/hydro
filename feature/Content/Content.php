<?php

namespace HydroFeature\Content;

use Flood\Component\Sonar\Annotation\Service as Service;
use HydroFeature\Container;
use HydroFeature\Content\ContentModel\ContentModel;

/**
 * Class Content
 *
 * @Service(id="feature-content", executed=false)
 * @package HydroFeature\Content
 */
class Content {

    protected $storageSection;
    protected $storageArticle;

    public function __construct() {
        $this->storageSection = new StorageHandler\Section();
        $this->storageArticle = new StorageHandler\Article();
    }

    public function createSection($name, $hook) {
        return $this->storageSection->create($name, $hook);
    }

    public function createArticle($section, $name, $hook, $tag = false) {
        return $this->storageArticle->create($section, $name, $hook, $tag);
    }

    public function createArticleData($article, $locale, $page_type, $hook) {
        $pageType = Container::_contentPageType()::get($page_type, $hook);
        if(!$pageType) {
            // fallback to global page-type if none especially for hook exists
            $pageType = Container::_contentPageType()::get($page_type);
        }

        return $this->storageArticle->createData($article, $locale, [
            'page_type' => $page_type,
            'meta' => [
                '_schema' => $pageType['schemaDefault'],
            ],
            'main_text' => '',
            'doc_tree' => [],
        ]);
    }

    public function getSections($hook = false) {
        if($hook) {
            $sections = $this->storageSection->getByHook($hook);
        } else {
            $sections = $this->storageSection->getAll();
        }
        if($sections) {
            foreach($sections as &$section) {
                $id = $section['ID'];
                unset ($section['ID']);
                $section['id'] = $id;
            }

            return $sections;
        }

        return [];
    }

    /**
     * @param $id
     *
     * @return bool|\HydroFeature\Content\StorageItem\Section
     */
    public function getSection($id) {
        $section = $this->storageSection->getById($id);
        if($section) {
            try {
                return new StorageItem\Section($section);
            } catch(\Exception $e) {
                error_log('HydroFeature\Content: data is invalid for section id `' . $id . '`');

                return false;
            }
        }

        return false;
    }

    public function findArticles($condition = []) {
        $articles = $this->storageArticle->getAll($condition);
        if($articles) {
            foreach($articles as &$article) {
                $id = $article['ID'];
                unset ($article['ID']);
                $article['id'] = $id;
            }

            return $articles;
        }

        return [];
    }

    public function getArticles($hook = false) {
        if($hook) {
            $articles = $this->storageArticle->getAll(['hook' => $hook]);
        } else {
            $articles = $this->storageArticle->getAll();
        }
        if($articles) {
            foreach($articles as &$article) {
                $id = $article['ID'];
                unset ($article['ID']);
                $article['id'] = $id;
            }

            return $articles;
        }

        return [];
    }

    /**
     * @param $id
     *
     * @return bool|\HydroFeature\Content\StorageItem\Article
     */
    public function getArticle($id) {
        $article = $this->storageArticle->getById($id);
        if($article) {
            try {
                return new StorageItem\Article($article);
            } catch(\Exception $e) {
                error_log('HydroFeature\Content: data is invalid for article id `' . $id . '`. ' . $e->getMessage());

                return false;
            }
        }

        return false;
    }

    /**
     * @param $tag
     * @param $hook
     *
     * @return bool|\HydroFeature\Content\StorageItem\Article
     */
    public function getArticleByTag($tag, $hook) {
        $article = $this->storageArticle->getByTag($tag,$hook);
        if($article) {
            try {
                return new StorageItem\Article($article);
            } catch(\Exception $e) {
                error_log('HydroFeature\Content: data is invalid for article tag `' . $tag . '`');
            }
        }

        return false;
    }

    /**
     * @param $data_id
     *
     * @return bool|\HydroFeature\Content\StorageItem\ArticleData
     */
    public function getArticleData($data_id) {
        $data = $this->storageArticle->getDataById($data_id);
        if($data) {
            try {
                return new StorageItem\ArticleData($data);
            } catch(\Exception $e) {
                error_log('HydroFeature\Content: data is invalid for article_data id `' . $data_id . '`');

                return false;
            }
        }

        return false;
    }

    /**
     * @param \HydroFeature\Content\StorageItem\ArticleData $article_data
     * @param \HydroFeature\Content\StorageItem\Article $article
     *
     * @return ContentModel|bool
     */
    public function buildContentModel($article_data, $article) {
        try {
            $content_model = new ContentModel($article_data, $article);

            return $content_model;
        } catch(\Exception $e) {
            error_log('Content: tried to build ContentMode: ' . $e->getMessage());
            return false;
        }
    }
}