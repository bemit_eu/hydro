<?php

namespace HydroFeature\Content;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * @Service(id="feature-content-page-type", executed=false)
 * @package HydroFeature\Content
 */
class PageType {
    static $store = [];
    static $store_hook = [];

    public static function register($id, $path, $hook = false) {
        if($hook) {
            if(!is_array(static::$store_hook[$hook])) {
                static::$store_hook[$hook] = [];
            }
            static::$store_hook[$hook][$id] = [
                'loaded' => false,
                'path' => $path,
            ];
        } else {
            static::$store[$id] = [
                'loaded' => false,
                'path' => $path,
            ];
        }
    }

    public static function get($id, $hook = false) {
        if($hook) {
            return static::getInHook($id, $hook);
        } else {
            return static::getGlobal($id);
        }
    }

    protected static function getGlobal($id) {
        if(!isset(static::$store[$id])) {
            return false;
        }
        if(static::$store[$id]['loaded']) {
            return static::$store[$id]['value'];
        }

        $page_type = \Flood\Component\Json\Json::decodeFile(static::$store[$id]['path'], true);
        if($page_type) {
            static::$store[$id]['loaded'] = true;
            static::$store[$id]['value'] = $page_type;

            return $page_type;
        }

        return false;
    }

    protected static function getInHook($id, $hook) {
        if(!isset(static::$store_hook[$hook][$id])) {
            return false;
        }
        if(static::$store_hook[$hook][$id]['loaded']) {
            return static::$store_hook[$hook][$id]['value'];
        }

        $page_type = \Flood\Component\Json\Json::decodeFile(static::$store_hook[$hook][$id]['path'], true);
        if($page_type) {
            static::$store_hook[$hook][$id]['loaded'] = true;
            static::$store_hook[$hook][$id]['value'] = $page_type;

            return $page_type;
        }

        return false;
    }

    public static function getList($hook = false) {
        if($hook) {
            if(isset(static::$store_hook[$hook])) {
                return array_keys(static::$store_hook[$hook]);
            }
            return [];
        } else {
            return array_keys(static::$store);
        }
    }

    public static function getListMerged($hook) {
        $list_hook = static::getList($hook);
        $list = static::getList();

        if($list_hook || $list) {
            // merge the two arrays, remove unique, make true JSON array - as unique preserves keys and [0:'', 2:''] which would be {'0':'', '2':''} instead of ['','']
            $combined = array_values(array_unique(array_merge($list_hook, $list)));
            sort($combined, SORT_NATURAL);

            return $combined;
        }
        return [];
    }
}