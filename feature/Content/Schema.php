<?php

namespace HydroFeature\Content;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * @Service(id="feature-content-schema", executed=false)
 * @package HydroFeature\Content
 */
class Schema {
    static $store = [];
    static $store_hook = [];

    public static function register($id, $path, $hook = false) {
        if($hook) {
            if(!is_array(static::$store_hook[$hook])) {
                static::$store_hook[$hook] = [];
            }
            static::$store_hook[$hook][$id] = [
                'loaded' => false,
                'path'   => $path,
            ];
        } else {
            static::$store[$id] = [
                'loaded' => false,
                'path'   => $path,
            ];
        }
    }

    public static function get($id, $hook = false) {
        if($hook) {
            return static::getInHook($id, $hook);
        } else {
            return static::getGlobal($id);
        }
    }

    protected static function getGlobal($id) {
        if(!isset(static::$store[$id])) {
            return false;
        }
        if(static::$store[$id]['loaded']) {
            return static::$store[$id]['value'];
        }

        $schema = \Flood\Component\Json\Json::decodeFile(static::$store[$id]['path'], true);
        if($schema) {
            static::$store[$id]['loaded'] = true;
            static::$store[$id]['value'] = $schema;

            return $schema;
        }

        return false;
    }

    protected static function getInHook($id, $hook) {
        if(!isset(static::$store_hook[$hook][$id])) {
            return false;
        }
        if(static::$store_hook[$hook][$id]['loaded']) {
            return static::$store_hook[$hook][$id]['value'];
        }

        $schema = \Flood\Component\Json\Json::decodeFile(static::$store_hook[$hook][$id]['path'], true);
        if($schema) {
            static::$store_hook[$hook][$id]['loaded'] = true;
            static::$store_hook[$hook][$id]['value'] = $schema;

            return $schema;
        }

        return false;
    }
}