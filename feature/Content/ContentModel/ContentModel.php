<?php

namespace HydroFeature\Content\ContentModel;

use HydroFeature\Content\DocTree;

class ContentModel {
    public $article_data;
    public $article;

    protected $raw_meta = false;
    protected $raw_maintext = false;
    protected $raw_doctree = false;

    protected $rendered_block = [];

    /**
     * ContentModel constructor.
     *
     * @param \HydroFeature\Content\StorageItem\ArticleData $data
     * @param \HydroFeature\Content\StorageItem\Article     $article
     *
     * @throws
     *
     */
    public function __construct($data, $article) {
        $has_content = false;

        $this->article_data = $data;
        $this->article = $article;

        if($data->getMeta()) {
            $has_content = true;
            $this->raw_meta = $data->getMeta();
        }
        if($data->getMainText()) {
            $has_content = true;
            $this->raw_maintext = $data->getMainText();
        }
        if($data->getDocTree()) {
            $has_content = true;
            $this->raw_doctree = $data->getDocTree();
        }

        if(!$has_content) {
            throw new \Exception('ContentModel ContentModelCreatedWithNoContent');
        }
    }

    /**
     * Renders a root_block that was saved for an article, e.g. defined in PageType
     *
     * @param string $name of the root_block
     *
     * @return array|mixed|string
     */
    public function renderBlock($name) {
        if($this->raw_doctree && isset($this->raw_doctree[$name])) {

            $doc_tree = new DocTree\DocTreeRenderer(
                new DocTree\DocTreeBlockItem($this->raw_doctree[$name], $name),
                $this
            );

            return $doc_tree->render();
        }

        return '';
    }
}