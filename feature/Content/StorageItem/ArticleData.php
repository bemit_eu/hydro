<?php

namespace HydroFeature\Content\StorageItem;

class ArticleData {
    protected $id;
    protected $article;
    protected $locale;
    protected $date_create;
    protected $date_update;
    protected $page_type;
    protected $meta;
    protected $main_text;
    protected $doc_tree;

    protected $storage;

    /**
     * User constructor.
     *
     * @param $data
     *
     * @throws \Exception
     */
    public function __construct($data) {
        if(!isset($data['ID'])) {
            throw new \Exception('Tried to create content_article_data without `ID`');
        }

        if(!isset($data['article'])) {
            throw new \Exception('Tried to create content_article_data without `article`');
        }

        if(!isset($data['locale'])) {
            throw new \Exception('Tried to create content_article_data without `locale`');
        }

        if(!isset($data['date_create'])) {
            throw new \Exception('Tried to create content_article_data without `date_create`');
        }

        if(!isset($data['date_update'])) {
            throw new \Exception('Tried to create content_article_data without `date_update`');
        }

        $this->id = $data['ID'];
        $this->article = $data['article'];
        $this->locale = $data['locale'];
        $this->date_create = $data['date_create'];
        $this->date_update = $data['date_update'];
        if(isset($data['page_type'])) {
            $this->page_type = $data['page_type'];
        }
        if(isset($data['meta'])) {
            // todo: find better json conversion/support, should be decoded from DB without explicitly telling the column name in storagehandler
            if(is_string($data['meta'])) {
                $data['meta'] = json_decode($data['meta'], true);
            }

            if(is_array($data['meta'])) {
                $this->meta = $data['meta'];
            } else {
                error_log('ArticleData.construct: meta for id `' . $this->id . '` not valid');
            }
        }
        if(isset($data['main_text'])) {
            $this->main_text = $data['main_text'];
        }
        if(isset($data['doc_tree'])) {
            // todo: find better json conversion/support, should be decoded from DB without explicitly telling the column name in storagehandler
            if(is_string($data['doc_tree'])) {
                $data['doc_tree'] = json_decode($data['doc_tree'], true);
            }
            if(is_array($data['doc_tree'])) {
                $this->doc_tree = $data['doc_tree'];
            } else {
                error_log('ArticleData.construct: doc_tree for id `' . $this->id . '` not valid');
            }
        }

        $this->storage = new \HydroFeature\Content\StorageHandler\Article();
    }

    /**
     * For API
     *
     * @todo refactor to somewhere else
     *
     * @deprecated
     * @return array
     */
    public function exportData() {
        return [
            'id'          => $this->getId(),
            'article'     => $this->getArticle(),
            'locale'      => $this->getLocale(),
            'date_create' => $this->getDateCreate(),
            'date_update' => $this->getDateUpdate(),
            'page_type'   => $this->getPageType(),
            'meta'        => $this->getMeta(),
            'mainText'    => $this->getMainText(),
            'docTree'     => $this->getDocTree(),
        ];
    }

    /**
     * @return null|string id for this data object
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return null|string id of the article
     */
    public function getArticle() {
        return $this->article;
    }

    /**
     * @return null|string id of the locale
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * @return null|string timestamp
     */
    public function getDateCreate() {
        return $this->date_create;
    }

    /**
     * @return null|string timestamp
     */
    public function getDateUpdate() {
        return $this->date_update;
    }

    /**
     * @return null|string id of the page_type
     */
    public function getPageType() {
        return $this->page_type;
    }

    /**
     * @return null|array all schema based meta information for article
     */
    public function getMeta() {
        return $this->meta;
    }

    /**
     * @param $meta
     *
     * @throws \Exception
     */
    public function setMeta($meta) {
        // todo: define better "defaults" which will be extracted from the existing meta
        if(isset($this->meta['_schema'])) {
            $meta['_schema'] = $this->meta['_schema'];
        }

        if(is_array($meta)) {
            $this->meta = $meta;

            return;
        }

        throw new \Exception('ArticleData.setMeta fail: not an array');
    }

    public function getMainText() {
        return $this->main_text;
    }

    /**
     * @param $main_text
     *
     * @throws \Exception
     */
    public function setMainText($main_text) {
        if(is_string($main_text)) {
            $this->main_text = $main_text;

            return;
        }

        throw new \Exception('ArticleData.setMainText fail: not a string');
    }

    public function getDocTree() {
        return $this->doc_tree;
    }

    /**
     * @param $doc_tree
     *
     * @throws \Exception
     */
    public function setDocTree($doc_tree) {
        if(is_array($doc_tree)) {
            $this->doc_tree = $doc_tree;

            return;
        }

        throw new \Exception('ArticleData.setDocTree fail: not an array');
    }

    public function save() {
        $updated = $this->storage->updateData($this->getId(), [
            'article'     => $this->getArticle(),
            'locale'      => $this->getLocale(),
            'date_create' => $this->getDateCreate(),
            'date_update' => $this->getDateUpdate(),
            'page_type'   => $this->getPageType(),
            'meta'        => json_encode($this->getMeta()),
            'main_text'   => $this->getMainText(),
            'doc_tree'    => json_encode($this->getDocTree()),
        ]);

        if(is_int($updated)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete() {
        $deleted = $this->storage->deleteData($this->getId());

        if(is_int($deleted)) {
            return true;
        } else {
            return false;
        }
    }
}