<?php

namespace HydroFeature\Content\StorageItem;

use \HydroFeature\Content\StorageHandler;

class Section {
    protected $id;
    protected $name;
    protected $hook;
    protected $articles = false;

    protected $storage;
    protected $storageArticle;

    /**
     * User constructor.
     *
     * @param $data
     *
     * @throws \Exception
     */
    public function __construct($data) {
        if(!isset($data['ID'])) {
            throw new \Exception('Tried to create content_section without `ID`');
        }

        if(!isset($data['name'])) {
            throw new \Exception('Tried to create content_section without `name`');
        }

        if(!isset($data['hook'])) {
            throw new \Exception('Tried to create content_section without `hook`');
        }

        $this->id = $data['ID'];
        $this->name = $data['name'];
        $this->hook = $data['hook'];

        $this->storage = new StorageHandler\Section();
        $this->storageArticle = new StorageHandler\Article();
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getHook() {
        return $this->hook;
    }

    public function getArticles() {
        if($this->articles) {
            return $this->articles;
        }

        $articles = $this->storageArticle->getBySection($this->id);

        if(is_array($articles)) {
            foreach($articles as &$article) {
                $id = $article['ID'];
                unset ($article['ID']);
                $article['id'] = $id;
            }

            $this->articles = $articles;
            return $articles;
        }

        return false;
    }

    public function save() {
        $updated = $this->storage->update($this->getId(), [
            'name' => $this->getName(),
        ]);

        if(is_int($updated)) {

            return true;
        } else {
            return false;
        }
    }

    public function delete() {
        $updated = $this->storage->delete($this->getId());

        if(is_int($updated)) {

            return true;
        } else {
            return false;
        }
    }
}