<?php

namespace HydroFeature\Content\StorageItem;

use Hydro\Container;
use HydroApi\HookHydroApi;
use HydroFeature\Container as FeatureContainer;

class Article {
    protected $id;
    protected $section;
    protected $tag;
    protected $name;
    protected $hook;

    /**
     * @var bool|array
     */
    protected $locales = false;
    /**
     * @var bool|ArticleData[]
     */
    protected $data = false;

    protected $storage;

    /**
     * User constructor.
     *
     * @param $data
     *
     * @throws \Exception
     */
    public function __construct($data) {
        if(!isset($data['ID'])) {
            throw new \Exception('Tried to create content_article without `ID`');
        }

        if(!isset($data['section'])) {
            throw new \Exception('Tried to create content_article without `section`');
        }

        if(!isset($data['name'])) {
            throw new \Exception('Tried to create content_article without `name`');
        }

        if(!isset($data['hook'])) {
            throw new \Exception('Tried to create content_article without `hook`');
        }

        $this->id = $data['ID'];
        $this->section = $data['section'];
        $this->name = $data['name'];
        $this->hook = $data['hook'];
        if(isset($data['tag'])) {
            $this->tag = $data['tag'];
        }

        $this->storage = new \HydroFeature\Content\StorageHandler\Article();
    }

    /**
     * For API
     *
     * @todo refactor to somewhere else
     *
     * @deprecated
     * @return array
     */
    public function exportData() {
        $hook = Container::_hookStorage()->get($this->getHook());

        $value = [
            'id'                => $this->getId(),
            'section'           => $this->getSection(),
            'name'              => $this->getName(),
            'tag'               => $this->getTag(),
            'hook'              => $this->getHook(),
            'locales'           => $this->getLocales(),
            // todo: dynamic default locale
            'localeDefault'     => '*',
            'localesPossible'   => $hook->getRouteLocaleList(),
            'pageTypesPossible' => FeatureContainer::_contentPageType()::getListMerged($this->getHook()),
            'data'              => [],
        ];

        foreach($this->getLocales() as $locale) {
            $data = $this->getData($locale);
            if($data) {
                $value['data'][$locale] = $data->exportData();
            }
        }

        return $value;
    }

    public function getId() {
        return $this->id;
    }

    public function getSection() {
        return $this->section;
    }

    /**
     * @param $section
     *
     * @throws \Exception
     */
    public function setSection($section) {
        if('' === trim($section)) {
            throw new \Exception('StorageItem\Article: setSection received empty section');
        } else {
            $this->section = trim($section);
        }
    }

    public function getTag() {
        return $this->tag;
    }

    public function setTag($tag) {
        if('' === trim($tag)) {
            $this->tag = null;
        } else {
            $this->tag = trim($tag);
        }
    }

    public function getName() {
        return $this->name;
    }

    public function getHook() {
        return $this->hook;
    }

    /**
     * @param $name
     *
     * @throws \Exception
     */
    public function setName($name) {
        if('' === trim($name)) {
            throw new \Exception('StorageItem\Article: setName received empty name');
        } else {
            $this->name = trim($name);
        }
    }

    public function getLocales() {
        if($this->locales) {
            return $this->locales;
        }

        $this->initData();

        return $this->locales;
    }

    /**
     * @param bool $locale
     *
     * @return bool|\HydroFeature\Content\StorageItem\ArticleData
     */
    public function getData($locale) {
        if(!$this->data) {
            $this->initData();
        }

        if($locale && isset($this->data[$locale])) {
            return $this->data[$locale];
        }

        return false;
    }

    protected function initData() {
        $data = $this->storage->getDataByArticle($this->id);

        if(is_array($data)) {
            $this->locales = [];
            $this->data = [];
            foreach($data as $d) {
                try {
                    $data_item = new ArticleData($d);
                    $this->locales[] = $data_item->getLocale();
                    $this->data[$data_item->getLocale()] = $data_item;
                } catch(\Exception $e) {
                    error_log('Article Data not valid for article `' . $this->id . '` ' . $e->getMessage());
                }
            }
        }
    }

    public function save() {
        $updated = $this->storage->update($this->getId(), [
            'section' => $this->getSection(),
            'name'    => $this->getName(),
            'tag'     => $this->getTag(),
        ]);

        if(is_int($updated)) {

            return true;
        } else {
            return false;
        }
    }

    public function delete() {
        $updated = $this->storage->delete($this->getId());

        if(is_int($updated)) {

            return true;
        } else {
            return false;
        }
    }
}