<?php

namespace HydroFeature\Content\StorageItem;


class Block {
    public $path;
    public $type = false;
    public $hook = false;
    public $schema = false;
    public $tpl = false;

    public function __construct($path, $raw) {
        $this->path = $path;

        if(isset($raw['type'])) {
            $this->type = $raw['type'];
        }
        if(isset($raw['schema'])) {
            $this->schema = $raw['schema'];
        }
        if(isset($raw['tpl'])) {
            $this->tpl = $raw['tpl'];
        }
    }
}