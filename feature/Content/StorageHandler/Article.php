<?php

namespace HydroFeature\Content\StorageHandler;

use Hydro\Container;

class Article {
    protected static $table = 'content_article';
    protected static $column = ['ID', 'section', 'name', 'tag', 'hook'];

    protected static $table_data = 'content_article_data';
    protected static $column_data = ['ID', 'article', 'locale', 'date_create', 'date_update', 'page_type', 'meta', 'main_text', 'doc_tree'];

    public function __construct() {
    }

    public function create($section, $name, $hook, $tag = false, $tried = false) {
        $id = Container::_storageData()->randomId();
        $data = [
            'ID' => $id,
            'name' => $name,
            'hook' => $hook,
            'section' => $section,
        ];
        if($tag) {
            $data['tag'] = $tag;
        }
        $inserted = Container::_storageData()->insert(
            static::$table,
            $data
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->create($section, $name, $hook, $tag, $tried);
    }

    public function createData($article, $locale, $optional = [], $tried = false) {
        $id = Container::_storageData()->randomId();
        $data = [
            'ID' => $id,
            'article' => $article,
            'locale' => $locale,
        ];
        if(isset($optional['page_type']) && $optional['page_type']) {
            $data['page_type'] = $optional['page_type'];
        }
        if(isset($optional['meta'])) {
            if(is_string($optional['meta'])) {
                $data['meta'] = $optional['meta'];
            } else if(is_array($optional['meta'])) {
                $data['meta'] = json_encode($optional['meta']);
            }
        }
        if(isset($optional['main_text'])) {
            $data['main_text'] = $optional['main_text'];
        }
        if(isset($optional['doc_tree'])) {
            if(is_string($optional['doc_tree'])) {
                $data['doc_tree'] = $optional['doc_tree'];
            } else if(is_array($optional['doc_tree'])) {
                $data['doc_tree'] = json_encode($optional['doc_tree']);
            }
        }
        $inserted = Container::_storageData()->insert(
            static::$table_data,
            $data
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->createData($article, $locale, $optional, $tried);
    }

    public function getById($id) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['ID' => $id]);
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(\Exception $e) {
            error_log('Article Storage getById error: ' . $e->getMessage());

            return false;
        }
    }

    public function getByTag($tag, $hook) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, [
            'AND' => [
                'tag' => $tag,
                'hook' => $hook,
            ],
        ]);

        if($stmt) {
            try {
                $stmt->execute();

                return $stmt->fetch();
            } catch(\Exception $e) {
                error_log('Article Storage getByTag error: ' . $e->getMessage());
            }
        }
        return false;
    }

    public function getBySection($sid) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['section' => $sid]);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('Article Storage getBySection error: ' . $e->getMessage());

            return false;
        }
    }

    public function getAll($condition = []) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, $condition);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('Article Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }

    public function getDataById($data_id) {
        $stmt = Container::_storageData()->select(static::$column_data, static::$table_data, ['ID' => $data_id]);
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(\Exception $e) {
            error_log('Article Storage getDataById error: ' . $e->getMessage());

            return false;
        }
    }

    public function getDataByArticle($article_id) {
        $stmt = Container::_storageData()->select(static::$column_data, static::$table_data, ['article' => $article_id]);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('Article Storage getDataByArticle error: ' . $e->getMessage());

            return false;
        }
    }

    public function update($article_id, $data) {
        try {
            return Container::_storageData()->update(static::$table,
                $data,
                ['ID' => $article_id]);
        } catch(\Exception $e) {
            error_log('Article Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function updateData($data_id, $data) {
        try {
            return Container::_storageData()->update(static::$table_data,
                $data,
                ['ID' => $data_id]);
        } catch(\Exception $e) {
            error_log('Article Storage updateData error: ' . $e->getMessage());

            return false;
        }
    }

    public function deleteData($data_id) {
        try {
            return Container::_storageData()->delete(static::$table_data, ['ID' => $data_id]);
        } catch(\Exception $e) {
            error_log('Article Storage deleteData error: ' . $e->getMessage());

            return false;
        }
    }

    public function deleteDataFromArticle($art_id) {
        try {
            return Container::_storageData()->delete(static::$table_data, ['article' => $art_id]);
        } catch(\Exception $e) {
            error_log('Article Storage deleteDataFromArticle error: ' . $e->getMessage());

            return false;
        }
    }

    public function delete($art_id) {
        try {
            $deleted = $this->deleteDataFromArticle($art_id);
            if(is_int($deleted)) {
                return Container::_storageData()->delete(static::$table, ['ID' => $art_id]);
            }
            throw  new \Exception('Deletion of data associated with article failed during deletion of article ' . $art_id);
        } catch(\Exception $e) {
            error_log('Article Storage delete error: ' . $e->getMessage());

            return false;
        }
    }
}