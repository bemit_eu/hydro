<?php

namespace HydroFeature\Content\StorageHandler;

use Hydro\Container;

class Section {
    protected static $table = 'content_section';
    protected static $column = ['ID', 'name', 'hook'];

    public function __construct() {
    }

    public function create($name, $hook, $tried = false) {
        $id = Container::_storageData()->randomId();
        $inserted = Container::_storageData()->insert(
            static::$table,
            [
                'ID'   => $id,
                'name' => $name,
                'hook' => $hook,
            ]
        );

        if($inserted) {
            return $id;
        }

        // todo: only listen on duplicate ID error

        if($tried === false) {
            $tried = 0;
        } else if($tried > 5) {
            return false;
        }
        $tried++;

        return $this->create($name, $hook, $tried);
    }

    public function getById($id) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['ID' => $id]);
        try {
            $stmt->execute();

            return $stmt->fetch();
        } catch(\Exception $e) {
            error_log('Section Storage getById error: ' . $e->getMessage());

            return false;
        }
    }

    public function getByName($name) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['name' => $name]);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('Section Storage getByName error: ' . $e->getMessage());

            return false;
        }
    }

    public function getByHook($hook) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['hook' => $hook]);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('Section Storage getByHook error: ' . $e->getMessage());

            return false;
        }
    }

    public function update($section_id, $data) {
        try {
            return Container::_storageData()->update(static::$table,
                $data,
                ['ID' => $section_id]);
        } catch(\Exception $e) {
            error_log('Section Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function delete($section_id) {
        try {
            if($this->deleteArticles($section_id)) {
                return Container::_storageData()->delete(static::$table, ['ID' => $section_id]);
            }
            throw  new \Exception('Deletion of articles within section failed `' . $section_id . '`');
        } catch(\Exception $e) {
            error_log('Section Storage delete error: ' . $e->getMessage());

            return false;
        }
    }

    protected function deleteArticles($section_id) {
        $storageArticle = new Article();
        $articles = $storageArticle->getBySection($section_id);

        if(is_array($articles)) {
            /**
             * @var bool $no_error when no deletion failed, everything is correct
             */
            $no_error = true;
            foreach($articles as $article) {
                if(!$storageArticle->delete($article['ID'])) {
                    $no_error = false;
                    error_log('Deleting articles for section `' . $section_id . '` failed for article `' . $article['ID'] . '`');
                }
            }

            return $no_error;
        }

        return false;
    }

    public function getAll() {
        $stmt = Container::_storageData()->select(static::$column, static::$table);
        try {
            $stmt->execute();

            return $stmt->fetchAll();
        } catch(\Exception $e) {
            error_log('Section Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }
}