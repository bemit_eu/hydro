<?php

namespace HydroFeature\User\StorageHandler;

use Hydro\Container;

class UserBase {
    protected static $table = 'user_base';
    protected static $column = ['ID', 'name', 'pass', 'roles', 'active'];

    public function __construct() {
    }

    public function create($name, $pass, $roles, $active) {
        $id = Container::_storageData()->randomId();
        if(
        Container::_storageData()->insert(
            static::$table,
            [
                'ID' => $id,
                'name' => $name,
                'pass' => $pass,
                'roles' => json_encode($roles),
                'active' => $active,
            ]
        )
        ) {
            return $id;
        }
        return false;
    }

    public function getById($id) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['ID' => $id]);
        $stmt->execute();

        return $stmt->fetch();
    }

    public function getByName($name) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, ['name' => $name]);
        $stmt->execute();

        return $stmt->fetch();
    }

    public function getAll($condition = []) {
        $stmt = Container::_storageData()->select(static::$column, static::$table, $condition);
        try {
            $stmt->execute();

            $res = $stmt->fetchAll();
            $users = [];
            foreach($res as $user) {
                $users[] = (new \HydroFeature\User\StorageItem\User($user))->export();
            }

            return $users;
        } catch(\Exception $e) {
            error_log('User Storage getAll error: ' . $e->getMessage());

            return false;
        }
    }

    public function update($id, $data) {
        try {
            return Container::_storageData()->update(static::$table,
                $data,
                ['ID' => $id]);
        } catch(\Exception $e) {
            error_log('User Storage update error: ' . $e->getMessage());

            return false;
        }
    }

    public function delete($id) {
        try {
            return Container::_storageData()->delete(static::$table, ['ID' => $id]);
        } catch(\Exception $e) {
            error_log('User Storage delete error: ' . $e->getMessage());

            return false;
        }
    }
}