<?php

namespace HydroFeature\User;

use HydroFeature\User\AccessManager\Ability;
use HydroFeature\Container as FeatureContainer;

class AccessManager {
    /**
     * any user is anonym, when not authenticated and hydro not running on the cli
     */
    const ENTITY_ANONYM = 'anonym';
    /**
     * any user that is successfully verified is authenticated
     */
    const ENTITY_AUTHENTICATED = 'authenticated';
    /**
     * active use is `cli` when running through php dev server with Hydro Console, like during installation
     */
    const ENTITY_CLI = 'cli';

    /**
     * Default Role `manager`
     */
    const ROLE_MANAGER = 'manager';
    /**
     * Default Role `customer`
     */
    const ROLE_CUSTOMER = 'customer';

    /*
     * General Actions
     */
    const ACTION_CREATE = 'generic:create';
    const ACTION_READ = 'generic:read';
    const ACTION_UPDATE = 'generic:update';
    const ACTION_DELETE = 'generic:delete';

    /**
     * @var Ability[] general non-hook-context abilities
     */
    protected $abilities = [];

    /**
     * @var [Ability[]] abilities that apply only to hook-context
     */
    protected $hook_abilities = [];

    /**
     * @param string      $id
     * @param string      $action
     * @param null|mixed  $value
     * @param bool|mixed  $defaults
     * @param bool|string $hook_context if not against the current user's hook should be checked against but another like the file requested to delete
     *
     * @return bool
     */
    public function isAllowed($id, $action, $value = null, $defaults = false, $hook_context = false) {
        if(!isset($this->abilities[$id])) {
            // when not existing, return defaults
            return $defaults;
        }

        $entity = FeatureContainer::_userActive()->getEntity();
        $roles = [];
        $roles[] = $entity;
        if($entity === static::ENTITY_AUTHENTICATED) {
            $roles = FeatureContainer::_userActive()->getRoles();
        }

        if(!$hook_context) {
            $hook_context = FeatureContainer::_userActive()->getHookContext();
        }
        if($hook_context) {
            if(isset($this->hook_abilities[$id][$hook_context])) {
                // only if the ability is registered for an hook, use that definition and ignore non-hook abilities
                // so if abilities are NOT defined for an hook it falls back to the root definition
                /**
                 * @var Ability $hook_ability
                 */
                $hook_ability = $this->hook_abilities[$id][$hook_context];
                return $hook_ability->check($roles, $action, $value, $defaults);
            }
        }

        return $this->abilities[$id]->check($roles, $action, $value, $defaults);
    }

    /**
     * Get all non-restricted allowed abilities
     *
     * @param string $id
     * @param bool   $hook_context if it should return only for one hook_context
     * @param bool   $rm_namespace
     *
     * @return array|bool
     */
    public function getAllowed($id, $hook_context = false, $rm_namespace = false) {
        if($hook_context) {
            if(!isset($this->hook_abilities[$id][$hook_context])) {
                return false;
            }
        } else if(!isset($this->abilities[$id])) {
            return false;
        }

        if($hook_context) {
            $ability = $this->hook_abilities[$id][$hook_context];
        } else {
            $ability = $this->abilities[$id];
        }

        $entity = FeatureContainer::_userActive()->getEntity();
        $roles = [];
        $roles[] = $entity;
        if($entity === static::ENTITY_AUTHENTICATED) {
            $roles = FeatureContainer::_userActive()->getRoles();
        }

        return $ability->get($roles, $rm_namespace);
    }

    /**
     * @param      $id
     * @param bool $hook
     *
     * @return Ability
     */
    public function ability($id, $hook = false) {
        if($hook) {
            if(!isset($this->hook_abilities[$id])) {
                $this->hook_abilities[$id] = [];
            }
            if(!isset($this->hook_abilities[$id][$hook])) {
                $this->hook_abilities[$id][$hook] = new Ability($id);
            }

            return $this->hook_abilities[$id][$hook];
        }

        if(!isset($this->abilities[$id])) {
            $this->abilities[$id] = new Ability($id);
        }

        return $this->abilities[$id];
    }

    /**
     * Careful! Binds an ability also to be the hooks ability, connects them through reference.
     *
     * A ability will be registered when not existing, so it is possible to bind a hook even when the ability will be defined later
     *
     * @param      $hook
     * @param      $id
     * @param bool $overwrite when `true` it will overwrite existing hook abilities, when `false` and it already exists creates logs error
     */
    public function hookBindAbility($hook, $id, $overwrite = true) {
        if(!isset($this->abilities[$id])) {
            $this->abilities[$id] = new Ability($id);
        }

        if(!isset($this->hook_abilities[$id])) {
            $this->hook_abilities[$id] = [];
        }

        if((isset($this->hook_abilities[$id][$hook]) && !$overwrite)) {
            // when exists but overwrite isn't allowed
            error_log('AccessManager: tried to bind ability to hook, but hook already got this ability, overwrite is forbidden for ability: `' . $id . '` and hook: `' . $hook . '`');
        }

        if(!isset($this->hook_abilities[$id][$hook]) || (isset($this->hook_abilities[$id][$hook]) && $overwrite)) {
            $this->hook_abilities[$id][$hook] = &$this->abilities[$id];
        }
    }
}