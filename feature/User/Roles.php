<?php

namespace HydroFeature\User;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * @Service(id="feature-user-roles", executed=false)
 * @package HydroFeature\User
 */
class Roles {
    static $store = [];

    /**
     * @param $id
     *
     * @return self
     */
    public static function register($id) {
        static::$store[$id] = true;
        return __CLASS__;
    }

    public static function getList() {
        return array_keys(static::$store);
    }
}