<?php

namespace HydroFeature\User;

use Flood\Component\Sonar\Annotation\Service as Service;
use HydroFeature\Container as FeatureContainer;
use HydroFeature\User\AccessManager\Exception\NotAllowed;

/**
 * Class User
 *
 * @Service(id="feature-user", executed=false)
 * @package HydroFeature\User
 */
class User {

    protected $storage;

    public function __construct() {
        $this->storage = new StorageHandler\UserBase();
    }

    /**
     * @param $name
     * @param $pass
     * @param $roles
     * @param $active
     *
     * @return bool
     * @throws \HydroFeature\User\AccessManager\Exception\NotAllowed
     *
     */
    public function create($name, $pass = false, $roles = null, $active = false) {
        if(
        !FeatureContainer::_accessManager()
                         ->isAllowed(
                             'feature.user',
                             AccessManager::ACTION_CREATE,
                             $name,
                             false
                         )
        ) {
            throw new NotAllowed('Action: User Create is NotAllowed');
        }
        if($pass) {
            $passwd = new Lib\Pass();
            $pass = $passwd->hash($pass);
        } else {
            $pass = null;
        }

        return $this->storage->create($name, $pass, $roles, $active);
    }

    public function getUsers($condition = []) {
        $users = $this->storage->getAll($condition);

        if(is_array($users)) {
            return $users;
        }

        return [];
    }

    /**
     * @param $id
     *
     * @return bool|\HydroFeature\User\StorageItem\User
     */
    public function getById($id) {
        $user = $this->storage->getById($id);

        if($user) {
            try {
                return new StorageItem\User($user);
            } catch(\Exception $e) {
                error_log('HydroFeature\User: data is invalid for id `' . $id . '`');

                return false;
            }
        }

        error_log('HydroFeature\User: could not get data for id `' . $id . '`');

        return false;
    }

    /**
     * @param $name
     *
     * @return bool|\HydroFeature\User\StorageItem\User
     */
    public function getByName($name) {
        $user = $this->storage->getByName($name);

        if($user) {
            try {
                return new StorageItem\User($user);
            } catch(\Exception $e) {
                error_log('HydroFeature\User: data is invalid for name `' . $name . '`');

                return false;
            }
        }

        error_log('HydroFeature\User: could not get data for name `' . $name . '`');

        return false;
    }

    /**
     * @param $input_pass
     * @param $pass
     *
     * @return bool
     */
    public function verify($input_pass, $pass) {
        $passwd = new Lib\Pass();

        return $passwd->verify($input_pass, $pass);
    }
}