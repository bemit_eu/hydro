<?php

namespace HydroFeature\User;

use Flood\Component\Sonar\Annotation\Service as Service;
use ReallySimpleJWT\Token as JWT;

/**
 * Class Token
 *
 * @Service(id="feature-user-token", executed=false)
 * @package HydroFeature\User
 */
class Token {
    /**
     * @var int how long the token should be valid, in minutes
     */
    static $max_age = 90;

    /**
     * @var string change this secret to your personal one
     */
    static $secret = 'hl<P#GkS785d1)UL';

    public static function secretFromEnv() {
        if(!getenv('HYDRO_USER_TOKEN_SECRET') && is_string(getenv('HYDRO_USER_TOKEN_SECRET'))) {
            static::$secret = getenv('HYDRO_USER_TOKEN_SECRET');
        }
    }

    public static function create($userid, $username, $roles = false, $max_age = false) {
        try {

            $payload = [
                'iat' => time(),
                'sub' => $userid,
                'username' => $username,
                'exp' => time() + ($max_age ? $max_age : (static::$max_age * 60)),
                'iss' => 'hydro',
            ];

            if($roles && !empty($roles)) {
                $payload['roles'] = $roles;
            }

            try {
                $token = JWT::customPayload($payload, static::$secret);
            } catch(\Exception $e) {
                error_log($e->getMessage());

                return false;
            }

            return $token;
        } catch(\Exception $e) {
            error_log($e->getMessage());

            return false;
        }
    }

    /**
     * Validates a token
     *
     * @param $token
     *
     * @return bool
     */
    public static function validate($token) {
        try {

            return JWT::validate($token, static::$secret);
        } catch(\Exception $e) {
            error_log('Hydro\Feature\User\Token Error:' . $e->getMessage());

            return false;
        }
    }

    /**
     * Get payload of token or false
     *
     * @param $token
     *
     * @return bool|array
     */
    public static function getPayload($token) {
        try {

            return JWT::getPayload($token, static::$secret);
        } catch(\Exception $e) {
            error_log($e->getMessage());

            return false;
        }
    }

    public static function isBearerInHeader() {
        $token = filter_input(INPUT_SERVER, 'HTTP_AUTHORIZATION', FILTER_SANITIZE_STRING);

        if(null !== $token) {
            // Token: something in AUTHORIZATION header
            if(0 !== strpos($token, 'Bearer ')) {
                // Token: JWT not valid, not containing Bearer.

                return false;
            }

            $token = str_replace('Bearer ', '', $token);
            if(1 > strlen($token)) {
                // Token: JWT not valid, is empty.

                return false;
            }

            return true;
        }

        return false;
    }

    public static function extractFromHeader() {
        $token = filter_input(INPUT_SERVER, 'HTTP_AUTHORIZATION', FILTER_SANITIZE_STRING);

        if(null !== $token) {
            $method = filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING);
            if($method === 'OPTIONS') {
                return false;
            }
            if(0 !== strpos($token, 'Bearer ')) {
                error_log('Token: JWT not valid, not containing Bearer.');

                return false;
            }

            $token = str_replace('Bearer ', '', $token);
            if(1 > strlen($token)) {
                error_log('Token: JWT not valid, is empty.');

                return false;
            }

            if(static::validate($token)) {

                return $token;
            }

            error_log('Token: from header extracted token is not valid.');
        } else {
            error_log('Token: nothing in AUTHORIZATION header.');
        }

        return false;
    }
}