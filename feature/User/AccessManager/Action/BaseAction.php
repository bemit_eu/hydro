<?php

namespace HydroFeature\User\AccessManager\Action;

abstract class BaseAction implements ActionRestricted {
    private $id;
    private $restriction;

    public function __construct($restriction = false) {
        $this->restriction = $restriction;
    }

    public function getId(): string {
        return $this->id;
    }

    protected function setId($id) {
        $this->id = $id;
    }

    /**
     * @return bool|callable
     */
    public function getRestriction() {
        return $this->restriction;
    }
}