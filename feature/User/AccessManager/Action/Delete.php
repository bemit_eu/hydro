<?php

namespace HydroFeature\User\AccessManager\Action;

use HydroFeature\User\AccessManager;

class Delete extends BaseAction implements ActionRestricted {
    public function __construct($restriction = false) {
        parent::__construct($restriction);

        $this->setId(AccessManager::ACTION_DELETE);
    }
}