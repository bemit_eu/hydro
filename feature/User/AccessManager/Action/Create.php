<?php

namespace HydroFeature\User\AccessManager\Action;

use HydroFeature\User\AccessManager;

class Create extends BaseAction implements ActionRestricted {
    public function __construct($restriction = false) {
        parent::__construct($restriction);

        $this->setId(AccessManager::ACTION_CREATE);
    }
}