<?php

namespace HydroFeature\User\AccessManager\Action;

class Action extends BaseAction implements ActionRestricted {
    public function __construct($id, $restriction = false) {
        parent::__construct($restriction);

        $this->setId($id);
    }
}