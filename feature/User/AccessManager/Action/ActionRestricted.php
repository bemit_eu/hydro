<?php

namespace HydroFeature\User\AccessManager\Action;

interface ActionRestricted {

    /**
     * @param bool|callable $restriction
     */
    public function __construct($restriction);

    /**
     * Returns the unique ID of this action
     *
     * @return string
     */
    public function getId()
    : string;

    /**
     * Returns the restriction function when one is set
     *
     * @return bool|callable
     */
    public function getRestriction();
}