<?php

namespace HydroFeature\User\AccessManager;

use HydroFeature\User\AccessManager\Ability\ActionList;
use HydroFeature\User\AccessManager\Action\ActionRestricted;

class Ability {

    protected $actions = null;
    public $id = null;

    public function __construct($id) {
        $this->actions = new ActionList();
        $this->id = $id;
    }

    /**
     * @param string[]|int[]                   $roles
     * @param bool|string[]|ActionRestricted[] $actions
     * @param bool|callable                    $restriction
     *
     * @return $this
     */
    public function grant($roles, $actions = true, $restriction = false) {
        foreach($actions as $action) {
            /**  @var string|ActionRestricted $action */
            $action_ident = $action;
            $action_restriction = $restriction;

            if(is_object($action)) {
                // it is an action object
                $action_ident = $action->getId();
                if($action->getRestriction()) {
                    $action_restriction = $action->getRestriction();
                }
            }

            if($action_restriction) {
                $this->actions->onAction($action_ident)
                              ->restrictRoles($roles, $action_restriction);
            } else {
                $this->actions->onAction($action_ident)
                              ->allowRoles($roles);
            }
        }

        return $this;
    }

    /**
     * @param string[]|int[] $roles
     * @param string         $action
     * @param mixed          $value
     * @param bool           $defaults
     *
     * @return bool
     */
    public function check($roles, $action, $value, $defaults = false) {
        if(!$this->actions->hasAction($action)) {
            return $defaults;
        }

        return $this->actions->onAction($action)->check($roles, $value, $defaults);
    }

    /**
     * @param string[]|int[] $roles
     * @param bool           $rm_namespace
     *
     * @return string[]
     */
    public function get($roles, $rm_namespace = false) {
        $allowed = [];
        foreach($this->actions->getAll() as $action) {
            $check = $action->check($roles);
            if($check) {
                if(is_bool($check)) {
                    if($rm_namespace) {
                        $allowed[$action::rmNamespace($action->id)] = true;
                    } else {
                        $allowed[$action->id] = true;
                    }
                } else {
                    if($rm_namespace) {
                        $allowed[$action::rmNamespace($action->id)] = $check;
                    } else {
                        $allowed[$action->id] = $check;
                    }
                }
            }
        }

        return $allowed;
    }
}