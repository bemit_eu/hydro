<?php

namespace HydroFeature\User\AccessManager\Ability;

class Action {
    /**
     * @var bool[]|callable[] with all granted and restricted roles,
     * ['<role>' => true] for granted and ['<role>' => callable] for restricted,
     * having only one array assures that one role can either bei granted or restricted, where the last executed grant overwrites previously defined
     */
    protected $rules = [];

    /**
     * @var string ident of this action, set during creation, should be `namespace:action-id` like `some.namespace:entry.update`, `some.namespace.access:something` (here the verb has been used in namespace)
     */
    public $id;

    public function __construct($id) {
        $this->id = $id;
    }

    /**
     * @param string[]|int[] $roles
     *
     * @return $this
     */
    public function allowRoles($roles) {
        foreach($roles as $role) {
            $this->rules[$role] = true;
        }
        return $this;
    }

    /**
     * @param string[]|int[] $roles
     * @param bool|callable  $restriction
     *
     * @return $this
     */
    public function restrictRoles($roles, $restriction) {
        foreach($roles as $role) {
            $this->rules[$role] = $restriction;
        }
        return $this;
    }

    /**
     * @param string[]|int[] $roles
     * @param mixed          $value
     * @param bool           $defaults
     *
     * @return bool|mixed
     */
    public function check($roles, $value = null, $defaults = false) {
        // separate checks are needed here, multiple foreach's
        // e.g. the first is not allowed but it is restricted AND restriction values to false, but the second would be allowed, it fails due to returning restriction value from first.

        // first check all roles for allowed
        if($this->isAllowed($roles)) {
            return true;
        }

        // then check all roles for restricted access
        foreach($roles as $role) {
            if(isset($this->rules[$role]) && is_callable($this->rules[$role])) {
                try {
                    $fn_inf = new \ReflectionFunction($this->rules[$role]);

                    if(!empty($value)) {
                        if(count($value) == $fn_inf->getNumberOfParameters()) {
                            // when not empty
                            // when quantity of values is the same as the wanted parameters
                            return call_user_func($this->rules[$role], $value);
                        }

                        error_log('AccessManager\Action wrong parameter quantity on `' . $this->id . '` role `' . $role . '`, needed `' . $fn_inf->getNumberOfParameters() . '` but supplier are `' . count($value) . '`');
                        return false;
                    } else {
                        if(0 == $fn_inf->getNumberOfParameters()) {
                            return call_user_func($this->rules[$role], $value);
                        } else {
                            error_log('AccessManager\Action empty values on `' . $this->id . '` role `' . $role . '` using `false` instead of restriction');
                        }
                        return false;
                    }
                } catch(\ReflectionException $e) {
                    error_log('ReflectionException on `' . $this->id . '` role `' . $role . '` ' . $e->getMessage());
                    return false;
                }
            }
        }

        // when roles found nowhere, return defaults
        return $defaults;
    }

    /**
     * @param string[]|int[] $roles
     *
     * @return bool|mixed
     */
    public function isAllowed($roles) {
        // check all roles for allowed
        foreach($roles as $role) {
            if(isset($this->rules[$role]) && true === $this->rules[$role]) {
                return true;
            }
        }

        return false;
    }

    static public function rmNamespace($id) {
        if(false !== strpos($id, ':')) {
            return explode(':', $id)[1];
        }

        return $id;
    }
}