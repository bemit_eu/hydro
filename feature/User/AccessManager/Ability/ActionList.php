<?php

namespace HydroFeature\User\AccessManager\Ability;

class ActionList {
    /**
     * @var Action[]
     */
    protected $actions = [];

    /**
     * @param $id
     *
     * @return Action
     */
    public function onAction($id) {
        if(!isset($this->actions[$id])) {
            $this->actions[$id] = new Action($id);
        }

        return $this->actions[$id];
    }

    public function hasAction($id) {
        return isset($this->actions[$id]);
    }

    /**
     * @return Action[]
     */
    public function getAll() {
        return $this->actions;
    }
}