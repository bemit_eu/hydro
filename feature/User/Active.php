<?php

namespace HydroFeature\User;

use Flood\Component\Sonar\Annotation\Service as Service;
use Hydro\Console;
use HydroFeature\Container as FeatureContainer;

/**
 * @Service(id="feature-user-active", executed=false)
 * @package HydroFeature\User
 */
class Active {
    protected $loaded = false;

    protected $entity = AccessManager::ENTITY_ANONYM;
    protected $id = false;
    protected $hook_context = false;
    protected $username = false;
    /**
     * @var bool|array
     */
    protected $roles = false;

    protected function load() {
        if($this->loaded) {
            return;
        }

        if(Console::$running) {
            $this->entity = AccessManager::ENTITY_CLI;
            $this->loaded = true;
            return;
        }

        $token = FeatureContainer::_userToken()::extractFromHeader();
        if($token) {
            $payload = FeatureContainer::_userToken()::getPayload($token);

            if(isset($payload['sub'])) {
                $this->id = $payload['sub'];
            }
            if(isset($payload['username'])) {
                $this->username = $payload['username'];
            }
            if(isset($payload['roles'])) {
                $this->roles = $payload['roles'];
            }

            $hook_context = filter_input(INPUT_SERVER, 'HTTP_CONTEXT_HOOK', FILTER_SANITIZE_STRING);
            if($hook_context) {
                $this->hook_context = $hook_context;
            }

            $this->entity = AccessManager::ENTITY_AUTHENTICATED;
            $this->loaded = true;
            return;
        }

        // todo: session auth
    }

    public function getEntity() {
        $this->load();

        return $this->entity;
    }

    public function getId() {
        $this->load();

        return $this->id;
    }

    public function getUsername() {
        $this->load();

        return $this->username;
    }

    public function getRoles() {
        $this->load();

        return $this->roles;
    }

    /**
     * @todo only implemented for JWT auth/HEADER extract, not session/frontend, thus only usable for API restrictions
     * @var bool|string id of the hook currently called,
     * @return bool|string
     */
    public function getHookContext() {
        $this->load();

        return $this->hook_context;
    }
}