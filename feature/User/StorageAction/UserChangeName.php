<?php

namespace HydroFeature\User\StorageAction;

use HydroFeature\User\AccessManager\Action\ActionRestricted;
use HydroFeature\User\AccessManager\Action\BaseAction;

class UserChangeName extends BaseAction implements ActionRestricted {
    public function __construct($restriction = false) {
        parent::__construct($restriction);

        $this->setId(Definition::USER_CHANGE_NAME);
    }
}