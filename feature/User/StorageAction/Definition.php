<?php

namespace HydroFeature\User\StorageAction;

class Definition {
    private const PREFIX = 'feature.user:';
    const USER_CHANGE_NAME = self::PREFIX . 'user-change-name';
    const USER_CHANGE_PASS = self::PREFIX . 'user-change-pass';
    const USER_CHANGE_ROLES = self::PREFIX . 'user-change-roles';
}