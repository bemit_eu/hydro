<?php

namespace HydroFeature\User\Lib;


class Pass {
    /**
     * @var int time in ms the process should wait when a password verification failed, a small but efficient brute-force attack counter-measure
     */
    protected static $verify_wait = 350;

    /**
     * @var int Which password algorithm should be used, PASSWORD_ARGON2I is available from PHP7.2 when compiled with argon2 support
     */
    public static $algo = PASSWORD_BCRYPT;

    /**
     * Makes a secure hash containing all information for further storage
     *
     * @param string $password
     * @param array  $options
     *
     * @return bool|string
     */
    public function hash($password, $options = []) {
        return password_hash($password, static::$algo, $options);
    }

    /**
     * Info contains hashing algorythm and it's information
     *
     * @param $hash
     *
     * @return array
     */
    public function getInfo($hash) {
        return password_get_info($hash);
    }

    /**
     * @param       $hash
     * @param array $options
     *
     * @return bool
     */
    public function needsRehash($hash, $options = []) {
        return password_needs_rehash($hash, static::$algo, $options);
    }

    /**
     * Verifies the given password against the given hash, lets the process wait X ms when failed
     *
     * @param $password
     * @param $hash
     *
     * @return bool
     */
    public function verify($password, $hash) {
        if(true === password_verify($password, $hash)) {

            return true;
        } else {
            usleep(static::$verify_wait);

            return false;
        }
    }
}