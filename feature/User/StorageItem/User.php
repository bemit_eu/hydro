<?php

namespace HydroFeature\User\StorageItem;

use HydroFeature\Container as FeatureContainer;
use HydroFeature\User\AccessManager;
use HydroFeature\User\Lib\Pass;
use HydroFeature\User\StorageAction;

class User {
    protected $id;
    protected $name;
    protected $pass;
    protected $roles;
    protected $active;

    protected $storage;

    /**
     * User constructor.
     *
     * @param $data
     *
     * @throws \Exception
     */
    public function __construct($data) {
        if(!isset($data['ID'])) {
            throw new \Exception('Tried to create user without `ID`');
        }

        if(!isset($data['name'])) {
            throw new \Exception('Tried to create user without `name`');
        }

        $this->id = $data['ID'];
        $this->name = $data['name'];
        if(isset($data['pass'])) {
            $this->pass = $data['pass'];
        }
        if(isset($data['roles'])) {
            // todo: find better json conversion/support, should be decoded from DB without explicitly telling the column name in storagehandler
            if(is_string($data['roles'])) {
                $data['roles'] = json_decode($data['roles'], true);
            }

            if(is_array($data['roles'])) {
                $this->roles = $data['roles'];
            }
        }
        if(isset($data['active'])) {
            $this->active = $data['active'];
        }

        $this->storage = new \HydroFeature\User\StorageHandler\UserBase();
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    /**
     * @param $name
     *
     * @throws \HydroFeature\User\AccessManager\Exception\NotAllowed
     */
    public function setName($name) {
        if($name !== $this->name) {
            if(
            !FeatureContainer::_accessManager()
                             ->isAllowed(
                                 'feature.user',
                                 StorageAction\Definition::USER_CHANGE_NAME,
                                 $this->getId(),
                                 false
                             )
            ) {
                throw new AccessManager\Exception\NotAllowed('Action: ' . StorageAction\Definition::USER_CHANGE_NAME . ' is NotAllowed');
            }
            $this->name = $name;
        }
    }

    public function getPass() {
        return $this->pass;
    }

    public function setPass($pass) {
        $passwd = new Pass();
        $this->pass = $passwd->hash($pass);
    }

    public function getRoles() {
        return $this->roles;
    }

    public function setRoles($roles) {
        $this->roles = $roles;
    }

    public function getActive() {
        return $this->active;
    }

    public function setActive($active) {
        $this->active = $active;
    }

    public function save() {
        $updated = $this->storage->update($this->getId(), [
            'name' => $this->getName(),
            'pass' => $this->getPass(),
            'roles' => json_encode($this->getRoles()),
            'active' => $this->getActive(),
        ]);

        if(is_int($updated)) {

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     * @throws \HydroFeature\User\AccessManager\Exception\NotAllowed
     */
    public function delete() {
        if(
        !FeatureContainer::_accessManager()
                         ->isAllowed(
                             'feature.user',
                             AccessManager::ACTION_DELETE,
                             $this->getId(),
                             false
                         )
        ) {
            throw new AccessManager\Exception\NotAllowed('Action: User Delete is NotAllowed');
        }

        $deleted = $this->storage->delete($this->getId());

        if(is_int($deleted)) {

            return true;
        } else {
            return false;
        }
    }

    /**
     * @deprecated only for api
     */
    public function export() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'password' => $this->pass ? true : false,
            'roles' => $this->roles,
            'active' => $this->active,
        ];
    }
}