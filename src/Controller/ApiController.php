<?php

namespace Hydro\Controller;

class ApiController extends BaseController {

    protected $resp_data = [];

    public function __construct() {
        parent::__construct();
    }

    public function setResponse($response = []) {
        $this->resp_data = $response;
    }

    public function respondJson() {
        $this->addHeader('Content-Type: application/json');
        $this->resp_data = json_encode($this->resp_data);
        $this->respond();
    }

    public function respond() {
        parent::respondEmpty();

        echo $this->resp_data;
    }
}