<?php

namespace Hydro\Controller;

use Flood\Component\Route\Container as RouteContainer;

class BaseController {

    /**
     * @var array
     */
    protected $header = [];

    public function __construct() {
    }

    public function sendHeader() {
        foreach($this->header as $h) {
            header($h, true);
        }
    }

    public function addHeader($header, $as_array = false) {
        if($as_array) {
            $this->header = array_merge($this->header, $header);
        } else {
            $this->header[] = $header;
        }
    }

    /**
     * @param int $code
     */
    public function addStatusHeader($code) {
        $this->addHeader(RouteContainer::_url()::getStatusHeader($code));
    }

    public function respondEmpty() {
        $this->sendHeader();
    }

    /**
     * @deprecated should use respondEmpty, but some extending classes currently use parent::respond()
     */
    public function respond() {
        $this->respondEmpty();
    }
}