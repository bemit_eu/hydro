<?php

namespace Hydro\Controller;

use Exception;
use Flood\Component\Route\Hook\ActiveResponse;
use Hydro\Container;
use HydroFeature\Container as FeatureContainer;
use HydroFeature\Template\TemplateData;
use Hydro\Hook\UrlGenerator;

class ResponseController extends BaseController {
    use TemplateData;

    /**
     * @var string the tag for the default cdn folder
     */
    protected $route_cdn = '';
    /**
     * @var string the tag for the home route
     */
    protected $route_home = '';

    /**
     * @var string
     */
    public $template_namespace = false;

    /**
     * @var \Flood\Component\Route\Hook\ActiveResponse hold the information about the current active response
     */
    protected $active_response;

    protected $article = false;
    protected $article_data = false;
    protected $content_model = false;

    /**
     * @var \Hydro\Hook\UrlGenerator
     */
    protected $url_generator;

    /**
     * ResponseController constructor.
     *
     * @param array $payload
     *
     * @throws \Exception
     */
    public function __construct($payload = []) {
        parent::__construct();

        // Parses the route id to fetch the current response info
        $this->active_response = new ActiveResponse(Container::_route()->match['_route']);

        //Container::_i18n()->setActive($this->active_response->locale);

        // todo: refac meta setting/schema extraction
        $meta = [];

        if(isset($payload['article'])) {
            $article = FeatureContainer::_content()->getArticleByTag($payload['article'], $this->active_response->hook);
            if(!$article) {
                throw new Exception('ResponseController ArticleNotFoundButSet');
            }

            $this->article = $article;
            $article_data = $this->article->getData($this->active_response->locale);
            if(!$article_data) {
                throw new Exception('ResponseController ArticleFoundHasNoData');
            }
            $this->article_data = $article_data;

            $this->content_model = FeatureContainer::_content()->buildContentModel($this->article_data, $this->article);

            if($this->article_data->getMeta()) {
                $meta = $this->article_data->getMeta();
            }

            $this->assignTemplateData('content_model', $this->content_model);
        }

        $this->assignTemplateData('config', Container::_config());
        $this->assignTemplateData('article', $this->article);
        $this->assignTemplateData('article_data', $this->article_data);
        $this->assignTemplateData('active_response', $this->active_response);
        $this->assignTemplateData('match', Container::_route()->match);

        // todo refac meta extraction
        // this meta has to do with: default canal/view and automatic blocks like "Title"
        $meta['lang'] = $this->active_response->locale;
        FeatureContainer::_template()->assignTemplateData('meta', $meta);
        if(isset($meta['head'])) {
            FeatureContainer::_template()->assignTemplateData('head', $meta['head']);
        }

        $this->url_generator = new UrlGenerator($this->active_response);
        $this->url_generator->asset = $this->url_generator->generateAssetFolder($this->route_cdn);
        $this->url_generator->home = $this->url_generator->generate($this->route_home);
        $this->url_generator->active = $this->url_generator->generateRoute(Container::_route()->match['_route']);

        FeatureContainer::_template()->assignTemplateData('url', $this->url_generator);

        FeatureContainer::_template()->addFunction('renderBlock', function($name) {
            if(!$this->content_model) {
                return '';
            }
            $blocks = $this->content_model->renderBlock($name);
            $html = '';
            if(!is_array($blocks)) {
                $blocks = [$blocks];
            }
            foreach($blocks as $block) {
                $html .= $block;
            }
            return $html;
        });
    }

    /**
     * @param string $template relative path to template file, from theme.path-view on
     * @param array $data
     *
     * @return string
     */
    public function render($template = '', $data = []): string {
        if(!empty($this->template_namespace)) {
            $this->assignTemplateData('namespace', '@' . $this->template_namespace . '/');
        } else {
            $this->assignTemplateData('namespace', '');
        }

        return FeatureContainer::_template()->render($this->tplPrefixNamespace($template), array_merge($this->template_data, $data));
    }

    /**
     * Prefixes the template path
     *
     * @param string $template
     *
     * @return string
     */
    protected function tplPrefixNamespace($template) {
        if(!empty($this->template_namespace)) {
            return '@' . $this->template_namespace . '/' . $template;
        }

        return $template;
    }

    /**
     * Parent sends header
     *
     * @param string $template
     *
     * @return string
     */
    public function respond($template = '') {
        $this->respondEmpty();

        return $this->render($template);
    }
}
