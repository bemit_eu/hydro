<?php

namespace Hydro\Cache;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * Class Cache
 *
 * @Service(id="cache", executed=false)
 * @package Hydro\Cache
 */
class Cache extends \Flood\Component\Cache\Cache {
    /**
     * @return self
     */
    public static function i() {
        return parent::i();
    }
}