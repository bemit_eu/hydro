<?php

namespace Hydro;

use Flood\Component\Config as FloodConfig;
use Flood\Component\Json\Json;

/**
 * Short description for class
 *
 * Long description for class
 *
 * @category
 * @package
 * @author     Original Author mb.project@bemit.eu
 * @link
 * @copyright  2017
 * @since      Version
 * @version    Release: @package_version@
 */
class Config extends FloodConfig\Storage {

    public function __construct() {
    }

    static public function setEnv($folder) {
        if(is_file($folder . '/.env.' . Hydro::$env)) {
            parent::setEnv($folder . '/.env.' . Hydro::$env);
        } else if(is_file($folder . '/.env')) {
            parent::setEnv($folder . '/.env');
        }
    }

    public function readFromMeta($file, $folder_root) {
        $meta = Json::decodeFile($folder_root . '/' . $file, true);
        try {
            if(is_array($meta)) {
                foreach($meta as $config_item) {
                    if(Hydro::$env !== 'production' && isset($config_item['file-dev'])) {
                        $config_item['file'] = $config_item['file-dev'];
                    }
                    $config_data = Json::decodeFile($folder_root . '/' . $config_item['file'], true);
                    if(Json::lastError() && !is_array($config_data) && Hydro::$debug) {
                        throw new FloodConfig\Exception\FileNotValid('group file not valid: ' . $config_item['file']);
                    } else {
                        $this->setConfigGroup($config_item['name'], $config_data);
                    }
                }
            } else {
                throw new FloodConfig\Exception\FileNotValid('meta file not valid: ' . $folder_root . '/' . $file);
            }
        } catch(FloodConfig\Exception\FileNotValid $e) {
            $msg = 'FileNotValid: Hydro.Config: ' . $e->getMessage();
            if(Hydro::$debug) {
                echo $msg . "\r\n";
            } else {
                error_log($msg);
            }
            exit(1);
        }
    }

    public function setConfigGroup($name, $data) {
        try {
            if(is_array($data)) {
                parent::setConfigGroup($name, $data);
            } else {
                throw new \Exception('data is not an array: ' . $name);
            }
        } catch(\Exception $e) {
            $msg = 'Exception:Hydro.Config: ';
            if(Hydro::$debug) {
                echo $msg . "\r\n";
            } else {
                error_log($msg);
            }
        }
    }

    /**
     * Returns the base server path of the Hydro installation
     *
     * @param bool $trailing_slash if a trailing slash should be appended
     *
     * @return string
     */
    public function serverPath($trailing_slash = false) {
        return (defined('HYDRO_WD') ? HYDRO_WD : Container::_filesystemPath()->slashTrailing(dirname(__DIR__, 3), $trailing_slash));
    }

    /**
     * @return \HydroFeature\Template\Config
     */
    public function template() {
        return Container::i()->get('config-template');
    }

    /**
     * @return Session\Config
     */
    public function session() {
        return Container::i()->get('config-session');
    }
}