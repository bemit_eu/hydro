<?php

namespace Hydro;

/**
 * Flood\Component\Container
 *
 * @category
 * @package    Hydro
 * @author     Michael Becker - michael@bemit.codes
 * @link       https://painttheweb.de/flood-component/container
 * @copyright  2017-2018 bemit UG (haftungsbeschraenkt)
 * @since      Version 0.0.1
 * @version    0.0.1
 */
class Container extends \Flood\Component\Container\Container {

    protected static $i = null;

    protected function __construct() {
        parent::__construct();
        $this->container_id = 'HydroContainer';
    }

    /**
     * Returns an instance of this class
     *
     * @return self
     */
    public static function i() {
        return parent::i();
    }

    /**
     * convenience method for `get('hydro')`
     *
     * @return Hydro
     */
    public static function _hydro() {
        return Container::i()->get('hydro');
    }

    /**
     * convenience method for `get('sonar')`
     *
     * @return \Flood\Component\Sonar
     */
    public static function _sonar() {
        return Container::i()->get('sonar');
    }

    /**
     * convenience method for `get('log')`
     *
     * @return Log\Logger
     */
    public static function _log() {
        return Container::i()->get('log');
    }

    /**
     * convenience method for `get('hydro-storage')`
     *
     * @return Hook\Storage
     */
    public static function _hookStorage() {
        return Container::i()->get('hook-storage');
    }

    /**
     * convenience method for `get('i18n')`
     *
     * @return \Flood\Component\I18n\LocaleStorage
     */
    public static function _i18n() {
        return Container::i()->get('i18n');
    }

    /**
     * convenience method for `get('flood-route')`
     *
     * @return \Flood\Component\Route\Route
     */
    public static function _route() {
        return Container::i()->get('flood-route');
    }

    /**
     * convenience method for `get('filesystem-path')`
     *
     * @return Filesystem\Path
     */
    public static function _filesystemPath() {
        return Container::i()->get('filesystem-path');
    }

    /**
     * convenience method for `get('config')`
     *
     * @return Config
     */
    public static function _config() {
        return Container::i()->get('config');
    }

    /**
     * convenience method for `get('cdn')`
     *
     * @return \Flood\Component\Cdn\CDN
     */
    public static function _cdn() {
        return Container::i()->get('cdn');
    }

    /**
     * convenience method for `get('cache')`
     *
     * @return Cache\Cache
     */
    public static function _cache() {
        return Container::i()->get('cache');
    }

    /**
     * convenience method for `get('session')`
     *
     * @return Session\Session
     */
    public static function _session() {
        return Container::i()->get('session');
    }

    /**
     * convenience method for `get('storage-data')`
     *
     * @return \Flood\Component\Storage\Storage
     */
    public static function _storageData() {
        return Container::i()->get('storage-data');
    }
}