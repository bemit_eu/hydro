<?php

namespace Hydro\Session;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * Short description for class
 *
 * Long description for class
 *
 * @Service(id="config-session", executed=false)
 * @category
 * @package
 * @author     Original Author mb.project@bemit.eu
 * @link
 * @copyright  2017
 * @since      Version
 * @version    Release: @package_version@
 */
class Config extends \Flood\Component\Config\Config {

    /**
     * @return string|null
     */
    public function name() {
        return $this->getData(['session', 'name']);
    }

    /**
     * @return string|null
     */
    public function engine() {
        return $this->getData(['session', 'engine']);
    }

    /**
     * @return string|null
     */
    public function active() {
        return $this->getData(['session', 'active']);
    }
}