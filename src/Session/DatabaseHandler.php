<?php

namespace Hydro\Session;

/**
 * Class DatabaseHandler
 *
 * @package Hydro\Session
 * @todo: just added from old scribbles, not tested/working, will be needed when running hydro load balanced
 */
class DatabaseHandler {

    protected $life_time;

    /**
     * @var string encryption secret key, but encryption is more some obfuscation
     */
    protected $secret = "wF2C4W59omFzJqpEKJcQJaNS6tcZyPTPM0cgbJUNtF2BFMV300cuG6JwMqFV621";

    /**
     * @var null todo: add the needed db connection
     */
    protected $db_connection;

    protected $db_cols;
    protected $db_table;

    public function __construct() {
        $this->db_table = 'session';
        $this->db_cols = ['id', 'expire', 'data'];
        $this->db_connection = null;
    }
    public function __destruct() {
        session_write_close();
    }
    public function open($s_savePath, $s_Name) {
        $return_val = true;
        $this->life_time = ini_get("session.gc_maxlifetime");
        return $return_val;
    }
    public function close() {
        $this->gc(ini_get('session.gc_maxlifetime'));
    }
    public function read($s_ID) {
        $return_val = '';
        $datas = $this->db_connection->select($this->db_table, ['session_data'], [
            'AND' => [
                'session_id' => $s_ID,
                'session_expires[>]' => time(),
            ]
        ]);
        if($this->db_connection->querySuccess()) {
            $return_val = $this->decryptData($datas[0]['session_data']);
        }
        return $return_val;
    }
    public function write($s_ID,$s_Data)  {
        $return_val = false;
        $newExpireTimestamp = time() + $this->life_time;
        $qty = $this->db_connection->count($this->db_table, [
            'session_id' => $s_ID,
        ]);
        if(0 < $qty) {
            $this->db_connection->update($this->db_table, [
                'session_expires' => $newExpireTimestamp,
                'session_data' => $this->encryptData($s_Data),
            ], [
                'session_id' => $s_ID,
            ]);
            if($this->db_connection->querySuccess()) {
                $return_val = true;
            }
        } else {
            $this->db_connection->insert($this->db_table, [
                'session_id' => $s_ID,
                'session_expires' => $newExpireTimestamp,
                'session_data' => $this->encryptData($s_Data),
            ]);
            if($this->db_connection->querySuccess()) {
                $return_val = true;
            }
        }
        return $return_val;
    }
    public function destroy($s_ID) {
        $return_val = false;
        $this->db_connection->delete($this->db_table, [
            'session_id' => $s_ID,
        ]);
        if($this->db_connection->querySuccess()){
            $return_val = true;
        }
        return $return_val;
    }
    public function gc($s_MaxLifeTime) {
        //Clean old sessions
        $this->db_connection->delete($this->db_table, [
            'session_expires[<]' => time(),
        ]);
        return $this->db_connection->querySuccess();
    }
    private function encryptData($pure_string) {
        //einfache Verschlüsselung des Data Feldes mit base64 und anhängen des Verschlüsselungskeys
        $encrypted_string = base64_encode($pure_string.$this->secret);
        $strcryptlen = strlen($encrypted_string);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.1))."[".substr($encrypted_string, $strcryptlen/(1.1),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.15))."]".substr($encrypted_string, $strcryptlen/(1.15),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.2))."}".substr($encrypted_string, $strcryptlen/(1.2),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.3))."&".substr($encrypted_string, $strcryptlen/(1.3),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.36))."$".substr($encrypted_string, $strcryptlen/(1.36),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.4))."=".substr($encrypted_string, $strcryptlen/(1.4),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.5))."?".substr($encrypted_string, $strcryptlen/(1.5),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.58))."§".substr($encrypted_string, $strcryptlen/(1.58),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.6))."!".substr($encrypted_string, $strcryptlen/(1.6),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.7))."?".substr($encrypted_string, $strcryptlen/(1.7),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.74))."{[".substr($encrypted_string, $strcryptlen/(1.74),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.8))."]}".substr($encrypted_string, $strcryptlen/(1.8),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.9))."(".substr($encrypted_string, $strcryptlen/(1.9),$strcryptlen);
        $encrypted_string = substr($encrypted_string, 0, $strcryptlen/(1.92)).")".substr($encrypted_string, $strcryptlen/(1.92),$strcryptlen);
        return $encrypted_string;
    }
    private function decryptData($encrypted_string) {
        // einfache Entschlüsselung des Data Feldes, bae64 Rückgängig und entfernen des Verschlüsselungskeys
        $encchars = ['[',']','&','$','=','?','§','!','?','{','}','(',')'];
        $encrypted_string = str_replace($encchars,'',$encrypted_string);
        $encrypted_string = base64_decode($encrypted_string);
        $keylen = strlen($this->secret);
        $strcryptlen = strlen($encrypted_string);
        $decrypted_string = substr($encrypted_string, 0, $strcryptlen-$keylen);
        return $decrypted_string;
    }
}