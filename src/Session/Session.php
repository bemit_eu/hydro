<?php

namespace Hydro\Session;

use Hydro\Container;
use Flood\Component\Sonar\Annotation\Service as Service;

/**
 *
 * @Service(id="session", executed=false)
 * @package Hydro\Session
 */
class Session {

    public function __construct() {
    }

    public function init() {
        if(Container::_config()->session()->active()) {
            switch(Container::_config()->session()->engine()) {
                case 'database':
                    $this->runDbHandler();
                    break;
                case 'file':
                default:
                    break;
            }
            session_name(Container::_config()->session()->name());
            session_start();
        }
    }

    protected function runDbHandler() {
        $handler = new DatabaseHandler();
        session_set_save_handler([$handler, 'open'],
            [$handler, 'close'],
            [$handler, 'read'],
            [$handler, 'write'],
            [$handler, 'destroy'],
            [$handler, 'gc']);
    }
}