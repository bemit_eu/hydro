<?php

namespace Hydro;

use Symfony\Component\Console\Application;

function writeln($msg) {
    echo $msg . "\r\n";
}

class Console extends Application {
    static $running = false;

    public function __construct(string $name = 'Hydro', string $version = 'UNKNOWN') {
        parent::__construct($name, $version);
        static::$running = true;
    }
}