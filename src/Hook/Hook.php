<?php

namespace Hydro\Hook;

use Hydro\Container;
use HydroFeature\Container as FeatureContainer;

class Hook extends \Flood\Component\Route\Hook\Hook {

    protected $api_config = false;

    /**
     * Method stub for easy extension
     *
     * @param $data
     */
    public function parseData($data) {
        if(isset($data['route'])) {
            $this->parseDataRoute($data['route']);
        }

        if(isset($data['template'])) {
            $this->parseDataTemplate($data['template']);
        }

        if(isset($data['api'])) {
            $this->parseDataApi($data['api']);
        }
    }

    protected function parseDataTemplate($data) {
        if(isset($data['path-view'])) {
            if(is_array($data['path-view'])) {
                FeatureContainer::_template()->addPathArray($data['path-view']);
            } else {
                error_log('Hook: path-view set but not valid for ' . $this->id);
            }
        }
    }

    protected function parseDataApi($data) {
        $this->api_config = $data;
    }

    public function getApi() {
        return $this->api_config;
    }

    protected function parseDataFolder($data) {
        //var_dump($data);
        // todo
    }

    protected function parseDataFile($data) {
        // todo
    }
}