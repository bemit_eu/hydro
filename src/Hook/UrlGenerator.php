<?php

namespace Hydro\Hook;

use Hydro\Container;
use Flood\Component\Route\Container as RouteContainer;

/**
 * Url Generator Helper Class for any Response, e.g. using ResponseController from a Hook
 *
 * @package Hydro\Url
 */
class UrlGenerator {
    public $asset;
    public $home;
    public $active;
    /**
     * @var \Flood\Component\Route\Hook\ActiveResponse
     */
    private $active_response;

    /**
     * UrlGenerator constructor.
     *
     * @param \Flood\Component\Route\Hook\ActiveResponse $active_response
     */
    public function __construct($active_response) {
        $this->active_response = $active_response;
    }

    public function generate($id, $param = []) {
        return $this->generateActiveHook($this->active_response->locale, $id, $param);
    }

    public function generateActiveHook($locale, $id, $param = []) {
        return $this->generateCrossHook($this->active_response->hook, $locale, $id, $param);
    }

    public function generateCrossHook($hook, $locale, $id, $param = []) {
        return
            $this->generateRoute(
                RouteContainer::_hookRouteGenerator()::createRouteId(
                    $hook,
                    $locale,
                    $id
                ),
                $param
            );
    }

    public function generateRoute($route_id, $param = []) {
        try {
            return Container::_route()->url_generator
                ->generate(
                    $route_id,
                    $param,
                    \Symfony\Component\Routing\Generator\UrlGenerator::ABSOLUTE_URL
                );
        } catch(\Exception $e) {
            error_log('Hook\UrlGenerator for route_id `' . $route_id . '` failed: ' . $e->getMessage());

            return '';
        }
    }

    public function generateAssetFolder($folder_tag, $hook = false) {
        if(
        !($storage_object = RouteContainer::_hookStorage()
                                          ->get($hook ? $hook : $this->active_response->hook)
                                          ->getStorageObject('folder'))
        ) {
            return '';
        }
        /**
         * @var \Flood\Component\Cdn\Route\Folder $storage_object
         */
        $cdn_ident = $storage_object->getByTag($folder_tag);
        if(!$cdn_ident) {
            return '';
        }
        return Container::_route()->url_generator->generate($cdn_ident, ['any' => ''], \Symfony\Component\Routing\Generator\UrlGenerator::ABSOLUTE_URL) . '/';
    }
}