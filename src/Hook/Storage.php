<?php

namespace Hydro\Hook;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 *
 * @Service(id="hook-storage", executed=false)
 * @package Hydro\Hook
 */
class Storage extends \Flood\Component\Route\Hook\Storage {

}