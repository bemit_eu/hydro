<?php

namespace Hydro\Filesystem\File;


class Read {
    public static function getContents($filename, $use_include_path = false, $context = null, $offset = 0) {
        return file_get_contents($filename, $use_include_path, $context, $offset);
    }
}