<?php

namespace Hydro\Filesystem;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * This class is the lexer and parser for path strings, making them more dynamic
 *
 * @Service(id="filesystem-path", executed=false)
 * @category
 * @package    Hydro\Filesystem
 * @author     Michael Becker mb.project@bemit.eu
 * @link
 * @copyright  2017 Michael Becker
 * @version    0.1.0
 */
class Path {

    public function __construct() {
    }

    /**
     * Checks if given path has a slash at the beginning or true
     *
     * @param string $path
     *
     * @return bool false when nowhere a slash, true when at begin or end
     */
    public function hasSlash($path) {
        $return_val = false;
        if($this->hasSlashBeginning($path) || $this->hasSlashTrailing($path)) {
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * Checks if given path has a slash at the beginning or true
     *
     * @param string $path
     *
     * @return bool false when nowhere a slash, true when at begin or end
     */
    public function hasSlashBeginning($path) {
        $return_val = false;
        $path = trim($path);
        if('/' === substr($path, 0, 1)) {
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * Checks if given path has a slash at the beginning or true
     *
     * @param string $path
     *
     * @return bool false when nowhere a slash, true when at begin or end
     */
    public function hasSlashTrailing($path) {
        $return_val = false;
        $path = trim($path);
        if('/' === substr($path, strlen($path) - 1)) {
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * Strips beginning and end slashes from a string, trims string before removing /
     *
     * @param $string
     *
     * @return string trimmed and / and \ removed from begin and end
     */
    public function stripSlash($string) {
        return $this->stripSlashBeginning($this->stripSlashTrailing($string));
    }

    /**
     * Strips beginning slash from a string, trims string before removing /
     *
     * @param $string
     *
     * @return string trimmed and / and \ removed from begin
     */
    public function stripSlashBeginning($string) {
        $string = trim($string);
        if('\\' === substr($string, 0, 1) || '/' === substr($string, 0, 1)) {
            $string = substr($string, 1);
        }

        return $string;
    }

    /**
     * Strips end slash from a string, trims string before removing /
     *
     * @param $string
     *
     * @return string trimmed and / and \ removed from end
     */
    public function stripSlashTrailing($string) {
        $string = trim($string);
        if('\\' === substr($string, strlen($string) - 1) || '/' === substr($string, strlen($string) - 1)) {
            $string = substr($string, 0, strlen($string) - 1);
        }

        return $string;
    }

    /**
     * Adds a trailing slash to the end of the given path (string) or removes it when not wanted
     *
     * @param string $path
     * @param bool   $trailing_slash true adds a / when not already at the end, false removes a trailing slash if in $uri
     *
     * @return string $uri with slash at end, or not
     */
    public function slashTrailing($path, $trailing_slash = true) {
        $return_val = $path;
        if(true === $trailing_slash) {
            if(!$this->hasSlashTrailing($path)) {
                // When the last char in the uri is not a / but wanted, add it
                $return_val .= '/';
            }
        } else if(false === $trailing_slash) {
            if($this->hasSlashTrailing($path)) {
                // When the last char in the uri is a / but not wanted, removes it
                $return_val = $this->stripSlashTrailing($path);
            }
        }

        return $return_val;
    }

    /**
     * @param string $path  the path to the file
     * @param int    $level the max level of fileendings that should be returned
     *
     * @return array|bool|string
     */
    public function fileEnding($path, $level = 3) {

        if(false !== strpos(strrev($path), DIRECTORY_SEPARATOR)) {
            // cut the last before a / part
            $path = strrev(substr(strrev($path), 0, strpos(strrev($path), DIRECTORY_SEPARATOR)));
        }
        if(false !== strpos($path, '.')) {
            // let asume that the first . dot is the begin from fileending, so dots in names are treated already, needed for dynamic level of fileending
            $path = substr($path, strpos($path, '.') + 1);
        }

        // get the last (max default: three) levels of the file ending
        $path = explode('.', strrev($path), $level);
        // re glue them together with . dots
        $path = strrev(implode('.', $path));

        return $path;
    }

    /**
     * @param string       $path_end
     * @param string|array $file_ending without beginning dot, only name, could be more precise, more then three parted ending are risky though,
     *                                  array is array of endings not ending as array
     *
     * @return bool
     */
    public function fileEndingCompare($path_end, $file_ending = []) {
        $return_val = false;

        if(is_string($file_ending)) {
            $file_ending[] = $file_ending;
        }

        foreach($file_ending as $fil_end) {
            if(0 === strpos($path_end, $fil_end)) {
                $return_val = true;
            }
        }

        return $return_val;
    }
}