<?php

namespace Hydro;

use Flood\Captn;
use Flood\Component\Cdn as FloodCdn;
use Flood\Component\I18n\LocaleStorage;
use Flood\Component\Route as FloodRoute;
use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * Short description for class
 *
 * Long description for class
 *
 * @Service(id="hydro", executed=false)
 * @category
 * @package
 * @author     Original Author mb.project@bemit.eu
 * @link
 * @copyright  2017 - 2018
 * @since      Version 0.1.0
 * @version    Release: @package_version@
 */
class Hydro {
    /**
     * @var bool|int when false it does nothing, with true it logs with `2` it will display the error and does not log; at console run: is the same value as Console::$debug
     */
    public static $debug = false;

    /**
     * @var string `development` or `production`, everything not `production` is considered dev by default
     *             sets e.g. which config files will be tried to read
     */
    public static $env = 'development';

    public function __construct() {

    }

    public function __destruct() {
        //Container::i()->destroy();
    }

    /**
     * @todo refactor and remove
     */
    public function prepareComponent() {
        FloodRoute\Container::i()->overwrite('hook-storage', Container::_hookStorage());
        FloodRoute\Container::i()->bind('hook-storage', Container::i()->bind('hook-storage'));
        FloodRoute\Container::i()->overwrite('hook', static function() {
            return new Hook\Hook();
        });

        Container::i()->register('flood-route', FloodRoute\Route::i());

        $config = Container::_config()->getData(['cache', 'route']);
        if(isset($config['path'])) {
            $config['path'] = HYDRO_WD . $config['path'];
        }
        FloodRoute\Container::i()->_cache()->init($config);

        /*
         * The following event handlers are for the CDN Component
         */
        FloodRoute\Container::i()->register('route-storage-object-folder', static function() {
            // storage object registration
            return new FloodCdn\Route\Folder();
        });

        FloodRoute\Container::i()->register('route-storage-object-file', static function() {
            // storage object registration
            return new FloodCdn\Route\File();
        });

        FloodRoute\Container::_hookRouteGenerator()->registerEventHandler('hook-list', static function($hook_id) {
            // route generator for CDN Folder
            $storage = FloodRoute\Container::_hookStorage()->get($hook_id)->getStorageObject('folder');
            if($storage) {
                return $storage->generateByHook($hook_id);
            }

            return false;
        });

        FloodRoute\Container::_hookRouteGenerator()->registerEventHandler('hook-list', static function($hook_id) {
            // route generator for CDN File
            $storage = FloodRoute\Container::_hookStorage()->get($hook_id)->getStorageObject('file');
            if($storage) {
                return $storage->generateByHook($hook_id);
            }

            return false;
        });
    }

    public function prepareHook() {
        Captn\Acknowledge::onEachTyped(static function($value, $type, $id) {
            if(isset($value['hook_file'])) {
                if(isset($value['hook_file']['json'])) {
                    Container::_hookStorage()->addHook($id, $value['hook_file']['json']);
                }

                if(isset($value['hook_file']['register'])) {
                    try {
                        /**
                         * @var \Flood\Component\Route\Hook\HookFileI $hook
                         */
                        $hook = new $value['hook_file']['register']();
                        if($hook instanceof \Flood\Component\Route\Hook\HookFileI) {
                            $hook->setId($id);
                        } else {
                            throw new \Exception('not implementing interface');
                        }
                    } catch(\Exception $e) {
                        error_log('Hook `' . $id . '` register failed: ' . $e->getMessage());
                        throw new \Exception('Hook `' . $id . '` register failed: ' . $e->getMessage());
                    }
                }
            }
        },
            Captn::TYPE__HOOK);
    }

    public function route() {
        Container::_route()->createContext();
        Container::_route()->generateRoute();
        Captn\EventDispatcher::trigger('hydro.exec.addRoutes', Container::_route());
    }

    public function dispatch() {
        Container::_session()->init();

        Container::_route()->match();

        return Container::_route()->dispatch();
    }

    public function mangle($response) {
        $mangled_response = $response;

        // todo: implementing mangle event

        return $mangled_response;
    }

    public function display($response) {
        echo $response;
    }
}