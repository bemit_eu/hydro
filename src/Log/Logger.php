<?php

namespace Hydro\Log;

use Flood\Component\Sonar\Annotation\Service as Service;

/**
 * Class Logger
 *
 * Currently only exception is implemented basically and the log signature is existing
 *
 * @package Hydro\Log
 *
 * @Service(id="log", executed=false)
 */
class Logger {

    public function __construct() {

    }

    /**
     * Will handle the exception message, displaying or logging or nothing
     *
     * @param \Exception $exception
     * @param bool|int   $debug_state
     */
    public function exception($exception, $debug_state = false) {
        if (false !== $debug_state) {
            if (true === $debug_state) {
                error_log($exception->getMessage());
            } else {
                if (2 === $debug_state) {
                    echo $exception->getMessage() . "\r\n";
                }
            }
        }
    }

    /**
     * empty signature for log method
     *
     * @param      $msg
     * @param      $type
     * @param bool $display
     */
    public function log($msg, $type, $display = false) {

    }
}