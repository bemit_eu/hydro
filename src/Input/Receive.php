<?php

namespace Hydro\Input;

class Receive {

    public const TYPE_ARRAY = 'array';

    protected static $input_data = false;
    protected static $post_data = false;

    public function __construct() {

        if(false === static::$input_data) {
            $input = file_get_contents('php://input');

            if($input) {
                // exists
                static::$input_data = json_decode($input, true);
            }
        }

        if(false === static::$post_data) {
            static::$post_data = true;
        }
    }

    /**
     * Use only for debugging, no included filter
     *
     * @return mixed|array
     */
    public function all() {

        if(static::$input_data) {
            return static::$input_data;
        } else if(static::$post_data) {
            return $_POST;
        }

        return [];
    }

    /**
     * @param     $key
     * @param int $filter
     * @param int $options
     *
     * @return mixed|null
     */
    public function get($key, $filter = FILTER_DEFAULT, $options = null) {
        $return_val = null;

        if(static::$input_data && isset(static::$input_data[$key])) {
            if($filter === FILTER_REQUIRE_ARRAY) {
                // when type ARRAY is requested, element wise sanitizing must be done from consumer
                return static::$input_data[$key];
            }
            // filter one element
            $return_val = filter_var(static::$input_data[$key], $filter);
        } else if(static::$post_data && filter_has_var(INPUT_POST, $key)) {
            if($filter === FILTER_REQUIRE_ARRAY) {
                $return_val = filter_input(INPUT_POST, $key, FILTER_UNSAFE_RAW, FILTER_REQUIRE_ARRAY);
            } else if($options) {
                $return_val = filter_input(INPUT_POST, $key, $filter, $options);
            } else {
                $return_val = filter_input(INPUT_POST, $key, $filter);
            }
        }

        return $return_val;
    }

    public function has($key) {
        if(static::$input_data && isset(static::$input_data[$key])) {
            return true;
        } else if(static::$post_data && filter_has_var(INPUT_POST, $key)) {
            return true;
        }

        return false;
    }
}
