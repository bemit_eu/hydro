<?php

use Hydro\Container;
use Flood\Captn\EventDispatcher;

return static function(
    /**
     * @var array $option = [
     *         'performance' => [
     *             'time' => microtime(true), // Set time value for performance monitor
     *             'memory' => memory_get_usage(), // Set memory value for performance monitor
     *             'log' => false,
     *         ],
     *     ]
     */ $option
) {
    $error = false;

    try {
        EventDispatcher::trigger('hydro.flow.registerPerformanceMonitor', [
            'container' => Container::class,
        ]);

        if(Container::i()->has('performance-monitor')) {
            /**
             * @var Flood\Component\PerformanceMonitor\Monitor $perf_monitor
             */
            $perf_monitor = Container::i()->get('performance-monitor');
            $perf_monitor->setResource();
            $perf_monitor->startProfile('hydro-complete', [
                'time' => $option['performance']['time'],
                'memory' => $option['performance']['memory'],
            ]);
            $perf_monitor->startProfile('hydro-logic-complete');
            $perf_monitor->startProfile('hydro-respond', [
                'time' => $option['performance']['time'],
                'memory' => $option['performance']['memory'],
            ]);
        } else {
            throw new \Exception(
                'Check PerformanceMonitor registration to Container, none found. Analyze your run\'s performance continuously for making better software.'
            );
        }
    } catch(\Exception $e) {
        echo hydroFatalError('Hydro PerformanceMonitor Error', $e);
        exit(1);
    }

    $perf_monitor->runProfiled('hydro-logic-boot', static function() use ($perf_monitor, &$error) {
        try {
            EventDispatcher::trigger('hydro.boot');
        } catch(\Exception $e) {
            echo hydroFatalError('Hydro Boot Error', $e);
            $error = true;
        }
    });

    if(!$error) {
        $perf_monitor->runProfiled('hydro-logic-init', static function() use ($perf_monitor, &$error) {
            try {
                EventDispatcher::trigger('hydro.init');
            } catch(\Exception $e) {
                echo hydroFatalError('Hydro Initialization Error', $e);
                $error = true;
            }
        });
    }

    if(!$error) {
        $perf_monitor->runProfiled('hydro-logic-exec', static function() use ($perf_monitor, &$error) {
            try {
                EventDispatcher::trigger('hydro.exec');
            } catch(\Exception $e) {
                echo hydroFatalError('Hydro Execution Error', $e);
                $error = true;
            }
        });
    }

    $perf_monitor->endProfile('hydro-logic-complete');

    $perf_monitor->endProfile('hydro-complete');

    if(
        (isset($option['performance']['log']) && $option['performance']['log']) ||
        (defined('FLAG__HYDRO_FLOW__PERF_LOG') && false !== FLAG__HYDRO_FLOW__PERF_LOG)
    ) {
        error_log(
            ($error ? '#! ' : '') .
            'Hydro Performance: ' .
            $perf_monitor->getInformation('hydro-complete')['time'] . 's' . ' ' .
            $perf_monitor->convertMemory($perf_monitor->getInformation('hydro-complete')['memory']) . ' ' .
            $perf_monitor->getResource() . ''
            . "\r\n"
        );
    }
};
